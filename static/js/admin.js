function refresh(once) {
    ws_send('users')
    ws_send('calls')
    ws_send('sql_cc_report')
    if (once === undefined)
      setTimeout('refresh()', 60 * 1000)
}
function adm_onopen(e) {
    console.info("Соединение установлено")
    ws_active = true
    ws_send('user_init', {
      u_ref: $.cookie('u_ref'),
      upass: $.cookie('upass'),
      upath: location.pathname
      }
    )
    ws_send('busy')
    refresh()
}
function adm_onmessage(e) { // MESSAGE FROM CRM
    data = $.parseJSON(e.data)
    
    command = data['command']
    d = data['data']

    if (command != 'ping') console.debug(command, d)

    switch (command) {
    case 'calls':
        fillTemplate('#online_calls', 'template_online_calls', d)
        break
    case 'users':
        fillTemplate('#users', 'template_users', d)
        break
    case 'sql_cc_report':
        s_call = 0
        s_client = 0
        s_out = 0
        for (k in d) {
          s_call   += Number(d[k].call)
          s_client += Number(d[k].client)
          s_out    += Number(d[k].outcall)
        }
        d['total'] = {
          op     : '<div align=right><b>Итого:</b></div>', 
          call   : s_call,
          client : s_client,
          outcall: s_out
        }
        fillTemplate('#cc_report', 'template_cc_report', d)
        da = $.map(d, function(value, index) {
              return [value];
        });
        /*
        i = 0
        j = 999
        cc_calls = $('.cc_call')
        cc_calls.each(function(k, v) {
          s = da[k]
          if (s.op.substr(0, 6) == 'Миклин') return
          if (k == da.length - 1) return
          n_call = Number(s.call)
          if (n_call > i) {
            i = n_call
            cc_calls.removeClass('green')
            v.className += ' green'
          }
          if (n_call < j) {
            j = n_call
            cc_calls.removeClass('red')
            v.className += ' red'
          }
        })
        i = 0
        j = 999
        cc_calls = $('.cc_client')
        cc_calls.each(function(k, v) {
          s = da[k]
          if (s.op.substr(0, 6) == 'Миклин') return
          if (k == da.length - 1) return
          n_call = Number(s.client)
          if (n_call > i) {
            i = n_call
            cc_calls.removeClass('green')
            v.className += ' green'
          }
          if (n_call < j) {
            j = n_call
            cc_calls.removeClass('red')
            v.className += ' red'
          }
        })
        */
        break
    default:
      ws_onmessage(e)
    }
}
function kickout(ref) {
    ws_send('x:self.users["'+ref+'"].send("logout")')
    ws_send('x:self.users.pop(self.users["'+ref+'"]).dict()')
    ws_send('x:self.kickout()')
}
window.onload = function() {
  ws.onmessage = function(e) { adm_onmessage(e) }
  ws.onopen = function(e) { adm_onopen(e) }
  admin_bar.style.display = ''
}
