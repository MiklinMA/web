/*
  NPS
*/
function nps_onload() {
    console.info('Моуль NPS загружен')
}

structure_nps_service = {
  step0: {
    text            : [],
    buttons: {
      'Пропустить'  : [ "skip()",           "btn-danger" ]
    }
  },
  step1: {
    text            : [
      ['label',   "io"],
    ],
    buttons: {
      'Да'          : [ "step2",            'btn-success' ],
      'Нет'         : [ "step1n",           'btn-danger' ],
      'Недоступен'  : [ "not_available()",  'btn-danger' ],
    }
  },
  step2: {
    text            : [
      ['label',   "label1"],
      ['label',   "label2"],
      ['label',   "now",    "Вам сейчас удобно разговаривать?"],
    ],
    buttons: {
      'Да'          : [ "step3",            'btn-success' ],
      'Нет'         : [ "step2n",           'btn-danger' ],
    }
  },
  step3: {
    text            : [
      ['label',   "question1"],
      ['text',    "pause",  "Сделайте небольшую паузу (1-2 секунды), чтобы клиент вспомнил время покупки."],
    ],
    buttons: {
      'Да'          : [ "step4",            'btn-success' ],
      'Был другой'  : [ "step3o",           'btn-warning' ],
      'Нет'         : [ "not_client()",     'btn-danger' ],
    }
  },
  step4: {
    text            : [
      ['label',   "question2"],
      ['text',    "choose", "Оцените возможность рекомендации в баллах от 1 до 10."],
    ],
    buttons: {
      'Нет'         : [ "step5_0",          'btn-danger' ],
      'Не знаю'     : [ "step5_1",          'btn-warning' ],
      'Да'          : [ "step5_2",          'btn-success' ],
    }
  },
  step5: {
    text            : {
      0             : [
        ['label', "why",    "Можете пояснить, что стало причиной столь низкой оценки?"],
      ],
      1             : [
        ['label', "why",    "Есть какие-то замечания или нерешенные вопросы?"],
      ],
      2             : [
        ['label', "why",    "Хотите что-то особенно отметить?"],
      ],
      after         : [
        ['text',  "todo",   "Заполнить таблицу критериями. Внести комментарий."],
        ['text',  "",       "<br>"],
        ['text',  "",       "<br>"],
        ['text',  "",       "<br>"],
        ['text',  "wait",   "Дождаться окончания пояснений клиента."],
        ['label', "ustd",   "Поняла вас, хотите еще что то добавить?"],
      ],
    },
    table: 'TZK',
    buttons: {
      'Далее'       : [ "step6",          'btn-success' ],
    }
  },
  step6: {
    text            : [
      ['label',   "io"],
      ['label',   "danke"],

      ['label',   "moscow", "Возможно Вам позвонят, или пришлют анкету из головного представительства {brand}"],
      ['label',   "moscow", "г.Москва для того, чтобы оценить качество нашей работы."],
      ['label',   "moscow", "Это очень важно для нас. Поэтому просим Вас поставить наивысшую оценку."],

      ['label',   "danke2", "Спасибо за уделённое время."],
      ['label',   "holiday"],
      ['label',   "bye",    "Всего хорошего, до свидания!"],
    ],
    buttons: {
      'Завершить'   : [ "finish()",         'btn-success' ],
    }
  },
  // Альтернативные ветки
  step0n: {
    buttons: {
      'Не доступен' : [ "not_available()",  "btn-danger" ]
    }
  },
  step1m: {
    text            : [
      ['label',   "how",    "Как я могу обращаться к вам?"],
      ['input',   "c_name"],
    ],
    buttons: {
      'Далее'       : [ "not_available()",  "btn-success" ],
      'Никак!'      : [ "nohow()",          "btn-danger" ]
    }
  },
  step1n: {
    text            : [
      ['label',   "",    "Вас беспокоит управляющая компания Автоград, отдел по работе с клиентами!"],
      ['label',   "",    "Меня зовут {u}!"],
      ['label',   "",    "С кем я могу переговорить по качеству обслуживания автомобиля {brand}?"],
    ],
    buttons: {
      'Со мной'           : [ "step2",          'btn-success' ],
      'Сейчас позову'     : [ "step1m",         'btn-success' ],
      'Не сейчас'         : [ "step2n",         'btn-warning' ],
      'Ошиблись номером'  : [ "not_client()",   "btn-danger"  ]
    }
  },
  step2n: {
    text            : [
      ['label',   "how",    "В какое время я могу перезвонить?"],
      ['input',   "recall_d", "date"],
      ['input',   "recall_t", "time"],
    ],
    buttons: {
      'Удобно'            : [ "step3",          'btn-success' ],
      'Отложить'          : [ "postpone()",     'btn-warning' ],
      'Никогда'           : [ "nohow()",        "btn-danger"  ]
    }
  },
  step3n: {
    text            : [
      ['label',   "",       "Приношу свои извинения за доставленное неудобство."],
      ['label',   "",       "<br>"],
      ['label',   "holiday"],
      ['label',   "bye",    "Всего хорошего, до свидания!"],
    ],
    buttons: {
      'Завершить'   : [ "finish_n()",         'btn-danger' ],
    }
  },
  step3o: {
    text            : [
      ['label',   "",    "Имя"],
      ['input',   "3o_name"],
      ['label',   "",    "Телефон"],
      ['input',   "3o_phone"],
    ],
    buttons: {
      'Далее'       : [ "update_inf()",     "btn-success" ],
      'Завершить'   : [ "not_been()",       "btn-danger"  ]
    }
  },
}

// причесать телефоны
// и установить основной для звонка
function nps_make_phones(d) {
}

// определить правильный бренд
// и установить автомобиль
function nps_make_car(d) {
}

// определить и просклонять имя клиента
// а также оператора
function nps_make_names(d) {
}

// добавить поздравление с праздниками
// и приветствие по времени суток
function nps_make_datetime(d) {
}

// добавить особые условия
function nps_make_unusual(d) {
}

function nps_update_data(d) {

  ds = JSON.stringify(structure_nps_service)
  ds.replace('"io"', d.ka)
  structure_nps_service = JSON.parse(ds)

  nps_make_phones(d)
  nps_make_car(d)
  nps_make_names(d)
  nps_make_datetime(d)
  nps_make_unusual(d)

  ka.innerText = d.ka

  for (k in data) {
    el = document.getElementById(k)

    try {
      el.innerText = data[k]
    } catch (e) {
      el.value = data[k]
    }
  }

  nps_navigate('step0')
}


function nps_navigate(step, kind) {
  var s
  s = structure_nps_service[step]
  console.log(step, kind, s)
  if (step == 'step0') {
    /* кнопка для админов */
    if (localuser.manager) s.buttons['Начать без звонка'] = [ "step1",       "btn-warning"  ]

  }
  change_view('nps')

  switch (s.table) {
    case 'TZK':
      step_body.classList.remove('col-md-12')
      step_body.classList.add('col-md-8')
      step_container.appendChild(tzk)
      //tzk.style = "margin: 0px; padding: 0px"
      tzk.style.display = ''
      break;
    default:
      step_body.classList.remove('col-md-8')
      step_body.classList.add('col-md-12')
      tzk.style.display = 'none'
  }

  step_text.innerHTML = ""
  step_buttons.innerHTML = ""
  if (s.text)
    try {
      s.text.forEach(function(t) { nps_add_textelem(t) })
    } catch (e) {
      var before = []
      var body = []
      var after = []
      for (k in s.text) {
        if (k == 'before') before = s.text[k]
        if (k == 'after')  after = s.text[k]
        if (k == kind)     body = s.text[k]
      }
      before.forEach(function(t) { nps_add_textelem(t) })
      body.forEach(function(t) { nps_add_textelem(t) })
      after.forEach(function(t) { nps_add_textelem(t) })
    }

  if (s.buttons)
    for (k in s.buttons) {
      b = s.buttons[k]
      //console.log(b)
      elem = document.createElement('span')
      elem.classList = 'btn-lg btn btn-nps'
      elem.classList.add(b[1])
      if (b[0].indexOf('(') != -1) {
        elem.onclick  = function(e) { eval(b[0]) }
        elem.setAttribute('onClick', b[0])
      } else {
        // nps_navigate
        sub = b[0].indexOf('_')
        if (sub != -1)
          elem.setAttribute('onClick', 'nps_navigate("' + b[0].substring(0, sub) + '", "' + b[0][sub+1] + '")')
        else
          elem.setAttribute('onClick', 'nps_navigate("' + b[0] + '")')
      }
      elem.innerHTML = k
      step_buttons.appendChild(elem)
    }
    //fill_tmodule()
}

