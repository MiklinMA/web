var app = angular.module("CRM", []);

app.controller("orgs", function($rootScope, $scope, $http) {
    $http.get("/api_orgs")
        .then(function(response) {
            console.log(response.data);
            $scope.orgs = response.data;
        });
    $scope.select = function(value) {
        $rootScope.$broadcast('deps_reload', value);
    };
});

app.controller("deps", function($rootScope, $scope, $http) {
    $scope.$on('deps_reload', function(event, data) {
        console.log("deps_reload", data);
        $http.get("/api_deps", {
            params: { o_ref: data }
            })
            .then(function(response) {
                console.log(response.data);
                $scope.deps = response.data;
                var m = 0;
                var d = undefined;
                for (var k in response.data) {
                    console.log(k, response.data[k]);
                    if (response.data[k].order <= m)
                        d = response.data[k];
                }
                if (d != undefined) {
                  $rootScope.$broadcast('managers_reload', d.ref);
                }
            });
    });
    $scope.select = function(value) {
        $rootScope.$broadcast('managers_reload', value.ref);
    };
});

app.controller("managers", function($rootScope, $scope, $http) {
    $scope.$on('managers_reload', function(event, data) {
        console.log("managers_reload", data);
        $scope.d_ref = data;
        $scope.d_phones = null;
        $http.get("/api_managers", {
            params: { d_ref: data }
            })
            .then(function(response) {
                console.log('api_managers', response.data);
                $scope.managers = response.data;
            });
        $http.get("/api_dep_phone", {
            params: { d_ref: data }
        })
        .then(function(response) {
            console.log('api_dep_phone', response.data);
            $scope.d_phones = response.data[0];
        });
    });
});

app.controller("events", function($rootScope, $scope, $http) {
    $http.get("/api_operatorEvent")
        .then(function(response) {
            console.log(response.data);
            $scope.events_urgent = response.data.urgentEvents;
            $scope.events_scheduled = response.data.scheduledEvents;
        });
    $scope.click_urgent = function(data) {
        jQuery.fancybox('<form class="pop-wrap" onSubmit="sendForm($(this), event)"><p class="center"><a href="#" class="button">Выполнить сейчас</a><p><hr><p><label for="date">Дата:</label> <input id="date" type="date" name="date" required><p><label for="time">Время:</label> <input type="time" id="time" name="time" required><p><button type="submit" class="button red">Отложить</button></form>');
    }
});

app.controller("log_record", function($rootScope, $scope, $http) {
    $http.get("/api_logRecord", {params: {o_ref: "B5D100215E2D367211DEBC7D05A0CE4E", cur_date: new Date().getTime()}})
        .then(function(response) {
            console.log(response.data);
            var d = response.data;

            $scope.records = d.employees;
            $scope.date = new Date(d.dateRequest);

            step   = d.timeAcceptions * 1000 * 60;
            mstart = Date.parse($scope.date.toISOString().substr(0, 11) + d.startTimeWork);
            mstop  = Date.parse($scope.date.toISOString().substr(0, 11) + d.endTimeWork);
            count  = Math.ceil((mstop - mstart) / step);
            $scope.width  = 100 / Math.fround(count);

            $scope.records = [];
            for (k in d.employees) {
                $scope.records.push({
                    master: d.employees[k].name,
                    periods: d.employees[k].records,
                });
            }

            $scope.periods = [];
            for (i = 0; i < count; i++) {
                ts = mstart + i * step;
                t = new Date(ts);
                te = mstart + (i+1) * step;
                te = new Date(te);

                $scope.periods.push(t);

                for (k in $scope.records) {
                    record = $scope.records[k];
                    found = false;

                    for (p in record.periods) {
                        period = record.periods[p];
                        // console.log(period.free)
                        if (period.free == true) continue;

                        ds = new Date(period.periodStart.substr(0, 16));
                        de = new Date(period.periodEnd.substr(0, 16));
                        // console.log(ds, de, t, te);

                        if (ds >= t && ds < te) {
                            period.time = t;
                            period.free = false;

                            found = true;
                            break;
                        }
                    }

                    if (found == false) {
                        $scope.records[k].periods.push({
                            time: t,
                            free: true,
                        });
                    }

                }
            }
            console.log($scope.records)

            // buildDiagram(
            //     d.startTimeWork,
            //     d.endTimeWork,
            //     d.timeAcceptions,
            //     d.employees,
            //     d.dateRequest
            // );
        })
});
