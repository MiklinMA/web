var ws
var localuser
var ws_active = false
var last_ping = new Date()
var timerid
var f_logout = false
//=================================================================
// Initialization
//=================================================================
function ws_connect(){
  console.info('Подключение')

  if (location.protocol == 'https:')
    ws_host = "wss://"+location.hostname+":7467/ws"
  else
    ws_host = "ws://"+location.hostname+":7466/ws"

  ws = new WebSocket(ws_host)

  ws.onopen    = function(e) { ws_onopen(e)    }
  ws.onclose   = function(e) { ws_onclose(e)   }
  ws.onmessage = function(e) { ws_onmessage(e) }
  ws.onerror   = function(e) { ws_onerror(e)   }
    
  return ws
}

//=================================================================
// WS Events
//=================================================================
function ws_onopen(e) {
  try { $("#ui_alert").dialog('close') }
  catch (e) {}
  console.info("Соединение установлено")
  ws_active = true
  ws_send('user_init', {
    u_ref: $.cookie('u_ref'),
    upass: $.cookie('upass'),
    upath: location.pathname
    }
  )
}

function ws_onerror(e) {
  console.warn("Ошибка подключения")
  //logout(true)
  //setTimeout('ws_connect()', 5 * 1000)
}

function ws_onclose(e) {
  console.log("ONCLOSE:", goAway, ws_active, e)
  return
  //if (ws_active) return

  ws_active = false
  if (location.pathname != '/') {
    goAway = false
    location.href = '/'
    return
  }
  change_view()
}

function ws_onmessage(e) { // MESSAGE FROM CRM
  d = $.parseJSON(e.data)

  command = d['command']
  d = d['data']
  if (command != 'ping') console.debug(command, d)
  //console.log('ws', command)

  switch (command) {
    case 'user_init':
      user_init(d)
      break
    case 'workspace':
      console.warn("NO WORKSPACE!")
      break
    case 'login':
      login(d)
      break
    case 'logout':
      if (timerid){
        console.error("Потеряно соединение с сервером")
        logout('server\'s call')
      }
      break
    case 'check':
      console.log('Проверка пользователя')
      ws_send('check', {
        ok: false,
        k: d.text
        }
      )
      break
    case 'ones_wait':
      console.log(d['text'])
      break
    case 'ones_init': //meet initialisation
      try { d = $.parseJSON(d) } catch (e) { }
      e_ref.value = d['e_ref']
      break
    case 'ones_call':
      break
    case 'ones_call_missed':
      break
    case 'ones_call_done':
      break
    case 'ones_call_warning':
      break
    case 'ones_call_error':
      break
    case 'ones_call_other':
      break
    case 'sql_orgs':
    /*
      orgs = document.getElementById('orgs') || document.getElementById('o_ref')
      if (orgs)
        for (org in d) {
          o = d[org]
          opt = document.createElement('option')
          opt.value = o['o_ref']
          opt.innerHTML = o['o_name_']
          orgs.appendChild(opt)
          // text module TODO
        }
      orgs = d
    */
      break
    case 'sql_targets':
        fill_targets(d)
        targets = d
        break
    case 'sql_managers':
        fill_managers(d)
        break
    case 'sql_contacts':
        fill_contacts(d)
        break
//    case 'goto':
//        if (dnd || activeView == 'form') {
//          ping = false
//          break
//        }
//        goAway = false
//        return location.href = d['text']
    case 'ping':
      last_ping = new Date()
      ws_send('pong', { busy: localuser && localuser.busy })
      /*
      if (d['cq'])
        status_calls.innerHTML = 'Входящие звонки на линии ' + d['cq'] + '.'
      else
        status_calls.innerHTML = ''
        */
//
//      //if (d['goto'] && !localuser.busy) {
//      //  if (location.pathname != d['goto']) {
//      if (
//        //location.pathname != '/' && 
//        (
//          location.pathname == '/call' ||
//          location.pathname == '/nps'
//        ) &&
//        d.goto  && d.goto != "/" && 
//        location.pathname != d['goto']
//        ) {
//        /*
//        if (d['goto'] == '/call'){
//          //dnd = true
//          //send_results('onunload')
//        }
//        */
//        //ws_send('goaway')
//        goAway = false
//        console.log("GOTO:", d.goto, "!=", location.pathname)
//        ws_active = false
//        return location.href = d.goto
//      }
/*
      _busy = undefined
      try {
        if (!RTC && activeView == 'main')
          _busy = localuser.busy
      } catch(e) {
      } finally {
      }
      //ping = false
      */
      break
    default:
      common_onmessage(e)
  }
}

function ws_send(command, ws_send_data, delay) {
    if (!ws_active) {
      console.warn("Socket inactive", command)
      return
    }

    if (ws.readyState != 1) {
      console.error("ws is not ready", command, ws_send_data, delay)
      return
    }

    if (ws_send_data)
        if (typeof (ws_send_data) == 'string')
            q = JSON.stringify([command, {'value': ws_send_data } ])
        else
            q = JSON.stringify([command, ws_send_data])
    else
        q = JSON.stringify(command)

    if (delay) {
        setTimeout('ws_send('+q+')', delay)
        return
    }

    ws.send(q)
}

function ws_sms(number, message) {
  if (!number) return
  if (!message) {
    message = 'Клиент {client}(номер телефона {phone}) просит вас ему перезвонить.'
    if (c_name.value)
      message = message.replace('{client}', c_name.value + ' ')
    else
      message = message.replace('{client}', '')
    message = message.replace('{phone}', g_number)
  }
  console.log(number, message)

  ws_send('sms_send', { number: number, message: message})
}

function ping_timeout(){
    date = new Date()
    if (date.getTime() - last_ping.getTime() > 59*1000 && !dnd){
        console.error('Истекло время ожидания')
        logout('server\'s call')
    }
}

ping = false
function ws_ping() {
    if (ping && localuser) {
        console.error('no ping!')
        ping = false
        ws_connect()
    } else {
        try {
          if (!RTC && activeView == 'main')
            _busy = localuser.busy
        } catch(e) {
          _busy = undefined
        }
        ws_send('ping', { busy: _busy })
        ping = true
        setTimeout('ws_ping()', 60 * 1000 * 1)
    }
}

function user_init(user) {
    if (!user) {
      return logout("Ошибка инициализации пользователя!")
    } else if (!user.username || !user.password) {
      return logout('отсутствуют авторизационные данные')
    } else if ( !$.cookie('u_ref') || !$.cookie('upass')) {
      return logout('отсутствуют сохраненные настройки')
    }


    if (user_bar)
        user_bar.style.display = ""
    if (admin_bar && user.name == 'miklin') {
        admin_bar.style.display = ""
        goAway = false
    }

    if (user.auth) {
        localuser = user
    }
    change_view('main')

    timerid = setInterval(ping_timeout, 60*1000)
    ws_send("globals")

    return true

// 
//     if (location.pathname == '/') {
//         console.info('Авторизация пройдена успешно')
// 
//         document.getElementById('engineer').style.display = 'none'
//         document.getElementById('front').style.display    = 'none'
//         document.getElementById('meet').style.display     = 'none'
//         document.getElementById('call').style.display     = 'none'
//         document.getElementById('callback').style.display = 'none'
// 
//         if (user.kind == 'front') {
//             goAway = false
//             location.href = '/front'
//         } else if (user.kind == 'hostess') {
//             goAway = false
//             location.href = '/meet'
//         } else if (user.kind == 'engineer') {
//             goAway = false
//             location.href = '/engineer'
//         } else if (user.can_call && user.can_meet) {
//             change_view('selector')
//             document.title = 'Выбор рабочего места - CRM Автоград'
// 
//             document.getElementById('meet').style.display     = ''
//             document.getElementById('call').style.display     = ''
//             document.getElementById('callback').style.display = ''
//         } else if (user.kind == 'operator') {
//             change_view('selector')
//             document.title = 'Выбор рабочего места - CRM Автоград'
// 
//             document.getElementById('call').style.display     = ''
//             document.getElementById('callback').style.display = ''
//         } else {
//             console.log("LOGOUT", user)
//             return logout('selector')
//         }
//     } else if (location.pathname == '/meet') {
//         if (!$.cookie('o_ref')) {
//           ws_send("sql_orgs", {isMeet: true})
//           change_view('org')
//         } else {
//           $('#orgs').val($.cookie().o_ref)
//           $('#o_ref').val($.cookie().o_ref)
//           get_targets($.cookie('o_ref'))
//         }
// 
//     } else if (location.pathname == '/call') {
//       /*
//         console.info("Инициализация рабочего стола")
//         ws_send("workspace")
//         ws_send("calls")
//         ws_send("sql_orgs", {isCall: true})
// 
//         setInterval('addmin()', 60 * 1000)
//         change_view('main')
//         fill_contacts({})
//         */
//     } else if (location.pathname == '/admin') {
//       ws_send("calls")
//       setInterval('addmin()', 60 * 1000)
//     } else {
//       change_view('main')
//     }
}

function login(d) {
    console.log(d)
    if (d['result'] == false) {
      change_view('login')
      return
    }

    $.cookie('u_ref',  d.ref)
    $.cookie('o_ref',  d.o_ref)
    $.cookie('d_ref',  d.d_ref)
    $.cookie('u_name', d.fullname)
    $.cookie('upass',  d.password)

    return user_init(d)
}

function logout(server) {
    console.warn('Выход из системы:', server)
    if(timerid) {
        clearTimeout(timerid)
        timerid = undefined
    }

    if ((server === undefined || server == "Пользователь") && f_logout == false){
    	f_logout = true
      ws_send('logout')
    }

    $.each($.cookie(), function(k) {
        $.removeCookie(k, null)
    })

    localuser = undefined

    if (location.pathname != '/') {
        goAway = false
        location.href = '/'
    } else {
        change_view('login')
        document.getElementById('user_bar').style.display = "none"
    }
    return false
}

