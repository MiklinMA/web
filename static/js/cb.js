v_total = null
//postpone = false
drop = false
drop_doc = false
drop_user = false
no_answer = false
kaio = undefined
c_phones = []
result_sent = false
fill_main_form = false
end_try = false
disp_moscow = ''
result = ''
done = false
var fb_ref = ''

function cb_onload(){
  loadJS('/static/js/dist/sip.js', function() {
    console.info('Моуль телефонии загружен ( версия:', SIP.version, ')')
    ws_connect()
    ws.onmessage = function(e) { cb_onmessage(e) }
    sip_button_init.onclick = function(e) { cb_call_init() }
  })
}

function cb_call_invite(number) {
  call_invite(number)
  _call.on('failed',    function(e) { cb_call_failed     (e) });
  _call.on('accepted',  function(e) { cb_call_accept     (e) });
}

function cb_call_accept(e) {
  if (result_sent == false){
    call_accept(e)
    navigate('step', 1)
  }
}

function cb_call_failed(e, e_clean) {
  call_failed(e, e_clean)
  if (step0.style.display == '') navigate('step', '0n')
}

function cb_onmessage(e) {
    data = $.parseJSON(e.data)

    command = data['command']
    data = data['data']

    console.log('cb', command, data)

    switch (command) {
    case 'user_init':
      user_init(data)
      cb_user_init(data)
      break
    case 'unbusy':
      ws_send('busy')
      break
    case 'ones_feedback_init':
      fill_form(data)
      change_view('form')
      break
      /*
    case 'ones_wait':
      setTimeout("ws_send('ones_feedback_init',{user: localuser})", 3000)
      break
      */
    case 'get_nps_end':
      console.error("На сегодня все клиенты обработаны")
      location.href = "/"
      break

    /*case 'nps_user_drop':
      if (end_try == false){
        try_other_number()
        cb_call_down()
      }
      if (end_try == true) {
        ui_alert("Внимание", 'Абонент недоступен, звонок будет отложен')
        console.warn('Абонент недоступен')
        cb_call_down()
        no_answer = true
        send_results('no_answer')
      }
      break
    case 'nps_no_answer':
     /* if (end_try == false){
        try_other_number()
        cb_call_down()
      }
      if (end_try == true) {
        ui_alert("Внимание", 'Абонент недоступен, звонок будет отложен')
        console.warn('Абонент недоступен')
        no_answer = true
        cb_call_down()
        ws_send('call_down')
       /* send_results('no_answer')
      }
      break
    case 'nps_user_inv':
      if (end_try == false){
        try_other_number()
        cb_call_down()
      }
      if (end_try == true) {
        ui_alert("Внимание", 'Абонент недоступен, звонок будет отложен')
        console.warn('Абонент недоступен')
        no_answer = true
        cb_call_down()
        send_results('no_answer')
      }
      break
      //nps_user_inv*/
      /*
    case 'initphoner':
      console.info("Инициализация телефона")
      loadJS('/static/js/dist/sip.js', function(){
                                         f_initphoner(data['sip']);
                                         cb_call_init();
                                       }) //initphoner(data['sip'])
      break
      */
    default:
      sip_onmessage(e)
    }
}

function cb_sip_bye(e) {
    if (!RTC) return

    RTC = false
    call_beep.pause()
    console.info('Разговор завершен')
    RTC = undefined

    document.getElementById('sip_button_init').classList.add   ('btn-success')
    document.getElementById('sip_button_init').classList.remove('btn-danger')
}

function cb_call_down() {
  if (RTC===false) return

  call_beep.pause()
  console.info('Вызов завершен')
  RTC = false
  sip = undefined
  if (step0.style.display == '') navigate('step', '0n')
  //initphoner(localuser['phone'])

  document.getElementById('sip_button_init').classList.add   ('btn-success')
  document.getElementById('sip_button_init').classList.remove('btn-danger')

  //dnd = true
}

function cb_user_init(user) {
  //console.info("Инициализация телефона")
  ws_send("workspace")

  navigate('step') // backwards 
  ws_send('ones_feedback_init',{user: localuser})
  nps_navigate('step0')

  if (user.manager || user.name == 'miklin') {
      span = document.createElement('span')
      span.setAttribute('class','btn btn-warning btn-lg ')
      span.setAttribute('onClick','nps_navigate("step1")')
      //span.style.cssText = 'margin-bottom: 8px'
      span.innerHTML = 'Начать без звонка'
      step_buttons.appendChild(span)
  }

  //sip_button_init.onclick = function(e) {ws_send("initphoner") /*cb_call_init()*/ }
}

// Вызывается по зеленой кнопке
// определяет нужно ли звонить или ответить на входящий звонок
function cb_call_init(number) {
  _call = get_unanswered_call()
  keys = Object.keys(calls)

  if (_call) call_answer() // если есть звонок на линии - отвечаем
  else if (keys.length < 2) { // если нет - звоним исходящий
    if (g_number) cb_call_invite(g_number) // если передан номер с кнопки соединяем
    else phone_window() // иначе спрашиваем номер
  }
  else {  // если уже есть два звонка на линии - вешаем активный
    //delete calls[keys[0]]
    hangup(active_call.sip)
    //active_call.sip.terminate() // ожидаем вызова call_terminate
  }
}

/*
function cb_call_init() {

 if (RTC) {
    if (RTC.status != 1) {
      call_down()
      cb_call_down()
    }
  } else {
    //initphoner(localuser['phone'])
    call_hold(true)
    if (testing == true) {
      RTC = sip.invite(g_number + '_nps_test', sip_opts)
    } else {
      RTC = sip.invite(g_number + '_nps', sip_opts)
    }
    RTC.on('connecting',function(e) { sip_connecting(e) })
    RTC.on('progress',  function(e) { sip_progress(e) })

    RTC.on('accepted',  function(e) { cb_sip_accepted(e) })
    RTC.on('rejected',  function(e) { cb_sip_failed(e) })
    RTC.on('cancel',    function(e) { cb_sip_failed(e) })
    RTC.on('failed',    function(e) { cb_sip_failed(e) })

    RTC.on('bye',       function(e) { cb_sip_failed(e) })

    document.getElementById('sip_button_init').classList.add   ('btn-danger')
    document.getElementById('sip_button_init').classList.remove('btn-success')
    //ws_send('busy')
    dnd = true
    navigate('step','0n')
  }
}
*/

function cb_sip_failed(e) {
  if (e != null)
    //console.log('Произошла какая-то ошибка!', e)
    cb_call_down()
}

//******************************************************************************
//                           HTML Функции для кнопок
//******************************************************************************
function skip(){              //кн. Пропустить
  $( "#drop_doc" ).dialog({ width: '591px',title:'Пропустить документ',show: 'slide', position: [$('#phone').offset().left, 120],'modal': true  })
}

function fDrop_doc(){
    textarea = document.getElementById('drop_doc').getElementsByTagName('textarea')[0]
    if (textarea.value.length == 0) {
      ui_alert('Внимание', 'Для пропуска документа заполните комментарий')
      return
    }
    comment.value = textarea.value
    dnd = true
    drop_doc = true;
    send_results('drop_doc')
}

function not_available(){     //кн. Недоступен
  dnd = true
  no_answer = true;
  send_results('no_answer')
}

function not_client(){
	v_total = -1
  result = 'succes';
  comment.value = 'Неправильно указан номер телефона.'
  navigate('step', '3n');
}

function not_been(){          //кн. Нет step3 step3o Завершить
  drop_doc = true;
  result = 'drop_doc';
  navigate('step', '3n');
}

function nohow(){             //кн. step1m Никак step1n Нет step2n Никогда
  drop_user = true;
  result = 'drop_user';
  navigate('step', '3n')
}

function postpone(){          //кн. Отложить step2n
  //postpone = true;
  result = 'postpone';
  navigate('step', '3n')
}

function finish_n(){          //кн. Завершить step3n
  send_results(result)
}

function finish(){            //кн. Завершить question3
  send_results('succes')
}
//******************************************************************************
//                           HTML Функции для кнопок
//*****************************************************************************

function fill_tmodule(data) {
  data = data.split(' ')

  try {
    rn = new RussianName(data[0], data[1], data[2]);
    io_vp = rn.fullName(rn.gcaseVin);
  } catch(e) {
    console.log(e)
  }
  if (rn.ln) io = data.splice(data.length * -1 + 1).join(" ");
  else io = data.join(" ");

  brand = document.getElementById('brand').innerText//.split(" ")[0]

  if ( disp_moscow == 'Chrysler Jeep Dodge'){
    arr_abrand = disp_moscow.split(' ')
    for ( i = 0; i < arr_abrand.length; i++){
      if (model.innerText.indexOf(arr_abrand[i])+1) {
        disp_moscow = arr_abrand[i]
        break
      }
    }

  }
  u = localuser.fullname.split(" ")[1]
  tms = document.getElementsByClassName('tmodule')
  Array.prototype.forEach.call(tms, function (tm) {
    tm.innerText = tm.innerText.replace('{io}', io)
    tm.innerText = tm.innerText.replace('{io_vp}', io_vp)
    tm.innerText = tm.innerText.replace('{brand}', disp_moscow)
    tm.innerText = tm.innerText.replace('{u}', u)
    tm.innerText = tm.innerText.replace(kaio, io)
  });

  kaio  = io
}

function getPhone(phone){
  //phone = data['phone'];
  phone = phone.replace(/\-/g, "");
  phone = phone.replace(/\s/g, "");
  phone = phone.replace(/\(/g, "");
  phone = phone.replace(/\)/g, "");
  phone = phone.replace(/\+7/, "8");

  if (phone.length == 6) phone = '83452' + phone;
  if (phone.length == 10) phone = '8' + phone;

  return phone;
}

function select_phone(elem){
  radio = elem.querySelectorAll('input[type=radio][name=phone]')[0]
  radio.checked = true

  phone_number = getPhone(elem.querySelectorAll('input[name=phone_number]')[0].value)

  a_phone = phone_table.getElementsByClassName('active_manager')

  if(a_phone.length){
    a_phone[0].classList.remove('active_manager')
  }

  elem.classList.add('active_manager')

  g_number = phone_number
  document.getElementById('phone').innerText = phone_number
}


function phone(phone, kphones) {
  var re = /([\8|\+7][\d\-\(\)]{10,17})\b|(\d{11})|(\d{10})|([\d\-]{6,8})\b|(\d{2}\s\d{2}\s\d{2})|(\(\d{3,5}\)(\s?[\d\-]{5,10}|\s?[\d\s]{7}))|(\d{5})/gm;
  var m; i=0;
  var main_number;
  c_phones = [];

  if (kphones.length > 0){
    c_phones = kphones
    for (var i = 0; i < kphones.length; i++){
      if (c_phones[i].length >10)
        c_phones[i].phone = c_phones[i].phone.substr(1)

      if (c_phones[i].main_num)
        main_number = getPhone(c_phones[i].phone)

      if (c_phones[i].last_num)          //Выбирается номер на который звонили в прошлый раз
        g_number = getPhone(c_phones[i].phone)
    }

    if (g_number == undefined)  //Если клиенту не звонили тогда выбирается основной номер телефона
      g_number = main_number

  } else {
    //if(sphone) m = re.exec(sphone);
    while ((m = re.exec(sphone)) != null) {
      if (m.index === re.lastIndex) {
          re.lastIndex++;
      }
      if(m[0]){
        c_phones[i]=getPhone(m[0]);
        i++;
      }
    }

    if (c_phones.length == 0) return

    c_phones = $.unique(c_phones)

    for (var i = 0; i < c_phones.length; i++){
      c_phones[i] = {'phone':c_phones[i],'comment':'', 'last_num': false, 'active': false, "main_num": false}
    }

    g_number = c_phones[0].phone
  }

  if (c_phones.length >= 1) {
    phone = document.getElementById('phone')
    phone.innerText = g_number

      text_el = [{'tag':'button', 'css':'margin-bottom: 7px;', 'text':'Добавить', 'attr':{'onclick':'insert_phone()','class':'btn btn-success'}}]
      dialog.appendChild(create_element(text_el))

      del_phone = false

      for (var i = 0; i < c_phones.length; i++) {
        if(c_phones[i].del){
          del_phone = true
        }
      }

      if (del_phone)
        text_el = [{'tag':'span', 'css':'cursor: pointer; color: red;', 'text':'', 'attr':{'onclick':'phone_window()','class':'glyphicon glyphicon-pencil'}}]
      else
        text_el = [{'tag':'span', 'css':'cursor: pointer;', 'text':'', 'attr':{'onclick':'phone_window()','class':'glyphicon glyphicon-pencil'}}]
      phone.parentNode.appendChild(create_element(text_el))
      
      if(c_phones.length > 1){
      	text_el = [{'tag':'div', 'css':'padding-top: 4px;font-size: 1.3em;font-weight: normal;color: #2d6987;', 'text':'несколько номеров!', 'attr':{}}]
      	phone.parentNode.appendChild(create_element(text_el))
      }

      if (del_phone){
        text_el = [{'tag':'div', 'css':'padding-top: 4px;font-size: 1.3em;font-weight: normal;color:red;', 'text':'есть скрытые номера!', 'attr':{}}]
        phone.parentNode.appendChild(create_element(text_el))
      }

      text_el = [{'tag':'table', 'css':'', 'text':'', 'attr':{'id':'phone_table'}}]
      dialog.appendChild(create_element(text_el))

      text_el = [{'tag':'tr', 'css':'', 'text':'', 'attr':{}},
                 {'tag':'th', 'css':'', 'text':'Номер', 'attr':{}},
                 {'tag':'th', 'css':'', 'text':'Комментарий', 'attr':{}},
                 {'tag':'th', 'css':'', 'text':'', 'attr':{}}]
      phone_table.appendChild(create_element(text_el))

      for (var i = 0; i < c_phones.length; i++) {
        css_class = '';

        if (c_phones[i].active){
          plus_minus = 'glyphicon-plus'
        } else {
          plus_minus = 'glyphicon-minus'
        }

        if (c_phones[i].main_num){
          css_class   = 'friendly_manager'
          minus_style = 'padding: 0 0 0 7px; cursor: pointer;'
          star_style  = 'display: none;'
        } else {
          minus_style = 'border-right: 2px solid; padding: 0 7px 0 7px; cursor: pointer;'
          star_style  = 'padding: 0 0 0 9px;'
        }

        text_trash = '<span class="glyphicon glyphicon-trash" onclick="delete_row(this)" title="Удалить" style="border-right: 2px solid;padding: 0 10px 0 0px;cursor: pointer;">  </span>'
        text_minus = '<span class="glyphicon ' + plus_minus + '" style="' + minus_style + '"">  </span>'
        text_star  = '<span class="glyphicon glyphicon-star star" onclick="make_main(this)" title="Сделать номер телефона основным" style="' + star_style + '""></span>'
        text_span  = text_trash + text_minus + text_star + '<input type="radio" name="phone" style="display:none">';

        text_el = [{'tag':'tr', 'css': '', 'text':'', 'attr':{'onclick':'select_phone(this)', 'class':css_class}},
                   {'tag':'td', 'css':'', 'text': '<input name="phone_number" class="c_phone" onblur="update_phone(this)" value='+getPhone(c_phones[i].phone)+' style="border: none;width: 170px;background: transparent;">', 'attr':{}},
                   {'tag':'td', 'css':'', 'text': '<textarea style="border: none;background: transparent;resize: vertical;" onchange="edit_comment(this)" placeholder="Комментарий">'+c_phones[i].comment+'</textarea>', 'attr':{}},
                   {'tag':'td', 'css':'text-align: center;', 'text': text_span, 'attr':{}}]
        tr = create_element(text_el)
        phone_table.appendChild(tr)
        if (c_phones[i].phone == g_number/*.substr(1)*/) {
          select_phone(tr)
        }
      }
  }

  ps = $('.c_phone')
  ps.mask("8 (999) 999-99-99", {placeholder:" "})
  //g_number = "89292658262"
}

function update_phone(elem){
  var phone = getPhone(elem.value).substr(1)

  if (phone != elem.defaultValue && phone.length == 10){
    i=elem.parentNode.parentNode.sectionRowIndex
    c_phones[i-1].phone = phone
  } else {
    main_number = phone_table.getElementsByClassName('friendly_manager')[0]
    select_phone(main_number)
    delete_row(elem.parentNode.parentNode.getElementsByTagName('span')[0])
  }
}

function make_main(elem){
  fm = phone_table.getElementsByClassName('friendly_manager')
  if (fm.length > 0){
    fm = fm[0]
    fm.classList.remove('friendly_manager')
    spans = fm.getElementsByTagName('span')
    spans[1].style.padding = '0 7px 0 7px'
    spans[1].style.borderRight = '2px solid'
    spans[2].style.display = ''
    spans[2].style.padding = '0 0 0 9px'

    parent_index = fm.sectionRowIndex-1
    if (c_phones[parent_index]) {
      c_phones[parent_index].main_num = false
    }
  }

  elem.parentNode.parentNode.classList.add('friendly_manager')
  spans = elem.parentNode.querySelectorAll('span')
  spans[2].style.display = 'none'
  spans[2].style.padding = '0'
  spans[1].style.border = ''
  spans[1].style.padding = '0 0 0 7px'

  /*if (spans[1].className.indexOf('glyphicon-plus')+1)
    active(spans[1])*/

  parent_index = elem.parentNode.parentNode.sectionRowIndex-1
    if (c_phones[parent_index]) {
      c_phones[parent_index].main_num = true
    }
}

function edit_comment(elem){
  parent_index = elem.parentNode.parentNode.sectionRowIndex-1
  if (c_phones[parent_index]) {
    c_phones[parent_index].comment = elem.value
  }
}

function delete_row(elem){
  i=elem.parentNode.parentNode.sectionRowIndex;
  if (i <= c_phones.length) {
    if (c_phones[i-1].main_num /*&& i-1 > 0*/ && c_phones.length > 1) {
      phone_table.deleteRow(i);
      c_phones.splice(i-1, 1);
      c_phones[c_phones.length-1].main_num = true
      var elements_table = phone_table.querySelectorAll('tr')
      make_main(elements_table[elements_table.length-1].getElementsByTagName('span')[0])
      return
    } else if(c_phones.length == 1) {
      ui_alert("Внимание", "Данный номер телефона является последним,\nв начале добавьте новый номер,\nа затем удалите этот")
      return
    }
  }
  c_phones.splice(i-1, 1);
  phone_table.deleteRow(i);
}

function phone_window(){
  $( "#dialog" ).dialog({ width: '591px',title:'Выбор номера телефона',show: 'slide', position: [$('#phone').offset().left, 120],'modal': true  });
}

function edit_save_phone(elem, other){
  var span
  if (other > 0) {
    delete_row(elem)
  }
  var phone = getPhone(elem.value)
  if (phone.substr(1).length < 10) {
    update_phone(elem)
    return
  }

  tr = elem.parentNode.parentNode
  if (other == 'glyphicon'){
    span = elem
  } else {
    span = tr.getElementsByTagName('span')[0]
  }

  input = tr.getElementsByTagName('input')[0]
  input.onblur = function () {update_phone(this)}

  textarea = tr.getElementsByTagName('textarea')[0].value

  g_number = phone
  c_phones.push({'phone':g_number.substr(1),'comment':textarea,'active':true, 'main_num': true, 'last_num': false})

  document.getElementById('phone').innerText = g_number

  fm = phone_table.getElementsByClassName('friendly_manager')
  if (fm.length > 0){
    fm = fm[0]
    fm.classList.remove('friendly_manager')
    spans = fm.getElementsByTagName('span')
    spans[1].style.padding = '0 7px 0 7px'
    spans[1].style.borderRight = '2px solid'
    spans[2].style.display = ''
    spans[2].style.padding = '0 0 0 9px'

    parent_index = fm.sectionRowIndex-1
    if (c_phones[parent_index]) {
      c_phones[parent_index].main_num = false
    }
  }

  tr.classList.add('friendly_manager')
  spans = tr.querySelectorAll('span')
  spans[2].style.display = 'none'
  spans[2].style.padding = '0'
  spans[1].style.border = ''
  spans[1].style.padding = '0 0 0 7px'
  setTimeout(function (){edit_span(span)},300)
}

function edit_span(elem){
  elem.classList.remove('glyphicon-floppy-disk')
  elem.classList.add('glyphicon-trash')
  elem.onclick = function onclick() {edit_save_phone(this,1)}
}

function insert_phone(){

  text_trash = '<span class="glyphicon glyphicon-floppy-disk" title="Сохранить" style="border-right: 2px solid;padding: 0 10px 0 0px;cursor: pointer;">  </span>'
  text_minus = '<span class="glyphicon glyphicon-plus" style="padding: 0 0 0 7px;">  </span>'
  text_star  = '<span class="glyphicon glyphicon-star star" onclick="make_main(this)" title="Сделать номер телефона основным" style="display: none;"></span>'
  text_span = text_trash + text_minus + text_star + '<input type="radio" name="phone" style="display:none">';

  text_el = [{'tag':'tr', 'css':'', 'text':'', 'attr':{'onclick':'select_phone(this)'}},
             {'tag':'td', 'css':'', 'text': '<input name=phone_number class="c_phone" onblur="edit_save_phone(this,0)" value="" style="border: none;width: 170px;background: transparent;">', 'attr':{}},
             {'tag':'td', 'css':'', 'text': '<textarea  style="border: none;background: transparent;resize: vertical;" onchange="edit_comment(this)" placeholder="Комментарий"></textarea>', 'attr':{}},
             {'tag':'td', 'css':'text-align: center;', 'text': text_span, 'attr':{}}]
  phone_table.appendChild(create_element(text_el))
  ps = $('.c_phone')
  ps.mask("8 (999) 999-99-99", {placeholder:" "})

  var elements_table = phone_table.querySelectorAll('[name=phone_number]')
  elements_table[elements_table.length-1].focus()
}

function fill_form(data) {
  kphones = []

  if (fill_main_form == false) {
    fill_main_form = true
  } else {
    //console.log("call_48_get")
    return
  }

  for (k in data) {
    el = document.getElementById(k)
    if (k == 'abrand' && data[k] != 'None') {
      disp_moscow = data[k]
    }

    if (k == 'kphones')
      kphones = data[k]

    if (el && k!='phone')
      try {
        el.innerText = data[k]
      } catch (e) {
        el.value = data[k]
      }
  }

  fb_ref = data['fb_ref']

  sphone = data['phone']+" "+data['mphone'];
  phone(sphone, kphones);

  fill_tmodule(ka.innerText)
  //g_number = document.getElementById('phone').innerText
  //if (g_number.length >= 10) g_number = '8' + g_number.substr(-10)
  //phone.innerText = g_number
  //phone.innerText = '89292003066'

  if( brand == 'ЭФ КР СУРГУТ'){
    brand = 'КР'
  }

  if ( brand == 'КР' || brand == 'ТКЗ'){
    brand_1 = 'КР'
    barnd_2 = 'ТКЗ'
  } else{
    brand_1 = 'Кузовной'
    barnd_2 = 'Мыс'
  }

  switch(brand){
    case brand_1:
      label1 = 'Вам звонят из Управляющей компании Автоград.'
      label2 = 'Отдел по работе с клиентами. Меня зовут '+u+'.'
      question1 = io + ', вам отремонтировали автомобиль в Кузовном центре Автоград. Верно?'
      question2 = 'Скажите, пожалуйста, будете ли вы рекомендовать Кузовной центр Автоград своим друзьям и знакомым?'
      elem = document.getElementById("question3").getElementsByClassName("tmodule")
      Array.prototype.forEach.call(elem, function (tm) {
        tm.style.display = 'none'
      });
      step_required(['step2','step3','step4']);
      break
    case barnd_2:
      label1 = 'Я звоню по обслуживанию автомобиля '+ model.innerText+'.'
      label2 = 'Специалист по работе с клиентами. Меня зовут '+u+'.'
      question1 = io + ', в течение этой недели Вы посещали автоцентр Мыс Авто. Верно?'
      question2 = 'Скажите, пожалуйста, будете ли вы рекомендовать автоцентр Мыс Авто своим друзьям и знакомым?'
      step_required(['step2','step3','step4']);
      break
    default:
      q_brand = brand
      label1 = 'Вам звонят из Управляющей компании Автоград.'
      label2 = 'Отдел по работе с клиентами. Меня зовут '+u+'.'
      if (brand.indexOf('СТО')+1){
        q_brand = brand.replace('СТО ','')
        question2 = 'Скажите, пожалуйста, будете ли вы рекомендовать сервис '+q_brand+' своим друзьям и знакомым?'
        step_required(['step3','step4']);
      }else{
        question2 = 'Скажите, пожалуйста, будете ли вы рекомендовать сервис Автоград своим друзьям и знакомым?'
      }
      question1 = io + ', в течение этой недели Вы посещали наш автосервис '+(disp_moscow||q_brand)+'. Верно?'
      break
  }

  tms = document.getElementsByClassName('tmodule')
  Array.prototype.forEach.call(tms, function (tm) {
    tm.innerText = tm.innerText.replace('{label1}', label1)
    tm.innerText = tm.innerText.replace('{label2}', label2)
    tm.innerText = tm.innerText.replace('{question1}', question1)
    tm.innerText = tm.innerText.replace('{question2}', question2)
  });

  /*Приветствие*/
  day = new Date();
  hour = day.getHours();
  mins = day.getMinutes();
  greeting = document.getElementById('greeting');
  if (hour>=5 && hour<11) {greeting.innerHTML = "Доброе утро!"+ '<br />'; greeting.style.display = "block";}
  else {
      if (hour>=11 && hour<16) {greeting.innerHTML = "Добрый день!"+ '<br />'; greeting.style.display = "block";}
      else {
            if (hour>=16 && hour<22) {greeting.innerHTML = "Добрый вечер!"+ '<br />'; greeting.style.display = "block";}
            else {
                  if (hour>=0 && hour<5) {greeting.innerHTML = "Доброй ночи!"+ '<br />'; greeting.style.display = "block";}
                  }
            }
      }
  //recall_t.value = new Date().toTimeString().substr(0, 5)
  d1 = new Date()
  d1.setMinutes(d1.getMinutes() + 30)
  recall_t.value = d1.toTimeString().substr(0, 5)
  recall_d.value = $.datepicker.formatDate('yy-mm-dd', d1)

  var days = day.getDate();
  var month = day.getMonth() + 1;
  /*Приветствие поздравлениен с праздником*/
  switch(month){
    case 1:
      if(days>=1 && days<6){
          write_holiday('С Новым Годом!')
        }
      if(days >= 6 && days<=7){
           write_holiday('С Рождеством!')
        }
      break
    case 12:
      if(days >=24 && days<=31){
           write_holiday('С наступающим Новым Годом!')
        }
      break
    default:
  }
}

function step_required(steps_names){
  steps = document.getElementsByClassName('step')
  //steps_names = ['step2','step3','step4']
  for (i = 0; i < steps.length; i++){
    step = steps[i]
    if(steps_names.indexOf(step.id)+1){
      if(step.id == 'step4'){
        step.children[0].children[0].classList.add('required')
        step.children[0].children[2].classList.add('required')
      }else{
        step.children[0].classList.add('required')
      }
    }
  }
}

function write_holiday(text){
 holiday = document.getElementsByClassName("holiday");
 Array.prototype.forEach.call(holiday, function (h) {
        h.innerText = h.innerText.replace('{holiday}', text)
        h.style.display = "block";
        });
}

function update_inf(){
    finish_button.onclick = function onclick(){
      navigate('step',0);
      finish_button.onclick = function onclick(){send_results(result)}
    };

    newname = document.getElementById('3o_name');

    if(newname.value) {
      fill_tmodule(newname.value);

      ka = document.getElementById('ka')
      s = ka.innerText.indexOf('(');
      if(s != -1) ka.innerText=ka.innerText.slice(0,s) +' ('+newname.value+')';
      else ka.innerText = ka.innerText+' ('+newname.value+')';
    }

    newphone = document.getElementById('3o_phone');
    if(newphone.value){
      if(newphone.value.length == 6) newphone.value = '83452'+newphone.value;

      if (newphone.value.length >= 10) newphone.value = '8' + newphone.value.substr(-10)

      if(selectphone.style.display != 'none'){
        p = document.createElement('option')
        p.value = newphone.value
        p.innerHTML = newphone.value
        p.selected = true
        selectphone.appendChild(p)
        selectphone.style.display = ''
        g_number = newphone.value
        c_phones.push(newphone.value)
      }else{
        c_phones.push(newphone.value)

        for (var i = 0; i < c_phones.length; i++){
          p = document.createElement('option')
          p.value = c_phones[i]
          p.selected = true
          p.innerHTML = c_phones[i]
          selectphone.appendChild(p)
        }

        document.getElementById('phone').style.display = 'none'
        selectphone.style.display = ''
        g_number = newphone.value
      }
    }

    navigate('step','3n');
}

function try_other_number(){
  if (selectphone.options.length <= selectphone.options.selectedIndex + 1) {
    end_try = true
    return
  }

  if (selectphone.options.length-1 >= selectphone.options.selectedIndex) {
    ui_alert("Внимание", 'Первый номер не отвечает, попробуйте позвонить на другой номер (номер уже выбран)')
    selectphone.options[selectphone.selectedIndex+1].selected = true
    g_number = selectphone.options[selectphone.selectedIndex].value
  } else {
    end_try = true
  }
}

function answer(value) {
  v_total = value
  switch (v_total) {
  case -1:
    ask_comment.innerText = "Можете пояснить, что стало причиной столь низкой оценки?"
    ask_comment.parentNode.getElementsByTagName('div')[0].style.display = ''
    break
  case 0:
    ask_comment.innerText = "Есть какие-то замечания или нерешенные вопросы?"
    break
  case 1:
    ask_comment.innerText = "Хотите что-то особенно отметить?"
    break
  default:
    return navigate('question', '3n')
  }
  navigate('question', 2)
}

function answer2(value) {
  done = true
  switch (v_total) {
  case -1:
    if (brand != 'Мыс' && brand != 'ТКЗ') {
      danke.innerText = "Спасибо за ваше мнение. Ваши замечания я зафиксировала и передам руководителю для устранения."
    }else{
      danke.innerText = "Спасибо за ваш отзыв. Нам очень приятно работать для вас."
    }
    break
  case 0:
    if (brand != 'Кузовной' && brand != 'КР'){
      if (brand != 'Мыс' && brand != 'ТКЗ' && brand.indexOf('СТО')+1 == 0 ) {
        danke.innerText = "Спасибо за ваше мнение. Мы учтем ваши замечания."
      }else{
        danke.innerText = "Спасибо за ваш отзыв. Нам очень приятно работать для вас."
      }
    }
    if (disp_moscow.length) {
      moskow.style.display = ''
    }
    break
  case 1:
  if (brand != 'Кузовной' && brand != 'КР'){
    if (brand != 'Мыс' && brand != 'ТКЗ' && brand.indexOf('СТО')+1 == 0 ) {
        danke.innerText = "Спасибо за ваш отзыв. Нам очень приятно работать для вас."
    }else{
        danke.innerText = "Спасибо за ваш отзыв. Нам очень приятно работать для вас."
      }
    }
    if (disp_moscow.length) {
      moskow.style.display = ''
    }
    break
  default:
    return navigate('question', '3n')
  }
  navigate('question', '3')
}

function getTZK(n) {
  v = $('input[name=tzk'+n+']:checked').val()
  if (v == "-1") return -1
  if (v == "1")  return 1
  return 0
}

function send_results(stat) {
   //if (!dnd) return

  //dnd = false
  //cb_call_down(RTC)

  if (stat == 'no_answer') {
    if (done) stat = 'success'
    else no_answer = true
  }

  dt = recall_d.value + "T" + recall_t.value + ":00Z"
  if (dt == "T:00Z") dt = undefined
  data = {
    zn_ref:       zn_ref.value,
    v_total:      v_total,
    dt_postpone:  dt,
    result:       stat,
    g_number:     g_number,

    v_mp:         getTZK(1),
    v_dep:        getTZK(2),
    v_time:       getTZK(3),
    v_parts:      getTZK(4),
    v_re:         getTZK(5),
    v_price:      getTZK(6),
    v_other:      getTZK(7),

    comment:      comment.value,

    user:         localuser,
    c_phones:     c_phones,
    recs:         recs,
    fio:          ka.innerText,
    fb_ref:       fb_ref,
  }
  //console.log(data)
  //return

  if(zn_ref.value.length)
    ws_send('ones_feedback', data)

  goAway = false
  result_sent = true
  location.reload()
}

function create_element(data){
  master_el = ''

  for (i = 0; i < data.length; i++){
    d_elem = data[i]
    tag = document.createElement(d_elem['tag'])
    for(a in d_elem['attr']){
      tag.setAttribute(a,d_elem['attr'][a])
    }
    tag.style.cssText = d_elem['css']
    try{
      tag.innerHTML = d_elem['text']
    }catch(error){}
    if (i == 0)
      master_el = tag
    else
      master_el.appendChild(tag)
  }
  return master_el
}

/*
structure_nps_service = {}
btn_dng = {}
btn_dng.text   = 'DANGER'
btn_dng.action = ''
btn_dng.method = 'alert("DANGER")'
btn_dng.color  = 'btn-danger'

btn_wrn = {}
btn_wrn.text   = 'WARNING'
btn_wrn.action = ''
btn_wrn.method = 'alert("WARNING")'
btn_wrn.color  = 'btn-warning'

btn_scs = {}
btn_scs.text   = 'SUCCESS'
btn_scs.action = ''
btn_scs.method = 'alert("SUCCESS")'
btn_scs.color  = 'btn-success'

s = structure_nps_service
s.step0 = {}
s.step0.text = []
s.step0.buttons = []
s.step0.buttons.push(btn_dng.assign())
s.step0.buttons[0].text   = 'Пропустить'
s.step0.buttons[0].method = 'skip()'
*/

structure_nps_service = {
  step0: {
    text            : [],
    buttons: {
      'Пропустить'  : [ "skip()",           "btn-danger" ]
    }
  },
  step1: {
    text            : [
      ['label',   "io"],
    ],
    buttons: {
      'Да'          : [ "step2",            'btn-success' ],
      'Нет'         : [ "step1n",           'btn-danger' ],
      'Недоступен'  : [ "not_available()",  'btn-danger' ],
    }
  },
  step2: {
    text            : [
      ['label',   "label1"],
      ['label',   "label2"],
      ['label',   "now",    "Вам сейчас удобно разговаривать?"],
    ],
    buttons: {
      'Да'          : [ "step3",            'btn-success' ],
      'Нет'         : [ "step2n",           'btn-danger' ],
    }
  },
  step3: {
    text            : [
      ['label',   "question1"],
      ['text',    "pause",  "Сделайте небольшую паузу (1-2 секунды), чтобы клиент вспомнил время покупки."],
    ],
    buttons: {
      'Да'          : [ "step4",            'btn-success' ],
      'Был другой'  : [ "step3o",           'btn-warning' ],
      'Нет'         : [ "not_client()",     'btn-danger' ],
    }
  },
  step4: {
    text            : [
      ['label',   "question2"],
      ['text',    "choose", "Оцените возможность рекомендации в баллах от 1 до 10."],
    ],
    buttons: {
      'Нет'         : [ "step5_0",          'btn-danger' ],
      'Не знаю'     : [ "step5_1",          'btn-warning' ],
      'Да'          : [ "step5_2",          'btn-success' ],
    }
  },
  step5: {
    text            : {
      0             : [
        ['label', "why",    "Можете пояснить, что стало причиной столь низкой оценки?"],
      ],
      1             : [
        ['label', "why",    "Есть какие-то замечания или нерешенные вопросы?"],
      ],
      2             : [
        ['label', "why",    "Хотите что-то особенно отметить?"],
      ],
      after         : [
        ['text',  "todo",   "Заполнить таблицу критериями. Внести комментарий."],
        ['text',  "",       "<br>"],
        ['text',  "",       "<br>"],
        ['text',  "",       "<br>"],
        ['text',  "wait",   "Дождаться окончания пояснений клиента."],
        ['label', "ustd",   "Поняла вас, хотите еще что то добавить?"],
      ],
    },
    table: 'TZK',
    buttons: {
      'Далее'       : [ "step6",          'btn-success' ],
    }
  },
  step6: {
    text            : [
      ['label',   "io"],
      ['label',   "danke"],

      ['label',   "moscow", "Возможно Вам позвонят, или пришлют анкету из головного представительства {brand}"],
      ['label',   "moscow", "г.Москва для того, чтобы оценить качество нашей работы."],
      ['label',   "moscow", "Это очень важно для нас. Поэтому просим Вас поставить наивысшую оценку."],

      ['label',   "danke2", "Спасибо за уделённое время."],
      ['label',   "holiday"],
      ['label',   "bye",    "Всего хорошего, до свидания!"],
    ],
    buttons: {
      'Завершить'   : [ "finish()",         'btn-success' ],
    }
  },
  // Альтернативные ветки
  step0n: {
    buttons: {
      'Не доступен' : [ "not_available()",  "btn-danger" ]
    }
  },
  step1m: {
    text            : [
      ['label',   "how",    "Как я могу обращаться к вам?"],
      ['input',   "c_name"],
    ],
    buttons: {
      'Далее'       : [ "not_available()",  "btn-success" ],
      'Никак!'      : [ "nohow()",          "btn-danger" ]
    }
  },
  step1n: {
    text            : [
      ['label',   "",    "Вас беспокоит управляющая компания Автоград, отдел по работе с клиентами!"],
      ['label',   "",    "Меня зовут {u}!"],
      ['label',   "",    "С кем я могу переговорить по качеству обслуживания автомобиля {brand}?"],
    ],
    buttons: {
      'Со мной'           : [ "step2",          'btn-success' ],
      'Сейчас позову'     : [ "step1m",         'btn-success' ],
      'Не сейчас'         : [ "step2n",         'btn-warning' ],
      'Ошиблись номером'  : [ "not_client()",   "btn-danger"  ]
    }
  },
  step2n: {
    text            : [
      ['label',   "how",    "В какое время я могу перезвонить?"],
      ['input',   "recall_d", "date"],
      ['input',   "recall_t", "time"],
    ],
    buttons: {
      'Удобно'            : [ "step3",          'btn-success' ],
      'Отложить'          : [ "postpone()",     'btn-warning' ],
      'Никогда'           : [ "nohow()",        "btn-danger"  ]
    }
  },
  step3n: {
    text            : [
      ['label',   "",       "Приношу свои извинения за доставленное неудобство."],
      ['label',   "",       "<br>"],
      ['label',   "holiday"],
      ['label',   "bye",    "Всего хорошего, до свидания!"],
    ],
    buttons: {
      'Завершить'   : [ "finish_n()",         'btn-danger' ],
    }
  },
  step3o: {
    text            : [
      ['label',   "",    "Имя"],
      ['input',   "3o_name"],
      ['label',   "",    "Телефон"],
      ['input',   "3o_phone"],
    ],
    buttons: {
      'Далее'       : [ "update_inf()",     "btn-success" ],
      'Завершить'   : [ "not_been()",       "btn-danger"  ]
    }
  },
}

function nps_fill_templates() {
}

function nps_add_textelem(t) {
  if (t.length == 2 || t.length == 3)
    type = t[0]
  else
    type = 'div'

  elem = document.createElement(type)
  elem.classList.add   ('tmodule')

  //if (t.length == 3) {
  elem.id = t[1]
  elem.innerHTML = t[2]
  //} else {
  //  elem.innerHTML = t[1]
  //}

  step_text.appendChild(elem)
  step_text.appendChild(document.createElement('br'))
}

function nps_navigate(step, kind) {
  var s
  s = structure_nps_service[step]
  console.log(step, kind, s)
  form.style.display = ""

  switch (s.table) {
    case 'TZK':
      step_body.classList.remove('col-md-12')
      step_body.classList.add('col-md-8')
      step_container.appendChild(tzk)
      //tzk.style = "margin: 0px; padding: 0px"
      tzk.style.display = ''
      break;
    default:
      step_body.classList.remove('col-md-8')
      step_body.classList.add('col-md-12')
      tzk.style.display = 'none'
  }

  step_text.innerHTML = ""
  step_buttons.innerHTML = ""
  if (s.text)
    try {
      s.text.forEach(function(t) { nps_add_textelem(t) })
    } catch (e) {
      var before = []
      var body = []
      var after = []
      for (k in s.text) {
        if (k == 'before') before = s.text[k]
        if (k == 'after')  after = s.text[k]
        if (k == kind)     body = s.text[k]
      }
      before.forEach(function(t) { nps_add_textelem(t) })
      body.forEach(function(t) { nps_add_textelem(t) })
      after.forEach(function(t) { nps_add_textelem(t) })
    }
  if (s.buttons)
    for (k in s.buttons) {
      b = s.buttons[k]
      //console.log(b)
      elem = document.createElement('span')
      elem.classList = 'btn-lg btn btn-nps'
      elem.classList.add(b[1])
      if (b[0].indexOf('(') != -1) {
        elem.onclick  = function(e) { eval(b[0]) }
        elem.setAttribute('onClick', b[0])
      } else {
        // nps_navigate
        sub = b[0].indexOf('_')
        if (sub != -1)
          elem.setAttribute('onClick', 'nps_navigate("' + b[0].substring(0, sub) + '", "' + b[0][sub+1] + '")')
        else
          elem.setAttribute('onClick', 'nps_navigate("' + b[0] + '")')
      }
      elem.innerHTML = k
      step_buttons.appendChild(elem)
    }
    //fill_tmodule()
}
