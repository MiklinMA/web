/*
 * DIALOG
 */
function diagramInfo(th) {
	var name = th.data('name');
	var date = th.data('date');
	var time = th.data('time');
	var id = th.data('id');
	var type = th.data('type');
	var nameEmployee = th.data('nameemployee');
	var idEmployee = th.data('idemployee');

    if (type === 'closed' || type === 'order') {
        dialog = document.getElementById('dialog_close_deal')
        page = 'order-finish'
        title = 'Выдача'
	} else {
        dialog = document.getElementById('dialog_record_info')
        page = 'client-order'
        title = 'Клиент'
	}

    $(dialog).find('.c_name').html(name)
    $(dialog).find('.e_date').html(date)
    $(dialog).find('.e_time').html(time)

    $(dialog).find('.button.action_do').off()
        .click( (e) => front_page(`${page}_${id}_${time}_${idEmployee}_${date}_${nameEmployee}`, `${title}: ${name}`) )

    $(dialog).dialog();
}

/*
 * DIALOG
 */
function diagramCreate(period) {
    console.log("Diagram Create", period)

	var date = th.data('date');
	var employee = th.data('employee');
	var name = th.data('name');
	var time = th.data('time');

    dialog = document.getElementById('dialog_create_record')

    page = 'client-koles'
    title = 'Создание записи'

    $(dialog).find('.m_name').html(page)
    $(dialog).find('.e_date').html(date)
    $(dialog).find('.e_time').html(time)

    $(dialog).find('.button.action_do').off()
        .click( (e) => front_page(`${page}_${date}_${time}_${employee}`, `${title}: ${name}`) )

    $(dialog).dialog();
}

/*
 * DIALOG
 */
function diagramChangeDate(th, pageId) {
	var page = document.getElementById(pageId);
	find_el_in_page(page, '.checkedTime').addClass('active');
	find_el_in_page(page, '.notcheckedTime').removeClass('active');
	var date = th.data('date');
	var employee = th.data('employee');
	var time = th.data('time');
	th.parent().parent().find('.line .el.checked').removeClass('checked');
    th.addClass('checked');
    find_el_in_page(page, 'input[name="checkedDate"]').val(date);
    find_el_in_page(page, 'input[name="checkedTime"]').val(time);
    find_el_in_page(page, 'input[name="checkedTimeShow"]').text(time);
    find_el_in_page(page, 'input[name="checkedMaster"]').val(employee);
}

/*
 * DIALOG
 */
function changeMaster (th, page, args) {
	var page = document.getElementById(page.Id);
	var date = th.data('date');
	var employee = th.data('employee');
	var employee_name = th.data('name');
	var time = th.data('time');

	console.log('time: ' + time);

	if (confirm("Хотите изменить запись на "+ time +" к мастеру "+ employee_name +"?")) {
		th.parent().parent().find('.line .el.checked').removeClass('checked');
	    th.addClass('checked');
	    find_el_in_page(page, 'input[name="checkedDate"]').val(date);
	    find_el_in_page(page, 'input[name="data-reg"]').text(date);
	    find_el_in_page(page, 'input[name="checkedTime"]').val(time);
	    find_el_in_page(page, 'input[name="time-reg"]').text(time);
	    find_el_in_page(page, 'input[name="checkedMaster"]').val(employee);
	    find_el_in_page(page, 'input[name="name-master"]').text(employee_name);

  	// load();
  	// $.getJSON("/api_upd_empl_record_log?r_ref="+ref+"&e_ref="+employee+"&date="+date+"&time="+time, function (data) {
		// 	buildDiagram();
		// 	unload();
		// 	// front_page_close(page.id);
    	// });
  // }
        load();
		api_call('api_upd_empl_record_log', { r_ref: ref, e_ref: employee, date: date, time: time }).then(data => {
            buildDiagram(args);
            unload();
        });
		// alert('Форма отправлена по кнопке Отправить в попапе ПСК');


	}
}

