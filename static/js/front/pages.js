class Diagram {
    constructor(page) {
        this.page = page
        this.el = page.el.querySelector('#diagram')
    }

    get_period(period) {
        // selected = selected && Math.floor(this.page.selected.time / this.step) * this.step == period.time
        let record = this.records.find( r => { if (period.m_ref === r.m_ref) return true })
        if (record) {
            let t = Number(Math.floor(period.time / this.step) * this.step)
            return record.periods[t] 
        }
    }

    build(cur_date) {
        //let offset = new Date().getTimezoneOffset() * 60 * 1000
		let offset = 0
        // Если число - пропускаем
        if (cur_date && typeof cur_date === 'string') {
            // Если форматная строка - парсим
            cur_date = Date.parse(cur_date)
        } else if (typeof cur_date !== 'number') {
            // иначе устанавливаем текущую
            if (this.page.selected) {
                cur_date = this.page.selected.time
            } else {
                cur_date = new Date().getTime() - offset
            }
        }

        console.log(this.page.selected)
        console.log('Построение диаграммы', this.page.id, new Date(cur_date))

        this.el.querySelector('#date_diagram').innerText  = new Date(cur_date).toLocaleDateString()
        this.el.querySelector('input#is_guarantee').checked = this.page.guarantee

        api_call('get_records', {
                d_ref: $.cookie('d_ref'),
                date: cur_date,
                is_guarantee: this.page.guarantee,
            }
        ).then( d => {
            this.step   = d.timeAcceptions * 1000 * 60;
            let mstart = Date.parse(new Date(cur_date - offset).toISOString().substr(0, 11) + d.startTimeWork);
            let mstop  = Date.parse(new Date(cur_date - offset).toISOString().substr(0, 11) + d.endTimeWork);
            let count  = Math.ceil((mstop - mstart) / this.step);
            this.width  = 100 / Math.fround(count);
            console.log(new Date(cur_date).toISOString().substr(0, 11))

            this.records = []
            let el_masters = this.el.querySelector("#masters")
            el_masters.innerHTML = ""

            this.periods = {}
            let el_periods = this.el.querySelector("#els-scala")
            el_periods.innerHTML = ''

            for (let i = 0; i < count; i++) {
                let ts = mstart + i * this.step;
                let te = mstart + (i+1) * this.step;
                te = new Date(te);

                let t = new Date(ts);
                this.periods[t.getTime()] = {
                    time: t.getTime(),
                    free: true
                }

                let el = document.createElement('div')
                el.className = 'el'
                el.style.width = this.width + '%'
                el.innerHTML = new Date(t).toLocaleTimeString().slice(0,5)
                el_periods.appendChild(el)
            }

            d.employees.forEach(line => {
                let master = {
                    m_name: line.name,
                    m_ref: line.idEmployee,
                    periods: {}
                }
                Object.keys(this.periods).forEach(k => {
                    master.periods[k] = JSON.parse(JSON.stringify(this.periods[k]))
                    master.periods[k].m_ref = line.idEmployee
                    master.periods[k].m_name = line.name
                })

                let el = document.createElement('div')
                el.className = 'line'
                el.innerHTML = line.name
                el_masters.appendChild(el)

                line.records.forEach(record => {
                    let ps = new Date(record.periodStart.substr(0, 16)).getTime()

                    ps = Math.floor(ps / this.step) * this.step

                    if (master.periods[ps]) {
                        master.periods[ps].free = false

                        if (master.periods[ps].records)
                            master.periods[ps].records.push(record)
                        else
                            master.periods[ps].records = [record]
                    } else {
                        console.warn(new Date(ps), new Date(Number(Object.keys(master.periods)[0])))
                    }
                })

                this.records.push(master)
            })
            console.log("FINALLY", this.records)
            this.update()
            this.after_build(this)
        })
    }

    update() {
        // фрейм ячеек
        let el_records = this.el.querySelector("#records")
        el_records.innerHTML = ''

        // для каждого мастера в массиве
        this.records.forEach(r => {
            let line = document.createElement('div')
            line.className = 'line'
            el_records.appendChild(line)
            
            Object.keys(r.periods).forEach(p => {
                let period = r.periods[p]
                // ЯЧЕЙКА
                let el = document.createElement('div')
                el.className = 'el'
                el.innerHTML = period.free || period.records.length == 1 ? '' : period.records.length
                el.style.width = this.width + '%'

                let selected = this.page.selected
                selected = selected && this.page.selected.m_ref == period.m_ref
                selected = selected && Math.floor(this.page.selected.time / this.step) * this.step == period.time

                this.page.period_behavior(el, period, selected)

                line.appendChild(el)
            })
        })
    }

    after_build(diagram) {
    }
}
/*
 * Массив открытых страниц
 */
let front_pages = {}

/*
 * Класс страницы
 */
class Frame {
    constructor(id, title) {
        console.log("Frame", id, title);

        this.id   = id 
        this.title = title || id
        this.busy = false
        this._active = false

        this.pages = front_pages

        let base = document.getElementById('front_base')

    	let args = id.split('_')
		let url = args[0]

        let onload = 'onload_' + url.replace(/-/, '_')
        console.log(onload)
        if (typeof window[onload] === 'function')
            this.onload = window[onload]
        else
            this.onload = this.onload_default

        this.el = document.createElement('div')
        this.el.id = id
        this.el.style.display = 'none'

        base.appendChild(this.el)
        this.pages[id] = this

        if (url[0] != '/')
            url = '/front/' + url

        $.get(url,
            response => {
                this.el.innerHTML = response
                this.after_init()
            }
        )
    }

    get active() {
        return this._active
    }

    set active(val) {
        console.log("Активация фрейма", this.id)

        Object.keys(this.pages).forEach(id => {
            let el = document.getElementById(id)
            el.style.display = 'none'
        })

        this._active = Boolean(val)

        this.el.style.display = this._active ? '' : 'none'
    }

    onload_default() {
        console.warn("Загрузка страницы НЕ ПЕРЕГРУЖЕНА!", this.id)
    }

    after_init() {
        this.diagram = new Diagram(this)

        let checkbox = this.el.querySelector('input#is_guarantee')
        if (checkbox) {
            checkbox.onchange = () => {
                this.guarantee = !this.guarantee
                this.diagram.update()
            }
        }
        console.log("Страница полностью загружена", this.id)
    }

    close() {
        if (this.busy) {
            console.error(`Невозможно закрыть страницу ${this.title}.`)
            return
        }

        this.el.remove()

        var keys = Object.keys(this.pages)
        var next = keys.indexOf(this.id)
        next = next -= 1 ? next : 0
        delete this.pages[this.id]
        next = this.pages[keys[next]]
        next.active = true
        return true
    }

    period_behavior(el, period) {
        if (!period.free)
            if (this.guarantee) {
                el.classList.add('active', period.isGarantee ? 'garantee' : 'pay')
                if (period.records.length == 1)
                    el.classList.add('active', period.records[0].isGarantee ? 'garantee' : 'pay')
                else
                    el.classList.add('active')
                    period.records.forEach(r => {
                        el.classList.add(r.isGarantee ? 'garantee' : 'pay')
                    })
            } else {
                if (period.records.length == 1)
                    el.classList.add('active', period.records[0].type)
                else
                    el.classList.add('active')
                    period.records.forEach(r => {
                        el.classList.add(r.type)
                    })
            }
    }
}

class Page extends Frame {
    constructor(id, title, period) {
        super(id, title)
        console.log("Page", id, title);

        this.tabs = document.getElementById('tabs')
        this.tab = document.createElement('a')
        this.tab.id = 'tab_' + id
        this.tab.href = '#'
        this.tab.innerText = title
        this.tab.style.display = 'none'
        this.tab.onclick = (function () {
            this.active = true
        }).bind(this)

        var x = document.createElement('i')
        x.className = 'fa fa-times'
        x.attributes['aria-hidden'] = true
        x.onclick = (function () {
            this.close()
        }).bind(this)

        this.tab.appendChild(x)
        this.tabs.appendChild(this.tab)

        this.records = undefined
        this._selected = period
        this.guarantee = false

        /*
        {
            date: undefined,
            time: undefined,
            m_ref: undefined,
        }
        */
    }

    get selected() {
        return this._selected
    }

    set selected(val) {
        console.log("SELECTED", val)
        this._selected = val
        if (this.el.querySelector('.hideShowBlocks'))
            if (this._selected) {
                this.el.querySelector('.checkedTime').classList.add('active')
                this.el.querySelector('.notcheckedTime').classList.remove('active')
            } else { 
                this.el.querySelector('.checkedTime').classList.remove('active')
                this.el.querySelector('.notcheckedTime').classList.add('active')
            }
    }

    set active(val) {
        super.active = val

        console.log("Активация страницы", this.id)

        Array.from(this.tabs.children).forEach(tab => {
            tab.classList.remove('active')
        })
        this.tab.style.display = ''

        if (this._active)
            this.tab.classList.add('active')
        else
            this.tab.classList.remove('active')
    }

    after_init() {
        super.after_init()
        this.selected = this._selected
        this.onload(this.selected)
    }

    close() {
        if (super.close()) {
            this.tab.onclick = null // чтобы остановить цепочку событий
            this.tab.remove()
            return true
        }

    }

    period_behavior(el, period, selected) {
        super.period_behavior(el, period)
        if (selected) {
            el.style.border = "3px blue solid" // TODO убрать в CSS
        } else if (period.free) {
            el.onclick = (() => onclick_page_period_free(this, period)).bind(this)
        }
    }
}

class Dashboard extends Frame {
    constructor(id, title) {
        super(id, title)
        console.log("Dashboard", id, title);

        this.tabs = document.getElementById('tabs')
        this.tab = document.getElementById('tab_dashboard')
        if (!this.tab) {
            this.tab = document.createElement('a')
            this.tab.id = 'tab_dashboard'
            this.tab.href = '#'
            this.tab.innerText = title
            this.tab.style.display = 'none'
            this.tab.onclick = (function () {
                this.active = true
            }).bind(this)
            this.tabs.appendChild(this.tab)
        }

    }

    set active(val) {
        super.active = val

        console.log("Активация дашборда", this.id)

        Array.from(this.tabs.children).forEach(tab => {
            tab.classList.remove('active')
        })
        this.tab.style.display = ''
        this.tab.innerText = this.title

        if (this._active)
            this.tab.classList.add('active')
        else
            this.tab.classList.remove('active')
    }

    after_init() {
        super.after_init()
        this.onload()
    }

    close() {
        console.warn("Нельзя закрыть дашборд!")
    }

    record_change() {
        console.log('change')
    }

    period_behavior(el, period) {
        super.period_behavior(el, period)
        if (period.free) {
            el.onclick = () => onclick_dashboard_period_free(period)
        } else {
            let record = period.records[0]
            let type = record.type

            if (type === 'closed' || type === 'order') {
                el.onclick = () => onclick_dashboard_period_close(period)
            } else {
                if (period.length > 1)
                    el.onclick = () => onclick_dashboard_period_busy_arr(period)
                else
                    el.onclick = () => onclick_dashboard_period_busy(period)
            }
        }
    }
}

front = new Dashboard('front', 'Рабочий стол')
front.active = true

/*
 * Общие функции закрытия/открытия страниц
 */

// Загрузка и открытие новой формы
function front_page(page, title, visible, dashboard, period) {
    if (Object.keys(front_pages).indexOf(page) != -1) {
        front_pages[page].active = true
        return page
	} else {
        Kind = dashboard ? Dashboard : Page
        p = new Kind(page, title, period)
        if (visible !== false)
            p.active = true
        return p
    }
}

// Закрытие формы
function front_page_close(page) {
    front_pages[page].close()
}

/*
 * Функции для загрузки форм
 */

function update_events() {
    api_call('get_events').then(data => { listEvents(data); });
}


// Рабочий стол
function onload_front() {

    this.diagram.after_build = (diagram) => {
        let margin = 102 + diagram.el.offsetHeight
        let height = innerHeight - margin + 'px'
        console.log("RESIZE:", innerHeight, margin, height)
        front.el.querySelectorAll(' main .wrap .events .list').forEach(el => {
            el.style.height = height
        })
    }

    this.diagram.build()
    /*
	buildDiagram()


	setInterval(function() {
        buildDiagram()
	}, 600000);
    */

	update_events();

	var paramsGET = window
    .location
    .search
    .replace('?','')
    .split('&')
    .reduce(
        function(p,e){
            var a = e.split('=');
            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
        {}
    );


    if (paramsGET['idRecord']) {
        api_call('get_record', { r_ref: paramsGET['idRecord'] }).then(data => {
            showRecord(data);
        });
    }

	$('#menu li a, .scrollTo').click(function(event) {
		event.preventDefault();
		var target = $(this).attr('href');
		var top = $(target).offset().top;
		$('html, body').animate({scrollTop : top+'px'}, 1000);
	});

	var owl = $(".recalls ul");
	if (owl.length) {
		owl.owlCarousel({
			items : 1,
			pagination: true,
			autoHeight: true,
			navigation: true,
    		mouseDrag: false,
    		touchDrag: true,
    		loop: true,
    		smartSpeed: 800,
		});
	}
}

// Планировщик
function onload_calendar(args) {
	if ($('#calendar-box').length) {
        $('#calendar-box').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'listMonth,agendaDay,agendaWeek,month'
            },
            defaultView: 'listMonth',
			// views: {
			// 	timelineDay: {
			// 		buttonText: ':15 slots',
			// 		slotDuration: '00:15'
			// 	},
			// 	timelineTenDay: {
			// 		type: 'timeline',
			// 		duration: { days: 10 }
			// 	}
			// },

            // defaultDate: new date.now(),
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: {
                url: '/api_get_tasks',
                error: function () {
                    $('#script-warning').show();
                    console.warn("Ошибка");
                }
            },
            loading: function (bool) {
                $('#loading').toggle(bool);
                console.warn("Loading");
            },
            // Documentation - https://fullcalendar.io/docs/

            // Click on the item
            eventClick: function(calEvent, jsEvent, view) {

		        alert('Event: ' + calEvent.title);
		        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
		        alert('View: ' + view.name);

		        // change the border color just for fun
		        $(this).css('border-color', 'red');

		    },
		    // Drop the item
		    eventDrop: function(event, delta, revertFunc) {

		        alert(event.title + " was dropped on " + event.start.format());

		        if (!confirm("Are you sure about this change?")) {
		            revertFunc();
		        }

		    }
        });
    }
}

// Страница в разработке
function onload_clear(args) {
    
}

// Заезд клиента
function onload_order() {
    // console.log(args)
    // ref = args[1]
    //
    // var page = document.getElementById(args.join("_"));
    //
    // close_dialog()

    onload_form(this)
    var form = $(this.el).find('form');

    ref = this.ref;

    showClientCall(this)

    //api_call('get_record', { r_ref: ref }).then(data => showClientCall(data, page) );

    $(page).find('#orderToMaster').submit(function(event) {
    	event.preventDefault();
    	var form = $(this);
    	load();
    	$.ajax({
			type: "GET",
			url: "/api_set_new_record_log", // API
			data: form.serialize()
		}).done(function() {
		    unload();
			// Код после отправки формы
	    }).fail(function() {
		    unload();
			alert( "Ошибка при отправке формы" );
		});
    	// alert("Сработала форма Передать мастеру");
    });

    $(page).find('#orderToMaster input[type="checkbox"]').change(function(event) {
        var checked = true;
        $(page).find('#orderToMaster input[type="checkbox"]').each(function(index, el) {
            if (!$(this).prop('checked')) checked = false;
            console.log($(this).prop('checked'));
        });
        $(page).find('#orderToMaster .button').prop('disabled', !checked);
    });
}

// Выдача автомобиля
function onload_order_finish(args) {
    onload_form(this)
    var form = $(this.el).find('form');

    ref = this.selected.records[0].idRecord

    close_dialog()

    console.log("onload_order_finish")
    showClientCall(this);

    // api_call('get_record', { r_ref: ref }).then(data => showClientCall(data, this.id) );

    var form = $('#orderExtradition');

    form.find('#openPDF_ZN').click(function (e) {
    	e.preventDefault();
        api_call('get_print_document', { r_ref: ref }).then(data => window.open(data, '_blank') );
    });

    form.submit(function(event) {
        event.preventDefault();
        load();
        api_call('', { date: form.serialize() }).then(data => {
            unload();
        });
    });

    form.find('.psk').click(function(event) {
        event.preventDefault();
        var fid = form.attr('id');

        dialog = document.getElementById('dialog_plan_contact')

        $(dialog).find('input[name="type"]').attr('form', fid);
        $(dialog).find('#date').attr('form', fid)
        $(dialog).find('#date').val(new Date().toISOString().slice(0, 10))
        $(dialog).find('#time').attr('form', fid)
        $(dialog).find('#time').val(new Date().toLocaleTimeString().slice(0,5))
        $(dialog).find('#comment').attr('form', fid)

        $(dialog).find('.button.action_do').off()
            .click( (e) => {
                close_dialog()
                form.submit() 
            })

	    $(dialog).dialog();
    });

    // Click to the button Принять оплату
    form.find('.getPayment').click(function(event) {
    	event.preventDefault();
    	load();
    	$.ajax({
			type: "GET",
			url: "/api_set_new_doc_payment", // API
			data: form.serialize()
		}).done(function() {
		    unload();
			// Код после отправки формы
		});
		//alert('Форма отправлена по кнопке Принять оплату');
    });

    // Click to the button Возврат на ремзону
    form.find('.return').click(function(event) {
    	event.preventDefault();
    	load();
    	$.ajax({
			type: "GET",
			url: "/api_upd_record_status_return", // API
			data: form.serialize()
		}).done(function() {
		    unload();
			// Код после отправки формы
		});
		//alert('Форма отправлена по кнопке Возврат на ремзону');
    });
}

$('.open-dialog').click(function(event) {
    event.preventDefault();
    var href = $(this).attr('href');
    $(href).dialog();
});

/*
 * Загрузка общих элементов форм
 * Журнал, цены, рекомендации
 */
function onload_form(page) {
    console.log('Onload form', page && page.id)

    if (!page) return


    function find_el(id) {
        return $(page.el).find(id)[0] && $(page.el).find(id).first() || $(id)
    }

    close_dialog()

    find_el('#recomend').click(function(event) {
        event.preventDefault();
        find_el('#recomend-list-wrap').dialog();
    });

    find_el('#select-master-wrap .button').click(function(event) {
        event.preventDefault();
        var inp = find_el('#select-master input:checked');
        var value = inp.val();
        alert('Выбрали мастера - ' + value);
        // $.ajax({
        //     type: "GET",
        //     url: "/", // API
        //     data: form.serialize()
        // }).done(function() {

        // });
    });

    find_el('.receive_kc').click(function(event) {
        event.preventDefault();
        alert('Вернуть в кц');
    });

    $('.find-info#auto + .del').click(function(event) {
        event.preventDefault();
        $(this).prev().prop('disabled', false).val('').focus();
        find_el('#carId').val('');
    });

    $('.find-info#model + .del').click(function(event) {
        event.preventDefault();
        $(this).prev().prop('disabled', false).val('').focus();
        find_el('#modelId').val('');
    });

    $('.find-info#name + .del').click(function(event) {
        event.preventDefault();
        $(this).prev().prop('disabled', false).val('').focus();
        find_el('#idCustomer, #idContragent').val('');
    });

    $('.find-info#tel + .del').click(function(event) {
        event.preventDefault();
        $(this).prev().prop('disabled', false).val('').focus();
    });

    $('.psk').click(function(event) {
        event.preventDefault();

        var form = find_el('form:not(.action_do)');
        var fid  = form.attr('id');

        dialog = document.getElementById('dialog_plan_contact')

        $(dialog).find('#date').attr('form', fid)
        $(dialog).find('#date').val(new Date().toISOString().slice(0, 10))
        $(dialog).find('#time').attr('form', fid)
        $(dialog).find('#time').val(new Date().toLocaleTimeString().slice(0,5))
        $(dialog).find('#comment').attr('form', fid)
        $(dialog).find('input[name="type"]').attr('form', fid);

        $(dialog).find('.button.action_do').off()
            .click( (e) => {
                close_dialog()
                form.submit()
            })

	    $(dialog).dialog();
    });

    $('.out').click(function(event) {
        event.preventDefault();

        var form = find_el('form:not(.action_do)');
        var fid  = form.attr('id');

        dialog = document.getElementById('dialog_out')

        $(dialog).find('#comment').attr('form', fid);
        $(dialog).find('input[name="type"]').attr('form', fid);

        $(dialog).find('.button.action_do').off()
            .click( (e) => {
                close_dialog()
                form.submit() 
            })

        $(dialog).dialog();
    });
    /*
    if (args.length == 6) {
          buildDiagram(args)
      }

    else if (args.length > 3) {
        var dtArr = args[1].split('.');
        var dt = dtArr[2]+'-'+dtArr[1]+'-'+dtArr[0];
        buildDiagram(args, dt)
    }
    else
        buildDiagram(args)
    */

    cur_date = new Date();
    date = cur_date.getDate();

    // Получение времени для кнопки Сегодня
    api_call('get_freetime', { date: cur_date.getTime() }).then(data => {
        for (var i = 0; data && i < data.length; i++) {
            var timeStart = data[i].timeStart;
            var timeEnd = data[i].timeEnd;
            var employee = data[i].employee;
            var date = data[i].date;
            if (i == 0)
                var butt = find_el('.button.today');
            else
                var butt = find_el('.button.tomorrow');
            console.log(butt);
            butt.find('span').text(timeStart);
            butt.attr('data-employee', employee);
            butt.attr('data-date', date);
            butt.attr('data-timeEnd', timeEnd);
            butt.attr('data-timeStart', timeStart);
        }
    });

    // var tomorrow = cur_date + 1000*60*60*24;

    api_call('get_service_crm').then(services => {
        find_el('#price-list').click(function(event) {
            event.preventDefault();
            fill_services(services);
            find_el('#price-list-cont').dialog();
        });

        find_el('#price-list-cont input[type="search"]').keyup(function(event) {
            var val = $(this).val();
            find_services(find_el('#price-list-cont #list-services li'), val);
        });
    });

    api_call('get_objections').then(data => {
    	find_el('#deny-list').text('');
        for (var i = 0; i < data.length; i++) {
            var objection = data[i]['objection'];
            var answer = data[i]['answer'];
            find_el('#deny-list').append('<li><a href="#" onclick="toggleP(this)">'+objection+'</a><p>'+answer+'</p></li>');
        }
        find_el('#deny').click(function(event) {
            event.preventDefault();
            find_el('#deny-list-wrap').dialog();
        });
    });

    api_call('get_spec_offers').then(data => {
        var num = data.length;
        find_el('#numSpec').text(num);
        find_el('#spec-list').text('');
        for (var i = 0; i < num; i++) {
            var name = data[i]['name'];
            var comment = data[i]['comment'];
            find_el('#spec-list').append('<li><a href="#" onclick="toggleP(this)">'+name+'</a><p>'+comment+'</p></li>');
        }
        find_el('#spec').click(function(event) {
            event.preventDefault();
            find_el('#spec-list-wrap').dialog();
        });
    });

    api_call('get_promises').then(data => {
        for (var i = 0; i < data.promises.length; i++) {
            var name = data.promises[i]['promise'];
            var val = data.promises[i]['ref'];
            find_el('#list-promises').append('<li><label><input type="checkbox" name="promises" value="'+ val +'"/> '+ name +'</label></li>');
        }
    });


    api_call('get_scripts_talk').then(data => {
        for (var i = 0; i < data.length; i++) {
            var name = data[i]['promise'];
            var val = data[i]['ref'];
            var comment = data[i]['comment'];
            var know = 'know';
            // var know = (data[i]['know']) ? 'know' : 'notKnow';

            if(data[i]['variant']==1){
                know = 'notKnow';
            }

            find_el('#list-scripts').append('<li class="'+ know +'"><label><input type="checkbox" name="scripts" value="'+ val +'"/> '+ comment +'</label></li>');
            // find_el('#list-scripts').append('<li class="know"><label><input type="checkbox" name="scripts" value="'+ val +'"/> '+ comment +'</label></li>');
        }
        $('input[name="diagnostik"]').click(function(event) {
            var inputVal = $(this).val();
            console.log(inputVal);
            if (inputVal == 'know')
                find_el('#list-scripts').removeClass('show-notknow').addClass('show-know');
            else
                find_el('#list-scripts').removeClass('show-know').addClass('show-notknow');
        });
    });

    find_el('#auto.find-info').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
            var val = $(this).val();
            var idContragent = find_el('#idContragent').val();
            var idCustomer = find_el('#idCustomer').val();
            if (val.length >= 3 || idContragent) {
                find_el('#list-cars').text('');
                api_call('get_car', {
                        lim: 50,
                        number: val,
                        c_ref: idContragent,
                        idCustomer: idCustomer
                    }
                ).then(data => {
                    for (var i = 0; i < data['cars'].length; i++) {
                        var carName = data['cars'][i]['carName'];
                        var idCar = data['cars'][i]['idCar'];
                        find_el('#list-cars').append('<li><a href="#" onclick="checkAutoFromList(this)" data-id="'+ idCar +'">'+ carName +'</a></li>');
                    }
                });
                $(document).mouseup(function (e) {
                    var container = find_el("#list-cars");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        }
    });

    find_el('#name.find-info').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
            var val = $(this).val();
            var carId = find_el('#carId').val();
            var tel = find_el('#tel').val();
            if (val.length >= 3 || carId || tel) {
                find_el('#list-clients').text('');
                api_call('get_client', {
                        lim: 50,
                        name: val,
                        o_ref: carId,
                        phone: tel
                    }
                ).then(data => {
                    for (var i = 0; i < data.length; i++) {
                        var name = data[i]['customerName'] + ' (' + data[i]['contragentName'] + ')' + '(' + data[i]['phone'] + ')';
                        var idContragent = data[i]['idContragent'];
                        var idCustomer = data[i]['idCustomer'];
                        var phone = data[i]['phone'];
                        find_el('#list-clients').append('<li><a href="#" onclick="checkAutoFromListName(this)" data-idContragent="'+ idContragent +'" data-idCustomer="'+ idCustomer+'" data-phone="'+ phone +'">'+ name +'</a></li>');
                    }
                });
                $(document).mouseup(function (e) {
                    var container = find_el("#list-clients");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        }
    });

    find_el('#tel.find-info').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
            var val = $(this).val();
            if (val.length >= 3) {
                find_el('#list-clients-by-tel').text('');
                api_call('get_client', {lim: 50, phone: val}).then(data => {
                    for (var i = 0; i < data.length; i++) {
                        var name = data[i]['customerName'] + ' (' + data[i]['contragentName'] + ')' + '(' + data[i]['phone'] + ')';
                        var idContragent = data[i]['idContragent'];
                        var idCustomer = data[i]['idCustomer'];
                        var phone = data[i]['phone'];
                        find_el('#list-clients-by-tel').append('<li><a href="#" onclick="checkClientByTel(this)" data-idContragent="'+ idContragent +'" data-idCustomer="'+ idCustomer+'" data-phone="'+ phone +'">'+ name +'</a></li>');
                    }
                });
                $(document).mouseup(function (e) {
                    var container = find_el("#list-clients-by-tel");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        }
    });

    find_el('#model.find-info').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
            var val = $(this).val();
            find_el('#list-models').text('');
            if (val.length >= 0) {
                api_call('get_top_models', {number: val}).then(data => {
                    var num = data.length;
                    for (var i = 0; i < num; i++) {
                        var name = data[i]['name'];
                        var ref = data[i]['ref'];
                        find_el('#list-models').append('<li><a href="#" onclick="checkModelFromList(this)" data-id="'+ ref +'">'+ name +'</a></li>');
                    }
                    if (num >= 10) {
                        var li = $('<li><a href="#" style="text-align:center">Показать еще <i class="fa fa-caret-down" aria-hidden="true"></i></a></li>');
                        find_el('#list-models').append(li);
                        li.click(function() {
                            $(this).remove();
                            api_call('get_models', {number: val}).then(data => {
                                for (var i = 10; i < data.length; i++) {
                                    var name = data[i]['name'];
                                    var ref = data[i]['ref'];
                                    find_el('#list-models').append('<li><a href="#" onclick="checkModelFromList(this)" data-id="'+ ref +'">'+ name +'</a></li>');
                                }
                            });
                        });
                    }
                });
                $(document).mouseup(function (e) {
                    var container = find_el("#list-models");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        }
    });

    find_el('.datepicker').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: 0,
        onSelect: (date) => {
            page.diagram.build(date)
            console.log('date', date);
        }
    });
    return page;
}

// ТО
function onload_client() {
    onload_form(this)

    id = this.id;
    var form = $(this.el).find('form');

    var c_name = find_el_in_page(form, '#name');

    // Click to the button Записать
    form.find('.order').click(function(event) {
    	event.preventDefault();
    	load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
		// alert('Форма отправлена по кнопке Записать');
    });

    // Click to the button Дата +30
    form.find('.data30').click(function(event) {
    	event.preventDefault();
    	load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
    });
}

// Звонок клиента
function onload_client_call() {
    this.diagram.build()

    id = this.id;
    var form = $(this.el).find('form');


    // Click to the button Записать
    form.find('.order').click(function(event) {
    	event.preventDefault();
    	load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
    });

    var c_name = find_el_in_page(form, '#name');

  	// Click to the button Сейчас
    form.find('.now').click(function (event) {
        event.preventDefault();
        load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
        // alert('Форма отправлена по кнопке Сейчас');
    });

    // Click to the button Сегодня
    form.find('.today').click(function(event) {
    	event.preventDefault();
    	load();
    	api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
		// alert('Форма отправлена по кнопке Сегодня');
    });

    // Click to the button Завтра
    form.find('.tomorrow').click(function (event) {
        event.preventDefault();
        load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            unload();
            front_page_close(id);
            front_page(`order_${data.r_ref}`, 'Заезд:'+c_name.val());
        });
        // alert('Форма отправлена по кнопке Завтра');
    });

    $('#select-master-wrap .button').click(function(event) {
        event.preventDefault();
        var inp = $('#select-master input:checked');
        var value = inp.val();
        alert('Выбрали мастера - ' + value);
        load();
        api_call('api_upd_empl_buisness_process', {data: form.serialize()+'&employee='+value}).then(data => {
            unload();
            front_page_close(id);
        });
    });
}

// Создание записи
function onload_client_koles(selected) {
    onload_form(this)
    this.diagram.build()

    page = this
    id = this.id;
    var form = $(this.el).find('form');

    find_el_in_page(form, '#select-master-wrap .button').click(function (event) {
        event.preventDefault();
        var inp = find_el_in_page(form, '#select-master input:checked');
        var value = inp.val();
        alert('Выбрали мастера - ' + value);
        load();
        api_call('api_upd_empl_buisness_process', {data: form.serialize()+'&employee='+selected.m_ref}).then(data => {
            unload();
        });
    });

    find_el_in_page(form, '#addNewAuto').submit(function (event) {

        event.preventDefault();
        var _form = $(this);

        var idContragent = form.find('#idContragent').val();
        var name = form.find('#name').val();

        load();
        api_call('api_set_new_auto', {data: _form.serialize() + '&name=' + name + '&idContragent=' + idContragent}).then(data => {
            unload();
            alert("Добавить авто!");
            var carId = data['carId'];
            var auto = data['auto'];
            form.find('#carId').val(carId);
            form.find('.find-info#auto').val(auto).prop('disabled', true);

            close_dialog()
        });
    });

    form.find('.receive_kc').click(function (event) {
        event.preventDefault();
        alert('Вернуть в кц');
    });

    // Отправление формы после заполнения ПСК
    form.submit(function (event) {
        event.preventDefault();
        var res = {};
        $.each(form.serializeArray(), function () {
            res[this.name] = this.value;
        });
        if (res.type == 'psk')
            api = 'api_set_new_task_planned_contact';
        else
            api = 'api_set_buisness_process_to_out';
        load();
        api_call(api, {data: form.serialize()}).then(data => {
            front_page_close(id);
            update_events();
            unload();
            front.diagram.update()
        });
    });

    // Click to the button Записать
    form.find('.order').click(function (event) {
        event.preventDefault();
        load();
        api_call('api_set_new_record_log', {data: form.serialize()}).then(data => {
            r_ref = data.r_ref;
            console.log(data.r_ref);
            front_page_close(id);
            front_page(`client-order_${r_ref}`, "Заявка");
            buildDiagram();
            unload();
        });
    });


    // Click to the button Сейчас
    form.find('.now').click(function (event) {
        event.preventDefault();
        load();
        api_call('api_set_new_record_log', {data: form.serialize() + '&periodStart=' + page.selected.time + '&checkedMaster=' + page.selected.m_ref}).then(data => {
            r_ref = data.r_ref;
            console.log(data.r_ref);
            front_page_close(id);
            p = front_page(`client-order_${r_ref}`, "Заявка");
            p.selected = page.selected

            buildDiagram();
            unload();
        });
    });

    // Click to the button Сегодня
    form.find('.today').click(function (event) {
        event.preventDefault();
        var a = $(this);
        var employee = a.data('employee');
        var date = a.data('date');
        var timeend = a.data('timeend');
        var timestart = a.data('timestart');
        load();
        api_call('api_set_new_record_log', {data: form.serialize() + '&periodStart=' + page.selected.time + '&checkedTime=' + timestart + '&checkedMaster=' + employee}).then(data => {
            r_ref = data.r_ref;
            console.log(data.r_ref);
            front_page_close(id);
            front_page(`client-order_${r_ref}`, "Заявка");

            // front_page_close(page.id);
            // p = front_page(`order_${ref}`, 'Заезд:'+c_name.val());
            p.selected = page.selected

            buildDiagram();

            unload();
        });
        // alert('Форма отправлена по кнопке Сегодня');
    });

    // Click to the button Завтра
    form.find('.tomorrow').click(function (event) {
        event.preventDefault();
        var a = $(this);
        var employee = a.data('employee');
        var date = a.data('date');
        var timeend = a.data('timeend');
        var timestart = a.data('timestart');
        load();
        api_call('api_set_new_record_log', {data: form.serialize() + '&periodStart=' + date + '&checkedTime=' + timestart + '&checkedMaster=' + employee}).then(data => {
            r_ref = data.r_ref;
            console.log(data.r_ref);
            front_page_close(id);
            front_page(`client-order_${r_ref}`, "Заявка");

            buildDiagram();
            unload();
        });
    });
}

// Форма записанного клиента
function onload_client_order() {
    page = this;

    // front.diagram.get_period(this.selected)

    selected = this.selected


	this.diagram.after_build = (function() {
        console.log("AFTER BUILD")
        showClientCall(this);
    }).bind(this)
    
	this.diagram.build()

	onload_form(this)

    var form = $(this.el).find('form');

  	var c_name = find_el_in_page(form, '#name');

    // Click to the button Открыть
    form.find('.open').click(function(event) {
    	event.preventDefault();
    	if (checkForm(form)) {

    	    front.diagram.update()

    	    selected = page.diagram.get_period(selected)

            front_page_close(page.id);
            p = front_page(`order_${selected.records[0].idRecord}`, 'Заезд:'+c_name.val());
            p.selected = page.selected
		}

    });

	// Отправление формы после заполнения ПСК/АУТ
	form.submit(function(event) {
		event.preventDefault();
		var res = {};
        $.each(form.serializeArray(), function() {
            res[this.name] = this.value;
        });
        if (res.type == 'psk')
            api = 'api_set_new_task_planned_contact';
        else
            api = 'api_set_buisness_process_to_out';
	    load();
        api_call(api, {data: form.serialize()}).then(data => {
            update_events();
            front.diagram.build();
            unload();
            front_page_close(page.id);
        });
	});
}
