
/* 
 * Инициализация модуля
 */
function front_onload() {
    loadJS('/static/js/dist/sip.js', function () {
        console.info('Моуль телефонии загружен ( версия:', SIP.version, ')');
        loadJS('/static/js/front/pages.js', function() {
            loadJS('/static/js/front/ws.js', function() {
                ws_connect();
                ws.onmessage = function(e) { front_onmessage(e) };
            })
        })
        loadJS('/static/js/front/phoner.js', function (){ })
    })

    loadJS('/static/js/front/dialog.js', function() { })

    console.info('Моуль фронт-офиса загружен.');
    topline_username.innerHTML = $.cookie('u_name');
}

/*
 * Подтвержение выхода
 */
function front_logout() {
    if (confirm('Вы действительно хотите выйти из рабочего места?')) {
        logout()
    }
}

/*
 * FILL INTERFACE OBJECT
 */
async function listEvents(events) {
    var num = 0;
    
    if (events.urgentEvents)
    	$('#urgentEvents').html("")
        for (var i = 0; i < events.urgentEvents.length; i++) {
            num++;
            var e = events.urgentEvents[i]
            $('#urgentEvents').append(`
              <li class="event_${e.type}">
                <a onClick="eventInfo($(this), event)" href="#"
                    data-id="${e.t_ref}"
                    data-type="${e.type}"
                    data-date="${e.dt}"
                    data-t_ref="${e.t_ref}"
                    >
                    ${new Date(e.dt).toLocaleTimeString().slice(0,5)} ${e.info} ${e.c_name}
                </a>
              </li>
            `);
        }
    if (events.scheduledEvents)
    	$('#scheduledEvents').html("")
        for (var i = 0; i < events.scheduledEvents.length; i++) {
            var e = events.scheduledEvents[i]
            // TODO idClient => idRecord возвращать из API
            // console.log(e)
            $('#scheduledEvents').append(`
              <li class="event_${e.type}">
                <a onClick="eventInfo($(this), event)" href="#"
                    data-id="${e.t_ref}"
                    data-type="${e.type}"
                    data-date="${e.dt}"
                    data-t_ref="${e.t_ref}"
                    >
                    ${new Date(e.dt).toLocaleTimeString().slice(0,5)} ${e.info} ${e.c_name}
                </a>
              </li>
            `);
        }
    if (num > 0) $('#numEvents').text(num);
}

/*
 * FILL INTERFACE OBJECT
 */
function eventInfo(th, e) {
    var id = th.data('id');
    var type = th.data('type');
    var page = `client-call_${id}`;
    var t_ref = th.data('t_ref');
    $.getJSON('/api_get_info_record?t_ref=' + t_ref, function(data) {
        if (data) {
            var employee = data.employee;
            var idRecord = data.idRecord;
            var periodStart = data.periodStart;
            var date = data.dateTime;
            var time = data.timeStart;
        }

        // *  0: Шаблон страницы
        //  *  1: Дата, на которую строим диаграмму
        //  *  2: Ось Y - выделенной ячейки в диаграмме (время)
        //  *  3: Ось X - выделенной ячейки в диаграмме (мастер)

        switch (type) {
            case 'sale':
                page = `client_${id}`
                title = 'ТО-0'
                break;
            case 'missed':
                title = 'Пропущенный звонок'
                page = `client-order_${date}_${time}_${employee}_${id}`
                break;
            case 'overdue':
                page = `client-order_${date}_${time}_${employee}_${id}`
                title = 'Клиент опаздывает'
                break;
            case 'closing':
                page = `order-finish_${id}`
                title = 'Выдача автомобиля'
                break;
            case 'plan_own':
                title = 'ПСК'
                page = `client-order_${date}_${time}_${employee}_${id}`
                break;
            case 'plan_master':
                title = 'ПСК МП'
                page = `client-order_${date}_${time}_${employee}_${id}`
                break;
            case 'plan_auto':
                title = 'База обзвона'
                page = `client-order_${date}_${time}_${employee}_${id}`
                break;
            default:
                title = `${type}`
        }

		e.preventDefault();

        /*
		dialog = document.getElementById('dialog_event_info')

		$(dialog).find('#date').val(new Date().toISOString().slice(0, 10))
		$(dialog).find('#time').val(new Date(th.data('date')).toLocaleTimeString().slice(0,5))

		$(dialog).find('.button.action_do').off()
			.click( (e) => front_page(page, title, true, false, record) )

		$(dialog).find('.action_postpone')
			.attr('onsubmit', "return postpone($(this))" )

        $(dialog).dialog();
        */
    
		record = {
            m_ref: employee,
            time: new Date(th.data('date')).getTime(),
            records: [
                {
                    idRecord: id
                }
            ]
        }

        open_dialog('dialog_event_info', page, title, {
            '#date': new Date().toISOString().slice(0, 10),
            '#time': new Date(th.data('date')).toLocaleTimeString().slice(0,5)
        }, {
            '.button.action_do': record,
            '.action_postpone': (dialog) => {
                console.log(dialog)
                pp = dialog.querySelector('.postpone')
                if (pp.style.display) {
                    pp.style.display = ''
                } else {
                    console.log('POSTPONE')
                    $(dialog).dialog('close')
                }
            }
        })
    });
}

/*
 * INTERFACE OBJECT ACTION
 */
function postpone(form) {

    close_dialog()

    load();
    api_call('api_set_new_time_task', { date: form.serialize() }).then(data => {
        unload();
    });

    // $.ajax({
    //     type: "GET",
    //     url: "/",
    //     data: form.serialize()
    // }).done(function() {
    //     form.find('input:not([type="hidden"])').val('');
    //     $('<div>Запись отложили!</div>').dialog();
    // });
    return false;
}

/*
 * FILL INTERFACE OBJECT
 */
function showRecord(record) {
    if (!record) return


    var phone = record.phone;
    var lastMilage = record.lastMilage;
    var reason = record.reason;
    var vin = record.vin;
    var сontragentStr = record.idContragent;
    var customerStr = record.idContragent;
    var carNumber = record.carNumber;
    var idRecord = record.idRecord;
    var isGarantee = record.isGarantee;

    // $.getJSON('/api_get_history_client?idContragent=' + record.idContragent + '&idCustomer=' + record.idCustomer, function (history) {
    //     for (var i = 0; i < history.length; i++) {
    //         var reason = history[i].reason;
    //         var dateTime = history[i].dateTime;
    //         $('main .wrap .wrap-content>.cont .history .box ul').append('<li> - ' + dateTime + ', ' + reason + '</li>');
    //     }
    // });

	$.getJSON('/api_get_history_client?idContragent=' + page.selected.records[0].idContragent + '&idCustomer=' +  page.selected.records[0].idCustomer, function (history) {
        for (var i = 0; i < history.history.length; i++) {
            var reason = history.history[i].reason;
            var dateTime = history.history[i].dateTime;
            $('main .wrap .wrap-content>.cont .history .box ul').append('<li> - ' + dateTime + ', ' + reason + '</li>');
        }
    });

	console.log("isGarantee:"+isGarantee)

	if (vin)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="vin"]').val(vin);
	if (phone)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="tel"]').val(phone);
	if (lastMilage)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="probeg"]').val(lastMilage);
	if (reason)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="reason"]').val(reason);
	if (сontragentStr)
		$('main .wrap .wrap-content>.cont #orderToMaster p input[name="сontragentStr"]').val(сontragentStr);
	if (customerStr)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="name"]').val(customerStr);
	if (carNumber)
		$('main .wrap .wrap-content>.cont #orderToMaster p label input[name="gn"]').val(carNumber);
	if (idRecord)
		$('main .wrap .wrap-content>.cont #orderToMaster p input[name="idRecord"]').val(idRecord);
	for (var i = 0; i < record.promises.length; i++) {
		var promise = record.promises[i].promise;
		$('#promises').append('<li>'+promise+'</li>');
	}
}


function showClientCall2(record) {
    if (!record) return
	for (var i = 0; i < record.history.length; i++) {
		var reason = record.history[i].reason;
		var dateTime = record.history[i].dateTime;
		find_el_in_page(page, 'main .wrap .wrap-content>.cont .history .box ul').append('<li> - '+dateTime+', '+reason+'</li>');
	}
	var phone = record.phone;
	var reason = record.reason;
	var vin = record.vin;
	var сontragentStr = record.сontragentStr;
	var customerStr = record.customerStr;
	var carNumber = record.carNumber;
	var idRecord = record.idRecord;
	var idContragent = record.idContragent;
	var idCustomer = record.idCustomer;
	var carId = record.carId;
	var nameEmployee = record.nameEmployee;
	var auto = record.carsName;

	// var date = args[4];
	// var time = args[2];
	// var idEmployee = args[3];
	// var nameEmployee = args[5];
	var lastMilage = record.lastMilage;
    var payment = (record.payment) ? 'green' : 'red';

    var t_ref  = record.t_ref;
    var isGarantee  = record.isGarantee;

	$.getJSON('/api_get_history_client?idContragent=' + page.selected.records[0].idContragent + '&idCustomer=' +  page.selected.records[0].idCustomer, function (history) {
        for (var i = 0; i < history.history.length; i++) {
            var reason = history.history[i].reason;
            var dateTime = history.history[i].dateTime;
            $('main .wrap .wrap-content>.cont .history .box ul').append('<li> - ' + dateTime + ', ' + reason + '</li>');
        }
    });

    find_el_in_page(page, '#openPDF_ZN').addClass(payment);
	if (date)
		find_el_in_page(page, 'input[name="data-reg"]').text(date);
	if (time)
		find_el_in_page(page, 'input[name="time-reg"]').text(time);
	if (nameEmployee)
		find_el_in_page(page, 'input[name="name-master"]').text(nameEmployee);
	if (lastMilage)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="probeg"]').val(lastMilage);
	if (carNumber)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="gn"]').val(carNumber);
	if (vin)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="vin"]').val(vin);
	if (phone)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="tel"]').val(phone);
	if (reason)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="reason"]').val(reason);
	if (сontragentStr)
		find_el_in_page(page, '.wrap-content>.cont p input[name="сontragentStr"]').val(сontragentStr);
	if (customerStr)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="name"]').val(customerStr);
	if (auto)
		find_el_in_page(page, 'input[name="auto"]').val(auto);
	if (idRecord)
		find_el_in_page(page, 'input[name="idRecord"]').val(idRecord);
	if (idContragent)
		find_el_in_page(page, 'input[name="idContragent"]').val(idContragent);
	if (idCustomer)
		find_el_in_page(page, 'input[name="idCustomer"]').val(idCustomer);
	if (t_ref)
		find_el_in_page(page, 'input[name="t_ref"]').val(t_ref);
	if (isGarantee){
	    find_el_in_page(page, 'input:radio[value="yes"]').attr('checked', true);
	}else{
	    find_el_in_page(page, 'input:radio[value="no"]').attr('checked', true);
        }

	if (carId)
		$('main .wrap input[name="carId"]').val(carId);
	for (var i = 0; i < record.promises.length; i++) {
		var promise = record.promises[i].promise;
		$('#promises').append('<li>'+promise+'</li>');
	}
}


/*
 * FILL INTERFACE OBJECT
 */
function showClientCall(page) {
    console.log(page)
    let record = page.selected

    if (!record.m_ref) {
        load();
        $.getJSON('/api_get_record?r_ref=' + record.records[0].idRecord, function (data) {
            showClientCall2(data);
            unload();
        });
        return
    }

	// for (var i = 0; i < record.history.length; i++) {
	// 	var reason = record.history[i].reason;
	// 	var dateTime = record.history[i].dateTime;
	// 	find_el_in_page(page, 'main .wrap .wrap-content>.cont .history .box ul').append('<li> - '+dateTime+', '+reason+'</li>');
	// }

    if (page.diagram.el != null) {
        record = page.diagram.get_period(record)
    }
    else {
        record = front.diagram.get_period(record)
    }
    if (!record) return console.log("ERROR 2")


    let line = record
    record = record.records[0]

    console.log(line, record)

	var phone = record.phone;
	var reason = record.reason;
	var vin = record.vin;
	var сontragentStr = record.сontragentName;
	var customerStr = record.customerName;
	var carNumber = record.carNumber;
	var idRecord = record.idRecord;
	var idContragent = record.idContragent;
	var idCustomer = record.idCustomer;
	var carId = record.carId;
	var nameEmployee = record.nameEmployee;
	var auto = record.carsName;

	$.getJSON('/api_get_history_client?idContragent=' + page.selected.records[0].idContragent + '&idCustomer=' +  page.selected.records[0].idCustomer, function (history) {
        for (var i = 0; i < history.history.length; i++) {
            var reason = history.history[i].reason;
            var dateTime = history.history[i].dateTime;
            $('main .wrap .wrap-content>.cont .history .box ul').append('<li> - ' + dateTime + ', ' + reason + '</li>');
        }
    });

	// var date = args[4];
	// var time = args[2];
	// var idEmployee = args[3];
	// var nameEmployee = args[5];
	var lastMilage = record.lastMilage;
    var payment = (record.payment) ? 'green' : 'red';

    var t_ref  = record.t_ref;
    var isGarantee  = record.isGarantee;

    var time = record.timeStart;
    var date = record.dateStart;

    find_el_in_page(page, '#openPDF_ZN').addClass(payment);
	if (date)
		find_el_in_page(page, 'input[name="data-reg"]').text(date);
	if (time)
		find_el_in_page(page, 'input[name="time-reg"]').text(time);
	if (line.m_name)
		find_el_in_page(page, 'input[name="name-master"]').text(line.m_name);
	if (lastMilage)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="probeg"]').val(lastMilage);
	if (carNumber)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="gn"]').val(carNumber);
	if (vin)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="vin"]').val(vin);
	if (phone)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="tel"]').val(phone);
	if (reason)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="reason"]').val(reason);
	if (сontragentStr)
		find_el_in_page(page, '.wrap-content>.cont p input[name="сontragentStr"]').val(сontragentStr);
	if (customerStr)
		find_el_in_page(page, '.wrap-content>.cont p label input[name="name"]').val(customerStr);
	if (auto)
		find_el_in_page(page, 'input[name="auto"]').val(auto);
	if (idRecord)
		find_el_in_page(page, 'input[name="idRecord"]').val(idRecord);
	if (idContragent)
		find_el_in_page(page, 'input[name="idContragent"]').val(idContragent);
	if (idCustomer)
		find_el_in_page(page, 'input[name="idCustomer"]').val(idCustomer);
	if (t_ref)
		find_el_in_page(page, 'input[name="t_ref"]').val(t_ref);
	if (isGarantee){
	    find_el_in_page(page, 'input:radio[value="yes"]').attr('checked', true);
	}else{
	    find_el_in_page(page, 'input:radio[value="no"]').attr('checked', true);
        }

	if (carId)
		$('main .wrap input[name="carId"]').val(carId);
	// for (var i = 0; i < record.promises.length; i++) {
	// 	var promise = record.promises[i].promise;
	// 	$('#promises').append('<li>'+promise+'</li>');
	// }
}

/*
 * INTERFACE OBJECT ACTION
 */
function sendForm(th, e) {
	e.preventDefault();
	var form = th;
	$.ajax({
		type: "GET",
		url: "server.php",
		data: form.serialize()
	}).done(function() {
		form.find('input:not([type="hidden"])').val('');
		$('<div>Ответ сервера</div>').dialog();
	});
}

/*
 * FILL INTERFACE OBJECT
 */
function fill_services(arr) {
	$('#price-list-cont #list-services').html('');
	for (var i = 0; i < arr.length; i++) {
		var name = arr[i]['crm_service'];
		var ref = arr[i]['ref'];
		$('#price-list-cont #list-services').append('<li><label><input type="checkbox" name="crm_service" val="'+ ref +'"> <span style="margin-left: 5px">'+ name +'</span></label></li>');
	}
}

/*
 * GET SELECTED SERVICES (POP-UP)
 */
function get_services(page) {
    var list = '';
    find_el_in_page(page, '#price-list-cont #list-services input:checked').each(function(index, el) {
        var inp = $(this);
        var val = inp.val();
        var name = inp.next('span').text();
        list += name+', ';
    });
    return list.slice(0, list.length - 2);
}

/*
 * FOR SEARCH SEEVICES
 */
function find_services(list, str) {

	list.each(function(index, el) {
		var th = $(this);
		var name = th.text();
		if (~name.toLowerCase().indexOf(str.toLowerCase()))
			th.removeClass('hidden');
		else
			th.addClass('hidden');
	});
}

/*
 * WTF? COMMON?
 */
function toggleP(th) {
	var a = $(th);
	var p = a.next('p');
	p.slideToggle();
	a.toggleClass('active');
}

/*
 * FILL INTERFACE OBJECT
 */
function getRecommendationForAuto(carId) {
	$('#recomend-list').text('');
	$.getJSON('/api_get_recommendations?c_ref='+carId, function(data) {
    	var num = data.length;
    	$('#numRecommend').text(num);
    	for (var i = 0; i < num; i++) {
			var exec_tag = data[i]['exec_tag'];
			var comment = data[i]['comment'];
			var li = '<li><input type="checkbox" ';
			if (exec_tag) li += 'checked';
			li += ' onclick="return false;" style="display:inline-block;margin-right: 5px"/> '+comment+'</li>';
			$('#recomend-list').append(li);
        }
    });

    $.getJSON('/api_get_service_crm?c_ref=' + carId, function (services) {
        $('#price-list-cont #list-services').html('');
        for (var i = 0; i < services.length; i++) {
            var name = services[i]['crm_service'];
            var ref = services[i]['ref'];
            $('#price-list-cont #list-services').append('<li><label><input type="checkbox" name="crm_service" val="' + ref + '"> <span style="margin-left: 5px">' + name + '</span></label></li>');
        }

    });
}

/*
 * CHECH NAME AFTER INPUTING AUTO
 */
function checkSingleName(id) {
    
    if ($('input[name="idCustomer"]').val() == '' && $('input[name="idContragent"]') == '') {
        load();

        $.getJSON('/api_get_client?lim=50&carId='+id, function(data) {
            var num = data.length;
            $('#list-clients').text('');
            if (num == 1) {
                var name = data[0]['customerName'] + ' (' + data[0]['contragentName'] + ')' + '(' + data[0]['phone'] + ')';
                var idContragent = data[0]['idContragent'];
                var idCustomer = data[0]['idCustomer'];
                var phone = data[0]['phone'];
                $('.find-info.name').val(name).prop('disabled', true);
                $('input[name="idCustomer"]').val(idCustomer);
                $('input[name="idContragent"]').val(idContragent);
                $('.tel').val(phone);
                showHistory(idContragent, idCustomer)
            } else {
                for (var i = 0; i < num; i++) {
                    var name = data[i]['customerName'] + ' (' + data[i]['contragentName'] + ')' + '(' + data[i]['phone'] + ')';
                    var idContragent = data[i]['idContragent'];
                    var idCustomer = data[i]['idCustomer'];
                    var phone = data[i]['phone'];
                    $('#list-clients').append('<li><a href="#" onclick="checkAutoFromListName(this)" data-idContragent="'+ idContragent +'" data-idCustomer="'+ idCustomer+'" data-phone="'+ phone +'">'+ name +'</a></li>');
                }
                $(document).mouseup(function (e) {
                    var container = $("#list-clients");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        });
        
        unload();
    }
}

/*
 * CHECH AUTO AFTER INPUTING NAME
 */
function checkSingleAuto(id, idCustomer) {

    if ($('input[name="carId"]').val() == '') {
        load();
        $.getJSON('/api_get_car?lim=50&c_ref='+id+'&idCustomer='+idCustomer, function(data) {
            var num = data['cars'].length;
            $('#list-cars').text('');
            if (num == 1) {
                var carName = data['cars'][0]['carName'];
                var idCar = data['cars'][0]['idCar'];
                $('.find-info#auto').val(carName).prop('disabled', true);
                $('input[name="carId"]').val(idCar);
                $('#recomend-list').text('');
                getRecommendationForAuto(idCar);
                $.getJSON('/api_get_service_crm?carId='+idCar, function(services) {
                    $('#price-list').click(function(event) {
                        event.preventDefault();
                        fill_services(services);
                        $('#price-list-cont').dialog();
                    });

                    $('#price-list-cont input[type="search"]').keyup(function(event) {
                        var val = $(this).val();
                        find_services($('#price-list-cont #list-services li'), val);
                    });
                });
            } else {
                for (var i = 0; i < num; i++) {
                    var carName = data['cars'][i]['carName'];
                    var idCar = data['cars'][i]['idCar'];
                    $('#list-cars').append('<li><a href="#" onclick="checkAutoFromList(this)" data-id="'+ idCar +'">'+ carName +'</a></li>');
                }
                $(document).mouseup(function (e) {
                    var container = $("#list-cars");
                    if (container.has(e.target).length === 0){
                        container.text('');
                    }
                });
            }
        });
        unload();
    }

}

/*
 * SELECT AUTO FROM THE LIST
 */
function checkAutoFromList(th) {
	var a = $(th);
	var carId = a.data('id');
	var val = a.text();
	$('input[name="auto"].find-info').val(val).prop('disabled', true);
	$('#list-cars').text('');
	$('input[name="carId"]').val(carId);
	checkSingleName(carId);
	getRecommendationForAuto(carId);
    $.getJSON('/api_get_service_crm?carId='+carId, function(services) {
        $('#price-list').click(function(event) {
            event.preventDefault();
            fill_services(services);
            $('#price-list-cont').dialog();
        });

        $('#price-list-cont input[type="search"]').keyup(function(event) {
            var val = $(this).val();
            find_services($('#price-list-cont #list-services li'), val);
        });
    });
}

/*
 * FILL INTERFACE OBJECT
 */
function showHistory(idContragent, idCustomer) {

    $('main .wrap .wrap-content>.cont .history .box ul').text('');
    $.getJSON('/api_get_history_client?idContragent='+idContragent+'&idCustomer='+idCustomer, function(record) {
        for (var i = 0; i < record.history.length; i++) {
            var reason = record.history[i].reason;
            var dateTime = record.history[i].dateTime;
            $('main .wrap .wrap-content>.cont .history .box ul').append('<li> - '+dateTime+', '+reason+'</li>');
        }
    });
}

/*
 * SELECT NAME FROM THE LIST
 */
function checkAutoFromListName(th) {
	var a = $(th);
	var idCustomer = a.data('idcustomer');
	var idContragent = a.data('idcontragent');
	var phone = a.data('phone');
	var val = a.text();
	$('.find-info.name').val(val).prop('disabled', true);
	$('#list-clients').text('');
	$('input[name="idCustomer"]').val(idCustomer);
	$('input[name="idContragent"]').val(idContragent);
	$('.tel').val(phone);
	checkSingleAuto(idContragent, idCustomer);
    showHistory(idContragent, idCustomer);
}

/*
 * SELECT PHONE FROM THE LIST
 */
function checkClientByTel(th) {
    var a = $(th);
    var idCustomer = a.data('idcustomer');
    var idContragent = a.data('idcontragent');
    var phone = a.data('phone');
    var val = a.text();
    $('.find-info.name').val(val).prop('disabled', true);
    $('.find-info.tel').val(phone).prop('disabled', true);
    $('#list-clients-by-tel').text('');
    $('input[name="idCustomer"]').val(idCustomer);
    $('input[name="idContragent"]').val(idContragent);
    checkSingleAuto(idContragent, idCustomer);
    showHistory(idContragent, idCustomer);
}

/*
 * SELECT MODEL FROM THE LIST
 */
function checkModelFromList(th) {
    var a = $(th);
    var modelId = a.data('id');
    var val = a.text();
    $('input[name="model"].find-info').val(val).prop('disabled', true);
    $('#list-models').text('');
    $('input[name="modelId"]').val(modelId);
}


function checkForm(form) {
	var correct = true;
	form.find(':input[required]:visible').each(function(index, el) {
		if ($(this).val() == '' && correct) {
			correct = false;
			$(this).focus();
		}
	});
	return correct;
}

/*
 * ВНЕШНИЕ МЕТОДЫ КЛАССА FRAME
 */
function onclick_dashboard_period_free(period) {
    open_dialog('dialog_create_record', 'client-koles', 'Создание записи', {
            // elements
            '.m_name': period.m_name,
            '.e_date': new Date(period.time).toLocaleDateString(),
            '.e_time': new Date(period.time).toLocaleTimeString(),
        }, {
            // действия
            // если передали объект, он пойдет на вход front_page
            '.button.action_do': period
        }
    )
}

function onclick_dashboard_period_busy(period) {
    let record = period.records[0]

    let data = {
        '.c_name': record.customerName,
        '.e_date': new Date(period.time).toLocaleDateString(),
        '.e_time': new Date(period.time).toLocaleTimeString(),
    }
    open_dialog('dialog_record_info', 'client-order', 'Клиент', data, {
        '.button.action_do': period
    })
}

function onclick_dashboard_period_busy_arr(period) {
    $('#dialog_select_client ul.list-for-select').html('');
    for (var i = 0; i < period.records.length; i++) {
        let record = period.records[i]

        let data = {
            '.c_name': record.customerName,
            '.e_date': new Date(period.time).toLocaleDateString(),
            '.e_time': new Date(period.time).toLocaleTimeString(),
        }
        
        var li = '<li>'+ record.customerName +' ('+ new Date(period.time).toLocaleTimeString() +')</li>';

        $(li).click(function(event) {
            onclick_dashboard_period_busy([period[i]]);
        });

        $('#dialog_select_client ul.list-for-select').append(li);
    }
}

function onclick_dashboard_period_close(period) {
    let record = period.records[0]

    let data = {
        '.c_name': record.customerName,
        '.e_date': new Date(period.time).toLocaleDateString(),
        '.e_time': new Date(period.time).toLocaleTimeString(),
    }
    open_dialog('dialog_close_deal', 'order-finish', 'Выдача', data, {
        '.button.action_do': period
    })
}

function onclick_page_period_free(page, period) {
    if (page.selected) {
        if (confirm("Хотите изменить запись на "+ period.time +" к мастеру "+ period.m_name +"?")) {
            // далее по аналогии с function changeMaster (th, page, args) {
            var page = document.getElementById(page.Id);
            var date = th.data('date');
            var employee = th.data('employee');
            var employee_name = th.data('name');
            var time = th.data('time');

            console.log(find_el_in_page(page, 'input[name="checkedMaster"]').val(employee));

            let ref = page.selected.records[0].idRecord
            page.selected = period

            api_call('upd_empl_record_log', {
                r_ref: ref,
                e_ref: period.m_ref,
                date: new Date(period.time).toISOString.slice(0, 10),
                time: new Date(period.time).toLocaleTimeString().slice(0,5)
            }).then(
                success => page.diagram.update()
            )
        }
    } else {
        page.selected = period
        page.diagram.update()
    }
}
