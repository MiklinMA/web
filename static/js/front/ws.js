/*
 * Обработка событий из вебсокета
 */
function front_onmessage(e) {
    var data = $.parseJSON(e.data)

    command = data['command']
    var d = data['data']

    switch (command) {
        case 'login':
          if (login(d)) {
              goAway = false
              location.href = '/front'
          }
          break
        case 'workspace':
            front_initphoner(d.sip)
            break;
        default:
            ws_onmessage(e);
    }
}

function front_ws_onload() {
    ws_connect();
    ws.onmessage = function(e) { front_onmessage(e) };
}
