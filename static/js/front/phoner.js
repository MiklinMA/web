function front_initphoner(sip) {
    ua = initphoner(sip)
    ua.on('invite', function(e) { front_invite(e) })
}

function front_invite(e) {
    if (active_call !== undefined) {
        e.reject()
        return
    }

    caller = e.remoteIdentity.displayName
    console.info('Входящий log', caller)

    _call = e
    _call.on('progress',  e => call_progress        (e))
    _call.on('accepted',  e => front_call_accept    (e))
    _call.on('rejected',  e => call_reject          (e))
    _call.on('failed',    e => call_failed          (e))
    _call.on('terminated',e => front_call_terminate (e))
    _call.on('cancel',    e => call_cancel          (e))
    _call.on('refer',     e => call_refer           (e))
    _call.on('replaced',  e => call_replace         (e))
    _call.on('dtmf',      e => call_dtmf            (e))
    _call.on('muted',     e => call_mute            (e))
    _call.on('unmuted',   e => call_unmute          (e))
    _call.on('bye',       e => call_bye             (e))

    active_call = {}
    active_call.tag = e.from_tag
    active_call.sip = _call
    active_call.answered = false
    active_call.hold = false
    active_call.direction = "inbound"
    active_call.caller = caller

    calls[e.from_tag] = active_call

    let page = `client-call_${caller}`
    // отправить в API запрос данных о клиенте по номеру телефона
    api_call('call_init', {caller: caller}, false).then(data => {
        if (Object.keys(data).length && active_call) {
            console.info('Входящий вызов', caller)

            d_phone = $(dialog_inbound_call).find('.dialog_phone')[0]
            d_phone.innerHTML = caller
            $(dialog_inbound_call).dialog()

            active_call.client = data
            d_phone.innerHTML = data.c_name
            dialog_inbound_call.data = data

            new Audio('/static/media/ring.mp3').play()

            page = front_page(page, "Входящий звонок", false);
            active_call.page = page
            api_call('get_record', {ref: data.t_ref}, false).then(data => {
                showClientCall(page.selected)
            });
        }
    });
}

function front_call_accept(e) {
    // console.log(e) // TODO нужны ли здесь какие-то действия?
}

function front_call_answer() {
    _call = get_unanswered_call()
    if (!_call) {
        console.info("Звонки на линии отсутствуют")
        return
    }

    _call.answered = true
    ws_send('call_accept') // get?

    call_opts.media.render.remote = create_stream()
    _call.sip.accept(call_opts) // делаем простой accept
    // здесь не вызывается (!!!) событие on_accepted

    sip_button_hold.style.display = ''
    sip_button_down.style.display = ''
    call_hold(false)

    start_timer('ttimer', 'Звонок на линии')

    close_dialog('#dialog_inbound_call')

    // activate(`client-call_${active_call.caller}`)
    // front_page(page, "Входящий звонок", true);
    active_call.page.active = true

    // front_pages[`client-call_${active_call.caller}`].busy = true
}

function front_call_down() {
    if (active_call) {
        // front_pages[`client-call_${active_call.caller}`].busy = false
        call_down()
    }
    close_dialog('#dialog_inbound_call')
}

function front_call_terminate(e) {
    if (active_call)
        // data = active_call.client
        // front_pages[`client-call_${active_call.caller}`].busy = false

        if (!active_call.answered) {

            front_page_close('client-call_' + active_call.caller)
            load();
            api_call('api_set_call_no_answer', { data: 'caller=' + active_call.caller }).then(data => {
                unload();
            });
        }
    call_terminate(e)
    close_dialog('#dialog_inbound_call')
}
