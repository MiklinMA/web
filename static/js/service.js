//***************************************************************
//                              VARs
//***************************************************************
//c_ref.value = 'A014001A6476001111E3C4711852CB1F'

var am_ref   = ''    //ссылка на авто клиента
var run      = ''    //пробег авто
var r_ref    = ''    //сслка на мендера
var s_dt     = ''    //дата на которую записались
var model    = ''    //модель автомобиля
var made     = ''    //год производства
var warranty = ''    //гарантийный авто

var td_date = 0

//***************************************************************
//                              VARs
//***************************************************************

init_service_form()

function init_service_form(){
  getAuto()
  getModels()
  getFreetime()
  getPrice()

  d = new Date()
  calendar_setMonth()
  list_month.options[d.getMonth()].selected = true
  calendar_year.setAttribute('value',d.getFullYear())

  calendar_string.value = d.getDate()+" "+getTextMonth(d.getMonth(),'rp')+" "+d.getFullYear()

  fill_calendar()
}
//***************************************************************
//                         GET FUNCTIONS
//***************************************************************
function getAuto(){
  cref = document.getElementById('c_ref')
  ws_send('sql_c_auto',{'c_ref' : cref.value})
}

function getModels(){
  oref = document.getElementById('o_ref')
  name = oref.options[oref.selectedIndex].innerText
  ws_send('sql_o_models',{'name':name})
}

function getFreetime(){
  oref = document.getElementById('o_ref')
  dep = oref.options[oref.selectedIndex].value
  d = new Date()

  ws_send('sql_service_journal', {'dep': dep, 'date':getDateTime(d.getTime(), 'sj_send')})
}

function getPrice(){
  document.getElementById('service_price')
  data = {'gear':{
             'Моторное масло':800,
             'Тормозные колодки':1000,
             'Моторное масло1':800,
             'Тормозные колодки1':1000,
             'Моторное масло2':800,
             'Тормозные колодки2':1000,
             'Моторное масло3':800,
             'Тормозные колодки3':1000
           },'work':{
             'Замена масла':300,
             'Замена колодок': 1000,
             'Замена масла1':300,
             'Замена колодок1': 1000,
             'Замена масла2':300,
             'Замена колодок2': 1000,
             'Замена масла3':300,
             'Замена колодок3': 1000,
             'Замена масла4':300,
             'Замена колодок4': 1000
           }
         }

  price = 0
  p_cost = 0

  for(elem in data){
    if (elem == 'gear'){
      className = 'gear'
      colname = 'Материалы'
    }else if( elem == 'work'){
      className = 'work'
      colname = 'Работы'
    }

    text_el = [{'tag':'tr', 'css':'', 'text':'', 'attr':{'onclick':"service_show_cost('"+className+"')"}},
               {'tag':'td', 'css':'', 'text':'<b>'+colname+'</b>','attr':{}},
               {'tag':'td', 'css':'', 'text':'', 'attr':{'class':'text-center', 'id':className}}
              ]
    works.appendChild(create_element(text_el))

    d = data[elem]
    for (el in d){
      text_el = [{'tag':'tr','css':'display:none','text':'','attr':{'class':className}},
                 {'tag':'td','css':'','text':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'+ el+'</b>','attr':{}},
                 {'tag':'td','css':'','text':'<b>'+d[el]+'</b>','attr':{'class':'text-center'}}
                ]
      price = price + d[el]
      p_cost = p_cost + d[el]
      works.appendChild(create_element(text_el))
    }
    document.getElementById(className).innerHTML = '<b>'+p_cost+'</b>'
    p_cost = 0
  }
  cost.innerHTML = price
}

function getDateTime(string, format){
  d = new Date()
  d.setTime(string)

  switch(format){
    case 'xmlrpc':
      month   = d.getMonth() < 10? '0'+(d.getMonth()+1) : d.getMonth()+1
      day     = d.getDate() < 10? '0'+d.getDate() : d.getDate()
      hour    = d.getHours() < 10? '0'+d.getHours() : d.getHours()
      minutes = d.getMinutes() < 10? '0'+d.getMinutes() : d.getMinutes()
      return d.getFullYear()+'-'+month+'-'+day+'T'+hour+':'+minutes+':'+'00Z'

    case 'sj_send':
      month = d.getMonth() < 10? '0'+(d.getMonth()+1) : d.getMonth()+1
      day = d.getDate() < 10? '0'+d.getDate() : d.getDate()
      return d.getFullYear()+month+day

    case 'fulltext':
      return d.getDate()+' '+getTextMonth(d.getMonth(),'rp')+' '+d.getFullYear()

    case 'zero':
      d.setHours(0)
      d.setMinutes(0)
      d.setMilliseconds(0)
      return d

    default:
      var d = new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'))
      return d
  }
}

function get2day(data, day){
  ftime  = new Date()
  rest = new Date()

  ftime.setSeconds(0)
  ftime.setMilliseconds(0)
  rest.setSeconds(0)
  rest.setMilliseconds(0)

  switch (day){
    case 'today':
      ftime.setTime(ftime.getTime() + 120*60*1000)
      if(ftime.getMinutes() > 0 && ftime.getMinutes() <=15 ){
        ftime.setMinutes(0)
      }else if ( ftime.getMinutes() > 15 && ftime.getMinutes() <= 40 && ftime.getMinutes() != 30 ){
        ftime.setMinutes(30)
      }else if( ftime.getMinutes() > 40 ){
        ftime.setHours(ftime.getHours()+1)
        ftime.setMinutes(0)
      }
      rest.setDate(rest.getDate() + 1)
      rest.setHours(15)
      rest.setMinutes(30)
      name_day = 'Сегодня'
      break
    case 'tomorrow':
      ftime.setDate(ftime.getDate()+1)
      ftime.setHours(16)
      ftime.setMinutes(0)

      rest.setDate(rest.getDate() + 1)
      rest.setHours(18)
      rest.setMinutes(00)
      name_day = 'Завтра'
      break
  }
  i = 0
  for ( elem in data ){
    time = getDateTime(data[elem].dt)
    if( i == 0 && day == 'today'){
      if (rest.getDay() - time.getDay() == 0) {
        ftime.setDate(ftime.getDate() + 1)
        ftime.setHours(9)
        ftime.setMinutes(0)
        name_day = 'Завтра'
      }
      i++
    }

    if ( ftime.getTime() == time.getTime() && ftime.getTime() <= rest.getTime() ){
      if(data[elem].manager != "00"){
        min = ftime.getMinutes() < 10? '0'+ftime.getMinutes() : ftime.getMinutes()
        text_el = [{'tag':'option', 'css':'', 'text':name_day+' в '+ftime.getHours()+':'+min, 'attr':{'value':name_day+' в '+ftime.getHours()+':'+min, 'onclick':'service_check_time(this)'}},
                   {'tag':'input', 'css':'', 'text':'', 'attr':{'value':data[elem].manager,'type':'radio'}},
                   {'tag':'input', 'css':'', 'text':'', 'attr':{'value':ftime.getTime(),'type':'radio'}}]

        service_time_our.appendChild(create_element(text_el))
        return
      }else{
        ftime.setTime(ftime.getTime() + 30*60*1000)
      }
    }
  }
}

function getTextMonth(number, param){
  switch(param){
    case 'ip':
      month =['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
      return month[number]
    case 'rp':
      month =['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',]
      return month[number]
  }
}
//***************************************************************
//                         GET FUNCTIONS
//***************************************************************

//***************************************************************
//                        FILL FUNCTIONS
//***************************************************************
function fill_am(caller) {
  y = getDateTime(caller.getElementsByClassName('made')[0].value)
  am_year.value = y.getFullYear()

  f_model = caller.getElementsByClassName('model')[0].value
  model = f_model

  find  = find_in_select('am_model', f_model)

  /*Тестовы кусок кода удалить*/
  /*if (find == false){
    option = document.createElement('option')
    option.innerText = f_model
    option.setAttribute('selected', true)
    am_model.appendChild(option)
  }*/
  /*Тестовы кусок кода удалить*/

  am_runkm.value = caller.getElementsByClassName('run')[0].value
  run = caller.getElementsByClassName('run')[0].value

  warranty = caller.getElementsByClassName('warranty')[0].value
  guarantee.selectedIndex = caller.getElementsByClassName('warranty')[0].value

  am_ref = caller.getElementsByClassName('am_ref')[0].value

  service_check_am('auto')
}

function fill_models(data){
  for (elem in data){
    option = document.createElement('option')
    option.value = data[elem].ref
    option.innerText = data[elem].name
    am_model.appendChild(option)
  }
  if(service_am.options.length > 0){
    service_am.options[0].selected = true
    fill_am(service_am.options[0])
  }
}

function fill_auto(data){
  for (elem in data){
    option = document.createElement('option')
    option.value = data[elem].am_ref
    option.innerText = data[elem].am_name
    option.setAttribute('onclick', 'fill_am(this)')

    el = data[elem]
    for ( i in el){
      input = document.createElement('input')
      input.className = i
      input.setAttribute('value', el[i])
      input.style.cssText = 'display:none'
      option.appendChild(input)
    }
    service_am.appendChild(option)
  }
}

function fill_freetime(data){
  var days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ]
  td =''
  start_time = new Date()
  start_time.setTime(start_time.getTime()+30*60*100)

  get2day(data, 'today')
  get2day(data, 'tomorrow')

  min_hour = 9
  max_hour = 10

  for (h in data){
    time = getDateTime(data[h].dt)
    if (time.getHours() < min_hour)
      min_hour = time.getHours()
    else if (time.getHours() > max_hour)
      max_hour = time.getHours()
  }

  freetime = document.getElementById('freetime')
  tr = document.createElement('tr')
  text_el = [{'tag':'tr','css':'font-size: 0.81em;','text':'','attr':{}},
             {'tag':'th','css':'','text':'День','attr':{'class':'text-center'}}]

  for (i = min_hour; i < max_hour+1; i++)
    text_el.push({'tag':'th', 'css':'', 'text':i+":00", 'attr':{'class':'text-center', 'colspan':2}})

  freetime.appendChild(create_element(text_el))
  //------------------------------//
  i = 0
  for (elem in data){

    time = getDateTime(data[elem].dt)

    if (i == 0) {
      cssText = ''
      if(td_date > 0){
        if(getDateTime(time.getTime(),'sj_send') == getDateTime(td_date,'sj_send')){
          cssText = 'font-weight: bold;'
          td_date = 0
        }
      }else{
        cssText = 'font-weight: bold;'
      }

      text_el = [{'tag':'tr','css':'','text':'','attr':{}},
                 {'tag':'td','css':cssText,'text':days[time.getDay()]+' '+time.getDate(),'attr':{}}]
      tr = create_element(text_el)
    }else if ( time.getDate() != old_time.getDate() ){
      freetime.appendChild(tr)
      cssText = ''
      if(td_date > 0){
        if(getDateTime(time.getTime(),'sj_send') == getDateTime(td_date,'sj_send')){
          cssText = 'font-weight: bold;'
          td_date = 0
        }
      }
      text_el = [{'tag':'tr','css':'','text':'','attr':{}},
                 {'tag':'td','css':cssText,'text':days[time.getDay()]+' '+time.getDate(),'attr':{}}]
      tr = create_element(text_el)
    }

    if ( data[elem].manager != "00" && time.getTime() >= start_time.getTime() ){
      min = time.getMinutes() < 10? '0'+time.getMinutes() : time.getMinutes()
      text_el = [{'tag':'td',
                  'css':'background-color: #5cb85c; cursor: pointer;',
                  'text':'&nbsp;',
                  'attr':{
                    'class':'number text-center',
                    'onclick':'service_change_time(this)',
                    'title':time.getHours()+':'+min}
                  }]
      text_el.push({'tag':'input','css':'','text':'','attr':{'type':'radio','value':data[elem].manager}})
      text_el.push({'tag':'span','css':'display:none','text':time.getTime(),'attr':{}})
      tr.appendChild(create_element(text_el))
     }else{
      text_el = [{'tag':'td','css':'cursor: not-allowed;','text':'&nbsp;','attr':{'class':'number text-center'}}]
      tr.appendChild(create_element(text_el))
     }
    old_time = time
    i++
  }
  freetime.appendChild(tr)
}
//***************************************************************
//                        FILL FUNCTIONS
//***************************************************************

//***************************************************************
//                          CALENDAR
//***************************************************************
function calendar_setMonth(){
  for(i = 0; i < month.length; i++){
    option = document.createElement('option')
    option.setAttribute('value',i)
    option.innerHTML = getTextMonth(i,'ip')
    list_month.appendChild(option)
  }
}

function calendar_SelectDate(el){
  old_elem = calendar.getElementsByClassName('yellow')

  if(old_elem.length > 0){
    old_elem[0].classList.remove('yellow')
  }

  el.classList.add('yellow')

  freetime.innerHTML = ''

  lm = list_month

  oref = document.getElementById('o_ref')
  dep = oref.options[oref.selectedIndex].value

  d = new Date()
  t_d = new Date()
  td_d = new Date()

  d.setMonth(lm.options[lm.selectedIndex].value)
  d.setYear(calendar_year.value)
  d.setDate(el.innerText)

  td_d.setTime(d.getTime())

  month = d.getMonth() - t_d.getMonth()
  date = d.getDate()-t_d.getDate()
  if (month == 0 && date < 2 && date >= 0) {
    d.setDate(d.getDate() - date)
  }else{
    d.setDate(d.getDate() - 2)
  }

  td_date =  td_d.getTime()
  calendar_string.value = getDateTime(td_date, 'fulltext')

  if (getDateTime(t_d.getTime(),'sj_send') > getDateTime(d.getTime(),'sj_send')) {
    d = t_d
    td_date = 0
  }

  ws_send('sql_service_journal', {'dep':dep, 'date':getDateTime(d.getTime(),'sj_send')})
}

function calendar_ChangeMonth(opt){
  d = new Date()
  d.setMonth(opt.value)
  d.setDate(1)
  d.setYear(calendar_year.value)
  calendar.innerHTML =''

  data = {'date':d}
  fill_calendar(data)
}

function calendar_ChangeYear(el){
  d = new Date()
  opt = list_month.options[list_month.selectedIndex].value
  d.setMonth(opt)
  d.setDate(1)
  d.setYear(el.value)
  calendar.innerHTML =''

  data = {'date':d}
  fill_calendar(data)
}

function fill_calendar(data){
  d = new Date()
 // d.setDate(date)
  for (k in data)
    if(k == 'date')
      d = data[k]

  day_month = d.getDate()
  f_month = d.getMonth()
  month = d.getMonth()
  i = 0

  while( d.getDate() >= day_month && f_month == month){
    day = d.getDate()
    n_day = d.getDay()
    if(n_day == 0)
        n_day = 7

    if(i==0){
      tr = [{'tag':'tr', 'css':'', 'text':'', 'attr':{}}]
      j = 1
      while(j != n_day){
        tr.push({'tag':'td', 'css':'', 'text':'', 'attr':{}})
        j++
      }
      tr.push({'tag':'td', 'css':'', 'text':d.getDate(), 'attr':{'onclick':'calendar_SelectDate(this)'}})
    }else{
      tr.push({'tag':'td', 'css':'', 'text':d.getDate(), 'attr':{'onclick':'calendar_SelectDate(this)'}})
    }

    if(n_day == 7){
      calendar.appendChild(create_element(tr))
      tr = [{'tag':'tr', 'css':'', 'text':'', 'attr':{}}]
    }

    d.setDate(d.getDate() + 1)
    month = d.getMonth()

    i++
  }
  calendar.appendChild(create_element(tr))
}

function fill_ListMonth(data){
  if( data == 'right'){
    if (list_month.selectedIndex == list_month.childElementCount-1) {
      list_month.options[0].selected = true
    }else{
      list_month.options[list_month.selectedIndex+1].selected = true
    }
  }else if( data == 'left'){
    if (list_month.selectedIndex == 0) {
      list_month.options[list_month.childElementCount-1].selected = true
    }else{
      list_month.options[list_month.selectedIndex-1].selected = true
    }
  }

  d = new Date()
  d.setMonth(list_month.selectedIndex)
  d.setDate(1)
  d.setYear(calendar_year.value)
  calendar.innerHTML =''

  data = {'date':d}
  fill_calendar(data)
}

function calendar_show(data){
  calender_container.style.display = data
}
//***************************************************************
//                          CALENDAR
//***************************************************************

function find_in_select(id, string){
  options = document.getElementById(id).childNodes
  for (i = 0; i < options.length; i++){
    if (options[i].innerText == string) {
      options[i].setAttribute('selected', true)
      return true
    }
  }
  return false
}

function setOtherModel(select){
  am_ref = ''
  model = select.options[select.selectedIndex].value

  elements = service_am.options;
  for(var i = 0; i < elements.length; i++)
      elements[i].selected = false;
}

function service_check_time(opt) {
  inputs = opt.getElementsByTagName('input')
  if(inputs.length > 0){
    r_ref = inputs[0].value
    s_dt = getDateTime(inputs[1].value, 'xmlrpc')
  }

  service_final_text('select')
  if (document.getElementById('service_time_our').selectedIndex >= 0)
    service_show_button('step4', 'Далее', '')
  else
    service_show_button('step4', 'Далее', 'none')
}

function service_check_am(variant) {
  switch (variant){
    case 'auto':
      made = am_year.value
      break
    case 'other':
      am_ref = ''
      made = am_year.value
      warranty = guarantee.selectedIndex

      elements = service_am.options;
      for(var i = 0; i < elements.length; i++)
        elements[i].selected = false;
      break
  }
  if (document.getElementById('am_year').value > 1900) {
    service_show_button('step1', 'Далее', '')
    return
  }

  service_show_button('step1', 'Далее', 'none')
}

function service_check_km() {
  if (document.getElementById('am_runkm').value > 0){
    service_show_button('step2', 'Далее', '')
    run = am_runkm.value
  }else{
    service_show_button('step2', 'Далее', 'none')
  }
}

function service_run_km() {
  document.getElementById('service_auto').style.display = 'none'
  document.getElementById('service_runkm').style.display = ''
  service_check_km()
}

function service_show_cost(el){
  elem = document.getElementsByClassName('active_cost')
  if(elem.length > 0){
    className = elem[0].className
    for (i = elem.length-1; i >= 0 ; i--){
      elem[i].style.display = 'none'
      elem[i].classList.remove('active_cost')
    }
    if(className.indexOf(el)+1)
      return
  }

  elem = document.getElementsByClassName(el)
  if(elem.length > 0){
    for (i = 0; i < elem.length; i++){
        elem[i].style.display = ''
        elem[i].classList.add('active_cost')
    }
  }
}

function service_change_time(td){
  old_elem = freetime.getElementsByClassName('yellow')

  if(old_elem.length > 0){
    old_elem[0].classList.remove('yellow')
  }

  td.classList.add('yellow')
  elem = td.getElementsByTagName('input')
  elem[0].checked = true

  r_ref = elem[0].value
  span  = td.getElementsByTagName('span')[0].innerText
  s_dt = getDateTime(span, 'xmlrpc')

  calendar_string.value = getDateTime(span, 'fulltext')

  service_show_button('step4n', 'Далее', '')
}

function service_show_button(main_div, content, display){
  buttons = document.getElementById(main_div).getElementsByTagName('button')
  for(i = 0; i < buttons.length; i++){
    if (buttons[i].innerText.indexOf(content)+1)
      buttons[i].style.display = display
  }
}

function service_final_text(elem){
  if(elem == 'select'){
    sel = document.getElementById('service_time_our')
    cname = c_name.value.length > 0? ', '+c_name.value : ''

    text = 'Спасибо за звонок'+cname+'. Мы ждем Вас на ТО '+ sel.options[sel.selectedIndex].value+'.'
          +'<br>Общая стоимость работ составит '+cost.innerText+' рублей.'
          +'<br>Всего доброго. До свидания.';
    ft = document.getElementById('finish_text')
    ft.innerHTML = text
  }
  if (elem == 'other') {
    td    = freetime.getElementsByClassName('yellow')[0]
    span  = td.getElementsByTagName('span')[0]
    input = td.getElementsByTagName('input')[0]

    d = new Date()
    d.setTime(span.innerText)
    min = d.getMinutes() < 10? '0'+d.getMinutes() : d.getMinutes()
    s_data = d.getDate()+' '+getTextMonth(d.getMonth(),'rp')+' '+d.getFullYear()+' года в '+d.getHours()+':'+ min

    cname = c_name.value.length > 0? ', '+c_name.value : ''
    text  = 'Спасибо за звонок'+cname+'. Мы ждем Вас на ТО '+s_data+'.'
          +'<br>Общая стоимость работ составит '+cost.innerText+' рублей.'
          +'<br>Всего доброго. До свидания.';
    ft = document.getElementById('finish_text')
    ft.innerHTML = text
  }
}

function service_send(){

  o_ref = document.getElementById('o_ref')
  o_ref = o_ref.options[o_ref.selectedIndex].value
  c_ref = document.getElementById('c_ref').value
  e_ref = document.getElementById('e_ref').value
  c_name = document.getElementById('c_name').value
  c_memo = document.getElementById('c_memo').value
  c_phone = [document.getElementById('c_phones').getElementsByTagName('input')[0].value]


  data = {'am_ref'    : am_ref,
          'a_run'     : run,
          's_time'    : s_dt,
          'a_model'   : model,
          'a_made'    : made,
          'a_warranty': warranty,
          'o_ref'     : o_ref,
          'c_ref'     : c_ref,
          'e_ref'     : e_ref,
          'c_name'    : c_name,
          'c_memo'    : c_memo,
          'action'    : 'send',
          'manager'   : r_ref,
          'c_phone'   : c_phone,
          'recs'      : recs,
          'target'    : '1',
          's_type'    : 'to',
         }

  console.log(data)
  location.reload()

  ws_send('ones_call',data)
  location.reload()
}

function service_onmessage(e) { // MESSAGE FROM CRM
    data = $.parseJSON(e.data)

    command = data['command']
    data = data['data']

    //console.log(command, data)

    switch (command) {
    case 'sql_service_journal':
      fill_freetime(data)
      break
    case 'sql_c_auto':
      fill_auto(data)
      break
    case 'sql_o_models':
      fill_models(data)
      break
    default:
      ws_onmessage(e)
    }
}

function service_show_managers() {
  managers_container.style.display = ''
  service_container.style.display = 'none'
  serv_send.style.display = 'none'
  service_container.style.display = 'none'

  am_model.innerHTML = ''
  service_time_our.innerHTML = ''
  freetime.innerHTML = ''
  works.innerHTML = ''
  navigate('step', 1)
  cf_butt.style.display = ''
  serv_send.style.display = 'none'

  set_tm('Переключить Вас на конкретного менеджера?')
  get_managers(1)
}

function create_element(data){
  master_el = ''

  for (i = 0; i < data.length; i++){
    d_elem = data[i]
    tag = document.createElement(d_elem['tag'])
    for(a in d_elem['attr']){
      tag.setAttribute(a,d_elem['attr'][a])
    }
    tag.style.cssText = d_elem['css']
    try{
      tag.innerHTML = d_elem['text']
    }catch(error){}
    if (i == 0)
      master_el = tag
    else
      master_el.appendChild(tag)
  }
  return master_el
}