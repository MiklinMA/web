//=================================================================
// VARs
//=================================================================
var ua, call_opts, active_call
var calls = {}
var call_beep = new Audio('/static/media/call.wav')
call_beep.loop = true

var phoner_call = undefined
var g_number
//var recs = []
//=================================================================
// Initialization
//=================================================================
sip_button_init.style.display = 'none'
sip_button_down.style.display = 'none'
sip_button_hold.style.display = 'none'
sip_buttons.style.display = ''

// Функция для вызова при загрузке из внешнего файла
function sip_onload(user_sip) {
  loadJS('/static/js/nps.js', function() { nps_onload() })
  loadJS('/static/js/dist/sip.js', function() {
    console.info('Моуль телефонии загружен ( версия:', SIP.version, ')')
    ws_connect()
    ws.onmessage = function(e) { sip_onmessage(e) }
  }) //, initphoner(user_sip))
}

function sip_onmessage(e) {
  var data = $.parseJSON(e.data)

  command = data['command']
  var d = data['data']

  //console.log('sip', command, data)
  //console.log('sip', command)

  switch (command) {
    case 'user_init':
      console.info("Инициализация рабочего стола")
      user_init(d)
      ws_send("workspace")
      ws_send("sql_orgs", {isCall: true})
      ws_send("calls")

      setInterval('addmin()', 60 * 1000)
      change_view('main')
      //change_view('nps')
      fill_contacts({})
      break
    case 'workspace':
      initphoner(d['sip'])
      //nps_navigate('step0') // TODO убрать!!!
      break
    case 'check':
      console.log('Проверка возможности ответа на звонок')
      ok = !localuser.busy
      ok = ok && (activeView == 'main')
      ok = ok && !active_call
      ws_send('check', {
        ok: ok,
        k: d.text
        }
      )
    case 'call_invite':
      break
    case 'call_push':
      call_push(d)
      break
    case 'call_accept':
      //recs.push(d['text'])
      break
    case 'call_internal':
      document.getElementById('sip_active_buttons').style.display = ""
      break
    case 'call_pop':
      call_pop(d)
      break
    case 'call_xfer':
      call_xfer(d)
      break
    case 'call_ready':
      if (d['k']) // если звоним по dsts передаем ключ
        call_invite('0' + d['k'])
      else if (d['num']) // иначе звоним по номеру
        call_invite(d['num'])
      break
    case 'call_group':
      if (d['text'])
          call_invite('0' + d['text'])
      break
    case 'calls':
      missed = []
      i = 0
      for (c in d['missed']) {
        i ++
        missed.push(d['missed'][c])
        if (i > 10) break
      }
      missed.sort(function(a, b){ return b.duration - a.duration })
      /*
      */
      //console.debug(d['missed'])
      //fillTemplate('#online_calls', 'template_online', {}) // TODO remove
      fillTemplate('#missed_calls', 'template_missed', missed)
      //fillTemplate('#missed_calls', 'template_missed', d['missed'])
      ps = document.getElementById('call_proceed_line')
      if (!ps) break

      //d.proceed.forEach(function(entry) {
      i = 0
      p = d.proceed
      ks = Object.keys(p)
      ks.sort(function(a, b){return b-a})
      for (k in ks) {
        c = ps.cloneNode(true)
        online_calls.appendChild(c)
        c.style.display = ''

        //console.log(k, ks[k], p[ks[k]])

        try {
          o = orgs[p[ks[k]]['o_ref']]
          if (o && o.o_name != '') {
            p[ks[k]].o_name = o.o_name
            p[ks[k]].o_name_ = o.o_name_
          } else {
            p[ks[k]].o_name = d['called']
            p[ks[k]].o_name_ = d['called']
          }
        } catch(e) {
          //console.log('O_REF', e) // TODO
        }

        c.firstElementChild.innerHTML = p[ks[k]].c_name
        c.lastElementChild.innerHTML = p[ks[k]].o_name
        /*
        console.log(d.proceed)
        c.firstElementChild.innerHTML = d.proceed[i].c_name
        c.lastElementChild.innerHTML = d.proceed[i].o_name
        */
        i++
      }
      while (i < 10) {
        c = ps.cloneNode(true)
        online_calls.appendChild(c)
        c.style.display = ''

        i++
      }
      /*
      for (i = 0; i < 10; i++) {
        c = ps.cloneNode(true)
        online_calls.appendChild(c)
        c.style.display = ''

        if (i < d.proceed.length) {
          c.firstElementChild.innerHTML = d.proceed[i].c_name
          c.lastElementChild.innerHTML = d.proceed[i].o_name
        }
      }
      */
      break
    case 'busy':
      busy(true)
      break
    case 'unbusy':
      busy(false)
      break
    case 'sms_send':
      if (d[1] > 0)
        console.info('Сообщение отправлено')
      else
        console.warn('Сообщение не отправлено')
      break
    case 'get_missed':
      fill_call_form(d) // заполняем и переходим на форму звонка
      change_view('form')
      break
    case 'get_nps':
      nps_update_data(d)
      break
    case 'nps_user_drop': // WTF?
      RTC = ''
    case 'nps_no_answer':
      RTC = ''
    case 'nps_user_inv':
      RTC = ''
    default:
      ws_onmessage(e)
  }
}

// Инициализация объекта SIP
function initphoner(user_sip){
  ats_host = GLOBALS['ats_host']
  ats_port = GLOBALS['ats_port']

  if (location.protocol == 'https:')
    wsServer = 'wss://' + ats_host + ':' + ats_port + '/ws'
  else {
    wsServer = 'ws://' + ats_host + ':' + ats_port + '/ws'
  }

  if (!user_sip) {
    return console.error("Нет профиля SIP!", user_sip)
  }

  if (typeof SIP != 'undefined') {
    var ua_opts = {
      userAgentString   : "Autograd CRM WebRTC",
      uri               : 'sip:'+user_sip+'@' + ats_host,
      wsServers         : wsServer,
      password          : '1234',

      register          : true,
      registerExpires   : 60 * 60,

      displayName       : localuser.name,

      log: {
          level:        'warn',
          //level:        'error', // debug, log, warn, error
          builtinEnabled: true,
      },

      stunServers:      [
        //"stun:192.168.5.73:3478",
        //"stun:null",
      ],

      //noAnswerTimeout : 10,         // для входящих вызовов
      //rel100          : "required", // AX11 не поддерживает
      rtcpMuxPolicy     : "negotiate" // иначе ошибка "оператор не ответил на звонок"
    };


    var ua = new SIP.UA(ua_opts);

    ua.on('connecting',         function(e) { UA_connecting   (e) })
    ua.on('connected',          function(e) { UA_connected    (e) })
    ua.on('disconnected',       function(e) { UA_disconnected (e) })
    ua.on('registered',         function(e) { UA_reg          (e) })
    ua.on('unregistered',       function(e) { UA_unreg        (e) })
    ua.on('registrationFailed', function(e) { UA_reg_fail     (e) })
    ua.on('invite',             function(e) { UA_invite       (e) })
    ua.on('message',            function(e) { UA_message      (e) })
    ua.on('failed',             function(e) { UA_failed       (e) })

    local_stream = create_stream()
    local_stream.muted = true
    
    call_opts  = {
      media: {
        constraints: {
          audio: true,
          video: false
        },
        render: {
          local: local_stream
        },
      },
    }
    return ua
  }
}

//=================================================================
// UA Events
//=================================================================
function UA_connecting(e) {
    console.info('Подключение телефона')
}
function UA_connected(e) {
    console.info("Телефон подключен")
}
function UA_disconnected(e) {
    console.log('UA. Disconnected', e)  // TODO
    console.error("Телефон отключен")
    //ws_send('no_phone')
}
function UA_reg(e) {
  switch(e.status_code) {
  case (200):
    console.info("Телефон зарегистрирован")
    sip_button_init.style.display = ''
    sip_button_down.style.display = 'none'
    sip_button_hold.style.display = 'none'
    ws_send("unbusy")
    break;
  default:
    console.log('sip_reg', e)
  }
}
function UA_unreg(e) {
    ws_send("busy")
    sip_button_init.style.display = 'none'
    sip_button_down.style.display = 'none'
    sip_button_hold.style.display = 'none'
    console.log('UA. Unregistered', e)         // TODO
    console.info("Перерегистрация телефона")
    //initphoner(localuser['phone'])    // why?
}
function UA_reg_fail(e) {
    console.info('Ошибка при регистрации телефона. Перерегистрация...')
    setInterval(initphoner(localuser.sip), 10*1000)
}
// Обработка входящего вызова
function UA_invite(e) {
    //console.log('UA. Invite', e)        // TODO
    if (activeView != 'main') return    // исключить возможность звонка на стороне сервера

    console.info('Входящий вызов')

    _call = e
    _call.on('progress',  function(e) { call_progress   (e) })
    _call.on('accepted',  function(e) { call_accept     (e) })
    _call.on('rejected',  function(e) { call_reject     (e) })
    _call.on('failed',    function(e) { call_failed     (e) })
    _call.on('terminated',function(e) { call_terminate  (e) })
    _call.on('cancel',    function(e) { call_cancel     (e) })
    _call.on('refer',     function(e) { call_refer      (e) })
    _call.on('replaced',  function(e) { call_replace    (e) })
    _call.on('dtmf',      function(e) { call_dtmf       (e) })
    _call.on('muted',     function(e) { call_mute       (e) })
    _call.on('unmuted',   function(e) { call_unmute     (e) })
    _call.on('bye',       function(e) { call_bye        (e) })

    active_call = {}
    active_call.tag = e.from_tag
    active_call.sip = _call
    active_call.answered = false
    active_call.hold = false
    active_call.direction = "inbound"

    calls[e.from_tag] = active_call

    ws_send('call_push', {tag: e.from_tag}) // сервер отправил на нас звонок
    // мы его получили и говорим серверу, что готовы на него ответить
    // при этом отправляем ему полученный из SIP.js тег звонка
    // после чего получаем структуру с информацией о звонке в функции call_push
}
function UA_message(e) {
    console.log('UA. Message', e)         // TODO
}
function UA_failed(e) {
    console.log('UA. Failed', e)         // TODO
}
//=================================================================
// Call Events
//=================================================================
count_progress = 0
function call_progress(e) {
  switch(e.status_code) {
  case (100):
    console.info('Обмен данными с АТС')
    sip_button_down.style.display = ''
    count_progress = 0
    break;
  case (180):
    if (count_progress < 1) {
      console.info('Соединение...')

      // заменяем в массиве тег исходящего вызова
      // тегом активного соединения
      delete calls[active_call.tag]
      active_call.tag = e.from_tag
      calls[e.from_tag] = active_call
    } else {
      console.info('Вызов абонента')
    }
    call_beep.play()
    count_progress++
    break
  case (183):
    console.info('Поиск абонента')
    break;
  default:
    console.log('Call: PROGRESS', e)
  }
}
function call_accept(e) {
  switch(e.status_code) {
  case (200):
    ws_send('call_accept')
    ws_send('busy')

    call_beep.pause()
    console.info('Начало разговора')

    // при исходящем уведомляем сервер
    // о том что данный тег принадлежит нам
    //if (active_call.direction == "outbound") // в принципе этот метод вызывается только для исходящих
    ws_send('call_invite', {tag: e.from_tag}) 
    // не уведомляем. достаточно call_accept
    // недостаточно! уведомляем только при исходящем

    sip_button_hold.style.display = ''
    sip_button_down.style.display = ''
    call_hold(false)

    active_call.answered = true

    //start_timer('ttimer', 'Звонок на линии')
    break;
  default:
    console.log('Call: ACCEPT', e.status_code)
  }
}
function call_reject(e) {
  switch(e.status_code) {
    case 486:
      console.info('Абонент занят')
      break
    default:
      console.info('Вызов отклонен')
  }
}
function call_failed(e, e_clean) {
  switch(e.status_code) {
    case 486:
      console.info('Абонент занят')
      break;
    case 487:
      console.info('Отмена вызова')
      break;
    case 503:
      console.info('Абонент недоступен')
      break;
    case 603:
      console.info('Абонент не отвечает')
      break;
    default:
      console.info('Ошибка вызова абонента')
      console.debug(e.status_code)
  }
}
function call_cancel(e) {
  console.info('Вызов отменен')
}
function call_refer(e) {
  console.log('Call: REFER', e) // TODO
  console.info('Трансфер')
}
function call_replace(e) {
  console.log('Call: REPLACE', e) // TODO
  console.info('Замена сессии')
}
function call_dtmf(e) {
  console.log('Call: DTMF', e) // TODO
}
function call_mute(e) {
  console.log('Call: MUTE', e) // TODO
}
function call_unmute(e) {
  console.log('Call: unMUTE', e) // TODO
}
function call_bye(e) {
  console.info('Разговор завершен')
}
function call_terminate(e, tender) {
  console.debug("TERM", e, tender, active_call)
  keys = Object.keys(calls)
  call_beep.pause()
  switch (keys.length) {
    case 0:
      // просто пишем в лог. пользователя не уведомляем
      console.log("Нет активных звонков для завершения")
      active_call = undefined
      sip_button_hold.style.display = 'none'
      sip_button_down.style.display = 'none'
      clearInterval(timers['ttimer'])
      clearInterval(timers['ctimer'])
      break
    case 1:
      if (tender)
        break
      delete calls[keys[0]]
      active_call = undefined
      sip_button_hold.style.display = 'none'
      sip_button_down.style.display = 'none'
      clearInterval(timers['ttimer'])
      clearInterval(timers['ctimer'])
      console.info('Линия свободна')
      call_pop()
      break
    case 2:
      console.log("TERM", active_call)
      delete calls[active_call.tag]
      keys = Object.keys(calls)
      active_call = calls[keys[0]]
      call_hold(false)
      console.info('Возврат на первую линию')
      clearInterval(timers['ctimer'])
      break
    default:
      console.error("Превышение количества одновременных звонков На линии")
  }
}

//=================================================================
// Server Call methods
//=================================================================

// Добавление входящего вызова от сервера
function call_push(d) { 
  _call = calls[d['tag']]

  if (!_call) return console.warn('Неопознанная цепочка звонка', d)

  // Заполняем поля для списка
  // Пишем в имя телефон, если не указано.
  if (d['c_name'] == '') d['c_name'] = d['caller_']
  // Пишем телефон вместо организации, если не указано.
  if (d['o_name'] == '') d['o_name'] = d['called']
  /*
  o = orgs[d['o_ref']]
  if (o && o.o_name != '') {
    d.o_name = o.o_name
    d.o_name_ = o.o_name_
  } else {
    d.o_name = d['called']
    d.o_name_ = d['called']
  }
  */
  // Заполняем объект для заполнения формы
  _call['data'] = d

  //fillTemplate('#online_calls', 'template_online', [_call.data])
  ps = document.getElementById('call_inbound_line')
  ps.style.display = ''
  ps.firstElementChild.innerHTML = '<b>' + d.c_name + '</b>'
  ps.lastElementChild.innerHTML = '<b>' + d.o_name + '</b>'

  c_ref_in.innerHTML = d.c_ref
  o_ref_in.innerHTML = d.o_ref

  online_calls.lastElementChild.style.display = 'none'
  // Звонок
  new Audio('/static/media/ring.mp3').play()
  active_call = _call
}
// Удаление входящего вызова от сервера
function call_pop(d) {
  //phoner_call = undefined  // зачем?
  //fillTemplate('#online_calls', 'template_online', {})
  ps = document.getElementById('call_inbound_line')
  if (ps) {
      ps.style.display = 'none'
      online_calls.lastElementChild.style.display = ''
  }
}
//=================================================================
// Common functions
//=================================================================
function get_unanswered_call() {
  _call = undefined
  keys = Object.keys(calls)
  // проходим по всем звонкам в очереди
  for (i = 0; i < keys.length; i++) {
    // если звонок не отвечен - выбираем его
    if (calls[keys[i]].direction == 'inbound') {
      if (!calls[keys[i]].answered) {
        _call = calls[keys[i]] 
        break
      }
    }
  }
  return _call
}
function get_phone_number() {
  console.info("Ввод номера телефона")
  if (active_call) call_hold(true) // нужно бы до выбора номера, но не срабатывает
  $("#phone_number").keyup(function(e) {
    if (e.keyCode == 13) {
      push_action(create_action_green(phone_number.value))
      $("#ui_get_phone_number").dialog("close")
      call_init(phone_number.value)
    }
  })
  $("#ui_get_phone_number").dialog( {
    title: "Введите номер телефона",
    modal: true,
    buttons: {
      "Вызов" : function(e) {
        push_action(create_action_green(phone_number.value))
        $(this).dialog("close")
        call_init(phone_number.value)
      },
      "Отмена": function(e) { 
        //console.log(e)
        call_hold(false)
        $(this).dialog("close")
      },
    }
  })
}
function create_stream() {
  stream = document.createElement('video')
  stream.autoplay = true
  stream.hidden   = true
  sip_streams.appendChild(stream)
  return stream
}

//=================================================================
// Client Call methods
//=================================================================
// Вызывается по зеленой кнопке
// определяет нужно ли звонить или ответить на входящий звонок
function call_init(number) {
  _call = get_unanswered_call()
  keys = Object.keys(calls)

  if (keys.length >= 2) {// если уже есть два звонка на линии - вешаем активный
    //delete calls[keys[0]]
    hangup(active_call.sip)
    //call_terminate(undefined, true)
    //active_call.sip.terminate() // ожидаем вызова call_terminate
  } else if (_call) call_answer() // если есть звонок на линии - отвечаем
  //else calls[keys[0]].sip.dtmf(1) // вместо ответа "да"
  else if (keys.length < 2) { // если нет - звоним исходящий
    //if (number) call_invite(number) // если передан номер с кнопки соединяем
    if (number) ws_send('call_ready', number) // если передан номер с кнопки соединяем
    else get_phone_number() // иначе спрашиваем номер
  }
  else {
    console.error("CALL INIT")
  }
}
// Ответ на входящий вызов
// При наличии входящего вызова
function call_answer(index){ //, data){
  // от сервера при входящем звонке передаются данные для fill_call_form
  _call = get_unanswered_call()
  if (!_call) {
    console.info("Звонки на линии отсутствуют")
    return
  }

  _call.answered = true
  ws_send('call_accept')

  call_opts.media.render.remote = create_stream()
  _call.sip.accept(call_opts) // делаем простой accept
  // здесь не вызывается (!!!) событие on_accepted

  fill_call_form(_call.data) // заполняем и переходим на форму звонка
  change_view('form')
  
  sip_button_hold.style.display = ''
  sip_button_down.style.display = ''
  call_hold(false)

  start_timer('ttimer', 'Звонок на линии')

  //active_call = { tag: _call.from_tag, sip: _call, answered: true, hold: false }

  //g_number = phoner_call.caller // хз зачем
}
// Исходящий вызов на номер
// Вызывается по зеленой кнопке
// Если линия свободна
function call_invite(number) {
  _call = get_unanswered_call()
  if (_call) return console.warn("Линия занята")

  if ( !number ) { return console.info("Телефон не выбран")
  } else if ( number.length == 6 ) {
    number = '83452' + number
  } else if ( number.length == 10) {
    number = '8' + number
  }
  call_hold(true)

  call_opts.media.render.remote = create_stream()
  _call = ua.invite(number, call_opts)

  active_call = {}
  active_call.tag = _call.from_tag
  active_call.sip = _call
  active_call.answered = false
  active_call.hold = false
  active_call.direction = "outbound"

  calls[_call.from_tag] = active_call

  _call.on('progress',  function(e) { call_progress   (e) });
  _call.on('accepted',  function(e) { call_accept     (e) });
  _call.on('rejected',  function(e) { call_reject     (e) });
  _call.on('failed',    function(e) { call_failed     (e) });
  _call.on('terminated',function(e) { call_terminate  (e) });
  _call.on('cancel',    function(e) { call_cancel     (e) });
  _call.on('refer',     function(e) { call_refer      (e) });
  _call.on('replaced',  function(e) { call_replace    (e) });
  _call.on('dtmf',      function(e) { call_dtmf       (e) });
  _call.on('muted',     function(e) { call_mute       (e) });
  _call.on('unmuted',   function(e) { call_unmute     (e) });
  _call.on('bye',       function(e) { call_bye        (e) });

  start_timer('ctimer', 'Исходящее соединение')
}
function hangup(call) {
  if (call == undefined)
    call = active_call.sip

  try {
    call.bye()
  } catch(e) {
    console.log("Невозможно выполнить BYE")
    call.terminate()
  }
}
// Завершение любого вызова (входящий/исходящий)
function call_down(tender) {
  keys = Object.keys(calls)
  console.log('call down', tender, keys.length, calls)
  switch (keys.length) {
    case 0:
      if (tender) return
      console.warn("Нет звонков на линии")
      break
    case 1: // один звонок на линии - завершаем
      if (tender) return
      ws_send('call_down')

      //calls[keys[0]].sip.bye()
      hangup(calls[keys[0]].sip)

      delete calls[keys[0]]
      active_call = undefined
      break
    case 2: // два звонка на линии - отключаемся и соединяем их между собой
      // связать звонки на стороне сервера
      if (active_call.answered) {
        ws_send('call_xfer', { aleg: keys[0], bleg: keys[1] })
        sip_button_down.style.display = 'none'
      } else if (tender) {
        ws_send('call_xfer', { aleg: keys[0], bleg: keys[1] })
        sip_button_down.style.display = 'none'
      } else {
        hangup()
      }
      break
    default:
      console.warn("Превышение количества одновременных звонков На линии")
  }
  return keys.length
}
// Обработка ответа сервера при трансфере
function call_xfer(data) {
  r = data['text']
  if (r == undefined || r.search('Ошибка') != -1) {
    console.error(r)
  } else { // все в порядке
    console.info("Соединение прошло успешно")
    //delete calls[keys[0]] // ожидаем вызова call_terminate, который завершит второй поток
    call_terminate() // вызываем явно
  }
  //if (!goAway) location.reload()
}
// Удержание вызова
function call_hold(trigger) {
  if (!active_call) {
    console.warn("Нет активного звонка для удержания")
    keys = Object.keys(calls)
    active_call = calls[keys[0]]
    if (!active_call) return
  }
  if (trigger != undefined) active_call.hold = !trigger
  
  if (active_call.hold) {
    $('#sip_button_hold').removeClass('on_hold')
    try {
      active_call.sip.unhold()
    } catch (e) {}
    console.info("Абонент на линии")
  } else {
    $('#sip_button_hold').addClass('on_hold')
    try {
      active_call.sip.hold()
    } catch (e) {}
    console.info("Абонент на удержании")
  }
  active_call.hold = !active_call.hold
}

// Активность оператора
function busy(trigger) {

  if (!localuser)
    return

  if (trigger != undefined) {
    localuser.busy = !trigger
  } else {
    if (localuser.busy) ws_send('unbusy')
    else ws_send('busy')
    return
  }

  if (localuser.busy) {
    $('#sip_button_busy').removeClass('on_hold')
    console.log("Оператор готов к приему входящих")
  } else {
    $('#sip_button_busy').addClass('on_hold')
    console.log("Входящие звонки отключены")
  }
  localuser.busy = !localuser.busy
}

// Таймер
var timers = {}
var tEnd   = {}
var tSec   = {}
function start_timer(_name, _text) {
  var elem
  tEnd[_name] = 0
  tSec[_name] = 0

  if (timers[_name])
    clearInterval(timers[_name]);

  elem = document.getElementById(_name)
  if (elem == undefined) {
    elem = document.createElement('span')
    elem.id = _name
    status_bar.appendChild(elem)
  }
  elem.style.display = ""

  timers[_name] = setInterval(
    function () {
      if (tEnd[_name] == 1) return;
      tSec[_name]++;
      elem.innerHTML = _text + ' ' + tSec[_name] + ' сек. ';
    },
  1000);
}

