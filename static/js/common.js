var goAway = true
/*
_debug = false
if (typeof jQuery != 'undefined') {
  _debug = jQuery.cookie('debug')
}
*/
_debug = jQuery.cookie('debug')
_debug = _debug === "true"

function mklog(args, from) {
    dt = new Date().toTimeString().substr(0, 8)
    if (typeof from != 'undefined' && from)
      args = [].slice.apply(args).concat([(new Error()).stack.split(/\n/)[3].trim()]);
    else
      args = [].slice.apply(args)
    args.reverse().push(dt)
    return args.reverse()
}
function ui_alert(title, text) {
  ui_alert_text.innerHTML = text
  $("#ui_alert").dialog( {
    title: title, 
    modal: true,
    buttons: {
      "OK" : function(e) {
        $(this).dialog("close")
      },
    }
  })
}

var _dbg = console.debug;
console.debug = function() {
    if (!_debug) return;
    return _dbg.apply(this, mklog(arguments))
};
if (!_debug) {
  var _log = console.log;
  console.log = function() {
      return _log.apply(this, mklog(arguments))
  }
  var _info = console.info;
  console.info = function() {
      if (!_debug)
        set_status('info', args_to_string(arguments))
      return _info.apply(this, mklog(arguments))
  }
  var _warn = console.warn;
  console.warn = function() {
      if (!_debug)
        set_status('warn', args_to_string(arguments))
      return _warn.apply(this, mklog(arguments))
  }
  var _err = console.error;
  console.error = function() {
      if (!_debug)
        ui_alert("Ошибка", args_to_string(arguments))
        set_status('error', args_to_string(mklog(arguments, true)))
      return _err.apply(this, mklog(arguments, true))
  }
}

function common_onmessage(e) {

    d = $.parseJSON(e.data)

    command = d['command']
    d = d['data']

    switch (command) {
    case 'sysmsg':
      switch (d['level']) {
      case 'ERR':
          console.error(d['text'])
          break
      case 'WRN':
          //console.warn(d['text'])
          break
      default:
          console.info(d['text'])
      }
      break
    case 'globals':
      GLOBALS = d;
      break
    default:
      console.info(command, ':', d)
    }
}

function set_tm(text) {
  if (typeof text_module !== "undefined")
    text_module.innerHTML = text
}

status_timer = 0
function set_status(type, s) {
  var elem

  elem = status_line
  if (elem) {
      switch (type) {
          case 'info':
              elem.className = "btn-success"
              break;
          case 'warn':
              elem.className = "btn-warning"
              break;
          case 'error':
              elem.className = "btn-danger"
              break;
      }
      //console.debug(s);
      elem.innerHTML = s;
  }
  elem = document.createElement('span')
  elem.innerHTML = s + '<br>'
  status_monitor.appendChild(elem)

  if (!status_timer && status_monitor.childElementCount > 5)
    status_timer = setInterval(function () {
      if (status_monitor.childElementCount > 5)
        status_monitor.removeChild(status_monitor.firstChild)
      else {
        status_timer = clearInterval(status_timer)
      }
    }, 2000)

  syslog = document.getElementById('syslog')
  if (syslog) syslog.innerHTML = syslog.innerHTML + s+'<br />'
}

function args_to_string(args) {
    var s = '';
    for(i=0; i < args.length; i++) {
            s += args[i] + ' ';
    }
    return s;
}

function set_info() {
    s = args_to_string(arguments);
    set_status('info', s);
}

function set_warning() {
    s = args_to_string(arguments);
    set_status('warn', s);
}

function set_error() {
    s = args_to_string(arguments);
    set_status('error', s);
}

/*
 * Закрывает указанный диалог
 * Если диалог не указан, закрывает все открытые диалоги
 */
async function close_dialog(dialog) {
    if (typeof dialog === 'undefined') {
        $('.dialog_box').each( (k, dialog) => {
            if ($(dialog).hasClass("ui-dialog-content") && $(dialog).dialog('isOpen'))
                $(dialog).dialog('close')
        })
    } else {
        if ($(dialog).hasClass("ui-dialog-content") && $(dialog).dialog('isOpen'))
            $(dialog).dialog('close')
    }
}

/* 
 * Находит элемент на странице
 * Если на странице элемент не найден, ищет элемент во всем документе
 */
function find_el_in_page(page, id) {
    return $(page).find(id)[0] && $(page).find(id).first() || $(id)
}

/*
 * Ожидание выполнения длительного действия
 */
function load() {
    document.getElementById('load').style.display = ''
    $('#load').addClass('active');
}

/*
 * Завершение ожидания
 */
function unload() {
    document.getElementById('load').style.display = 'none'
    $('#load').removeClass('active');
}

function compare_by(field, a, b) {
    if (a[field] < b[field]) return -1
    if (a[field] > b[field]) return 1
    return 0
}
const api_call = (method, data, wheel) => {
    return new Promise( (resolve, reject) => {

        let url = '/api_' + method
        console.log("API CALL", url)

        if (data && typeof data === 'object') {
            url += '?'
            if (data.tagName && data.tagName === 'FORM') {
                url += $(data).serialize()
            } else {
                url += Object.keys(data).map(function(k) {
                        return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
                }).join('&')
            }
        } else if (typeof data !== 'undefined') {
            console.log("API", typeof data, data)
            return
        }

        if (wheel !== false) load()

        $.getJSON(url,
            success => {
                unload()
                resolve(success)
            },
            error => {
                unload()
                reject(error) 
            }
        )
    })
}

function open_dialog(id, page, title, data, action, period) {
    let dialog = document.getElementById(id).cloneNode(true)
    try {
        dialog = dialog.cloneNode(true)
    } catch (e) {
        return console.error("Диалог не найден", id)
    }
    let args = ''
    let params = {
        // modal: true,
        resizable: false,
        buttons: {}
    }

    Object.keys(data).forEach(k => {
        let el = dialog.querySelector(k)
        if (el)
            if (el.tagName == 'INPUT') {
                el.value = data[k]
            } else {
                el.innerText = data[k]
            }
        args += args && '_'
        args += data[k]
    })
    args = args.replace(/[ :\/]/g, '')

    Object.keys(action).forEach(k => {
        let el = dialog.querySelector(k)
        if (el) {
            if (typeof action[k] === 'object') {
                el.onclick = () => front_page(`${page}_${args}`, title, true, false, action[k])
            } else if (typeof action[k] === 'string') {
                let e = action[k].split(':')
                el.setAttribute(e[0], e[1])
            } else if (typeof action[k] === 'function') {
                el.onclick = () =>  action[k](dialog)
            } else {
            }
		} else {
            if (typeof action[k] === 'object') {
                console.log(k, action[k])
                params.buttons[k] = () => front_page(`${page}_${args}`, title, true, false, action[k])
            } else {
                params.buttons[k] = () => action[k](dialog)
            }
		}
    })

    if (dialog.querySelector('.datepicker'))
        params.open = () => {
            $(dialog).find('.datepicker').datepicker({
                dateFormat: "dd.mm.yy",
                minDate: 0,
                onSelect: (date) => {
                    console.log('date', date);
                    dialog.querySelector('.date').value = date
                }
            });
        }

    $(dialog).dialog(params)
}

