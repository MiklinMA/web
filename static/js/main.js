//=================================================================
// VARs
//=================================================================
var GLOBALS = {}
var dnd = false
var abuse = false
var service_load = false
var activeView = ''
var vd = undefined
var actions = {}
var version = "0.9.2"
//=================================================================
// Main
//=================================================================

function loadJS(url, callback){
    stamp = new Date().getMilliseconds()
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url + '?' + stamp;

    script.onreadystatechange = callback;
    script.onload = callback;

    head.appendChild(script);
}

loadJS('/static/js/common.js', function() {
  loadJS('/static/js/ws.js', function() {

    $.cookie('upath', location.pathname);
    /*
    loadJS('/static/js/angular.js',
      function () {
          console.info("Модуль Angular загружен")
      }
    );
    */

    switch (location.pathname) {
      case '/':
        loadJS('/static/js/front/ws.js', function() { front_ws_onload() })
        break
      case '/call':
        loadJS('/static/js/phoner.js', function() { sip_onload() })
        break
      case '/nps':
        loadJS('/static/js/phoner.js', function() {
          loadJS('/static/js/cb.js', function() {
            loadJS('/static/js/name.js', function() {
              cb_onload()
            })
          })
        })
        break
      case '/admin':
        loadJS('/static/js/admin.js', function() { ws_connect() })
        break
      case '/front':
          loadJS('/static/js/phoner.js', function() {
              loadJS('/static/js/front/front.js', function() {
                  front_onload()
              });
          });
          break
      default:
        ws_connect()
    }
  })
})

//=================================================================
//                           restruct
//=================================================================
onload = function (){
  ps = $('.c_phone');
  if (ps){
    ps.mask("8 (999) 999-99-99", {placeholder:" "});
  }
  // bear 20171019
  // if ( document.getElementById('o_ref') ){
  //   o_ref.onchange = function (){onchange_oref()}
  // }

  /*
  if ( document.getElementById('comment_editor') ){
    comment_editor.onkeyup =  function(){onkeyup_ce(this)}
  }
  */

  if ( document.getElementById('sex') ){
    sex_span = sex.getElementsByTagName('span')
    for (i = 0; i < sex_span.length; i++)
      sex_span[i].onclick = function onclick(){onclick_sex(this)}
  }

  /*
  var inputs = document.getElementsByTagName('input');
  for (var i = 0; i < inputs.length; i += 1) {
    if (inputs[i].id == "input_qstring") continue
    if (inputs[i].id == "username") continue
    if (inputs[i].id == "password") continue
    if (inputs[i].id == "phone_number") continue
    if (inputs[i].id == "name") continue
    if (inputs[i].id == "auto") continue
    if (inputs[i].id == "tel") continue
    if (inputs[i].id == "is_guarantee") continue
	
    inputs[i].onchange = function(e) {
      field_update(this.id || this.name, this.value)
    }
  }
  */

  console.info('Основной моуль js загружен ( версия: ' + version + ' )')
}

document.onkeydown = function (event){
  event=event||window.event;
  if (event.keyCode == 27)  call_down()  // ESC

  //if (e.keyCode == 38)  call_init()  // Up arrow
  //if (e.keyCode == 40)  call_down()  // Down arrow

  if (event.keyCode == 107)  call_init() // +
  if (event.keyCode == 109)  call_down() // -
  if (event.keyCode == 106)  call_hold() // *
}

// отправляет в сокет информацию об измененных данных
function field_update(k, v) {
  d = {}
  //console.log(k, v)
  d.e_ref = e_ref.value

  if (k.indexOf('phone_') != -1) {
    ps = []
    for (k = 0; k < c_phones.childElementCount; k++) {
      v = document.getElementById('phone_' + k)
      if (v && v.value) ps.push(v.value)
      else c_phones.removeChild(c_phones.children[k])
    }
    d['c_phones'] = ps
  } else if (k.indexOf('email_') != -1) {
    d['c_email'] = email_user.value + '@' + email_host.value
  } else {
    d[k] = v
  }

  console.log(d)
  ws_send('update', d)
}

function create_action() {
  e = {}
  e.stamp = new Date()
  e.stamp = e.stamp.toISOString()
  e.kind  = 'call'
  e.state = 'init'
  return e
}
function create_action_green(number) {
  e = create_action()
  e.dst_p = number
  e.dst_k = "main"
  return e
}
function create_action_call(elem) {
  console.log("push_action", elem)
  _elem = elem

  e = create_action()
  e.dst_p = elem.innerText

  if      (elem.classList.contains( "cell")) e.dst_k = "cell"
  else if (elem.classList.contains("phone")) e.dst_k = "inner"
  else if (elem.classList.contains("group")) e.dst_k = "group"
  else if (elem.classList.contains("green")) e.dst_k = "main"
  else                                       e.dst_k = "unknown"
  
  if (e.dst_k == "cell" || e.dst_k == "inner") {
    // хитрый доступ до ссылки на менеджера
    try {
      e.dst_r = elem.parentElement.parentElement.firstElementChild.firstElementChild.value
    } catch (e) {
      e.dst_r = "Ссылка отсутствует"
    }

    // хитрый доступ до имени менеджера
    try {
      e.dst_n = elem.parentElement.parentElement.firstElementChild.innerText
    } catch (e) {
      e.dst_n = "Имя менеджера отсутствует"
    }
  }
  return e
}

function push_action(e, kind) {
  if (kind != undefined) e.kind = kind
  //stamp = new Date().getMilliseconds()
  actions[performance.now()] = e
  console.log("actions", actions)
}

function onclick_call_group(elem) {
    ws_send('call_group', {
        o_ref: o_ref,
        d_ref: d_ref.value,
        }
    )
}

function onclick_cb(elem){

  push_action(create_action_call(elem))

  _elem = elem
  console.log(elem)

  if (elem.querySelectorAll('span')[0]){
    phone = elem.querySelectorAll('span')[0].innerText
  } else if (elem.innerText) {
    phone = elem.innerText
  } else {
    phone = elem.value
  }

  if (phone) {
    phone = phone.replace(/[A-Za-zА-Яа-яЁё\- ()]/g, "")
    if (phone.length == 10) {
      phone = '8' + phone
    }

    all_cb = document.getElementById('managers').getElementsByClassName('on_hold')
    while (all_cb.length > 0){
      all_cb[0].classList.remove('on_hold')
    }

    elem.classList.add('on_hold')
    call_init(phone)
  }
}

function onclick_sex(elem){
  elem.querySelectorAll('input[type=radio]')[0].checked =true

  sex_span = sex.getElementsByClassName('btn-success')
  if (sex_span.length > 0) {
    sex_span[0].classList.remove('btn-success')
  }
  elem.classList.add('btn-success')
}

function onclick_targets(elem){
  radio = elem.querySelectorAll('input[type=radio]')[0]
  radio.checked = true

  target_r_phone = document.getElementById('r_phone' + radio.value)
  document.getElementById('d_phone').value = '';

  old_elem = document.getElementById('targets').getElementsByClassName('btn-success')
  if (old_elem.length > 0){
    old_elem[0].classList.remove('btn-success')
  }

  elem.classList.add("btn-success")

  if (radio.value == -1) {
      managers_container.classList.add('hidden')
      data_container.classList.add('width100')

      switch (location.pathname) {
      case '/call':
          get_managers(-1);
          break;
      case '/meet':
          memo_container.classList.add('width70')
          fio_container.classList.add('width30')
          break;
      }
  } else {
      managers_container.classList.remove('hidden')
      data_container = document.getElementById('data_container')
      if (data_container) {
        data_container.classList.remove('width100')
      }

      if (target_r_phone && target_r_phone.value != 'None') {
        r_phone.value = target_r_phone.value
        //r_sms.style.display = ''
      } else {
        //r_sms.style.display = 'none'
      }

      if (location.pathname == '/meet') {
        memo_container.classList.remove('width70')
        fio_container.classList.remove('width30')

        get_managers(radio.value);
      } else if(location.pathname == '/call') {
        set_tm('')

        if(abuse_container.style.display != 'none'){
          abuse_container.style.display = 'none'
          abuse_end.style.display = 'none'
          managers_list.style.display = ''
          buttons_form.style.display = ''
        }

        if(service_container.style.display != 'none'){
          service_container.style.display = 'none'
          cf_butt.style.display = ''
          serv_send.style.display = 'none'

          am_model.innerHTML = ''
          service_time_our.innerHTML = ''
          freetime.innerHTML = ''
          works.innerHTML = ''

          navigate('step', 1)
        }

        console.log(radio.value)
        switch (Number(radio.value)) {
        case 101: // TRIGGER 1
          btns = [
            ['ТО',      'success', 'service_auto()'],
            ['Прочее',  'warning', 'get_managers('+radio.value+')'],
          ]
          create_buttons(selector, btns)
          managers_container.style.display = 'none'
          qstring.style.display = 'none';
          selector.style.display = ''
          s = 'М'
          if (c_name.value.length)
            s = c_name.value + ', м'
          s += 'огу я уточнить цель Вашего звонка?'

          set_tm(s)
          break
        case 3:
          fillTemplate('#managers', 'template_managers', {});
          managers_container.style.display = ''
          selector.style.display = 'none'
          qstring.style.display = '';
          call_dep_btn.style.display = 'none'

          get_managers(radio.value);
          break
        default:
          managers_container.style.display = ''
          selector.style.display = 'none'
          qstring.style.display = 'none';

          get_managers(radio.value);
        }

        t = targets[radio.value]
        if (t) {
            t = t['text']
            if (t) {
                text_module_org.style.display = "none"
                text_module_dep.style.display = ""
            } else {
                text_module_org.style.display = ""
                text_module_dep.style.display = "none"
            }
            text_module_dep.innerHTML = t
        }
      }
  }

  try{
    managers.getElementsByClassName('active_manager')[0].classList.remove('active_manager')
  } catch (error) {}

  am = document.getElementById('active_manager')
  if(am)
    am.value = null
}

function onclick_manager(elem){
  try {
    dep_ref.value = elem.querySelectorAll('input[type=hidden]')[0].value
  } catch (error) {}
}

function onkeyup_ce(elem){
  event=event||window.event;
  if (event.keyCode == 13) {
    var val = elem.innerText;
		
    if (val == "\n" || val == "\n\n") {
      elem.innerText = '';
      return;
    }
    if (val == "") {
      elem.innerText = '';
      return
    }
    var comment = document.getElementById('c_memo')//$('#c_memo');
    var d = new Date();
    y = d.getFullYear();
    M = d.getMonth() + 1;
    h = d.getHours();
    m = d.getMinutes();
    d = d.getDate();

    if (M < 10) M = '0' + M;
    if (d < 10) d = '0' + d;
    if (h < 10) h = '0' + h;
    if (m < 10) m = '0' + m;

    d = '' + d + '.' + M + '.' + y;
    d += ' ' + h + ':' + m;

    val = val.replace(/\n+$/m, '')
	/*
    new_text = d + " : "  + val + '\n';
    comment.innerHTML = comment.innerHTML+ new_text ;
	*/
    elem.innerText = '';
	
    events = document.getElementById('events')

    tr = document.createElement('tr')
    td = document.createElement('td')
    td.innerHTML = d
    tr.appendChild(td)
    
    td = document.createElement('td')
    td.innerHTML = val
    tr.appendChild(td)
    
    td = document.createElement('td')
    td.innerHTML = localuser.fullname
    tr.appendChild(td)
    
    events.innerHTML = "<tr>" + tr.innerHTML + "</tr>" + events.innerHTML
    
    //ws_send('add_comment', val)
    data = {}
    data[d] = {
      'text': val,
      'author': localuser.fullname
      }
    field_update('events', data)
  }
}

function onchange_oref(){
  o_ref = document.getElementById('o_ref')
  oref_val = o_ref.options[o_ref.selectedIndex].value
  if (service_container.style.display != 'none'){
    am_model.innerHTML = ''
    freetime.innerHTML =''
    service_time_our.innerHTML = ''
    works.innerHTML =''
    navigate('step', 1)

    getModels()
    getFreetime()
    getPrice()
  }else{
    get_targets(oref_val);
    get_managers(-1);

    document.getElementById('text_module_org').style.display = ""
    document.getElementById('text_module_dep').style.display = "none"

    document.getElementById('text_module_org').innerHTML = orgs[oref_val]['text']
    document.getElementById('text_module_dep').innerHTML = ""

    if(document.getElementById('qstring').style.display != 'none')
      document.getElementById('qstring').style.display = 'none';
  }
  field_update('o_ref', oref_val)
  field_update('manager', '')
}
//=================================================================
//                           restruct
//=================================================================

//=================================================================
//                         GET FUNCTIONS
//=================================================================
function get_targets(o_ref) {
    //console.debug(o_ref)
    ws_send('sql_targets', {'o_ref': o_ref})
}

function get_managers(target_ref) {

  o_ref = $.cookie('o_ref')
  if (!o_ref) {
      o_ref = document.getElementById('o_ref')
      if (o_ref) o_ref = o_ref.value
  }

  //console.debug(o_ref, target_ref)
  if (!o_ref) return;
  if (!target_ref) return;

  ws_send('sql_managers', {
      'o_ref': o_ref,
      'target': target_ref,
      'upath': location.pathname == '/call' ? '1': '0',
  })

  v = document.getElementById('d_ref_' + target_ref)
  if (v && v.value)
    field_update('d_ref', v.value)
    field_update('manager', '')
}

function get_contacts(text) {
    //console.debug(o_ref)
    ws_send('sql_contacts', {'text': text})
}
//=================================================================
//                         GET FUNCTIONS
//=================================================================

//=================================================================
//                         FILL FUNCTIONS
//=================================================================
function fill_targets(data) {
  fillTemplate('#targets', 'template_targets', data);
  targ = document.getElementById('targets')
  if (targ){
    targ_span = targ.getElementsByTagName('span')
    if(targ_span){
      for (i = 0; i < targ_span.length; i++) {
        targ_span[i].onclick = function onclick(){onclick_targets(this)}
        if (vd) {
          if(i == vd){
            onclick_targets(targ_span[i])
          }
        }
      }
    }
  }
  fill_managers({})
}

function fill_managers(data) {
    selector.style.display = 'none'
    managers_container.style.display = ''
    //_data = data
    fillTemplate('#managers', 'template_managers', data);
    
    //for (k in data) {
    //  line = data[k]
    //  tr = document.createElement('tr')
    //  tr.id = k
    //  tr.onclick = select_managers(this)
    //  td = document.createElement('td')
    //}

    m_tr = managers.getElementsByTagName('tr')
    for (i = 1; i < m_tr.length; i++){
      elem = m_tr[i]
      m = elem.getElementsByTagName('input')[0].value

      if (!m) return

      if (typeof call != 'undefined' && call['c_managers'].indexOf(m.toLowerCase()) >= 0){
        elem.classList.add('friendly_manager')
      }

      am = document.getElementById('active_manager')
      if(am){
        if (m == active_manager.value){
          elem.classList.add('active_manager')
          radio.checked = true
        }
      }
    }

    call_dep_btn = document.getElementById('call_dep_btn')
    if (call_dep_btn){
      call_dep_btn.style.display = 'none'
    }
    if(document.getElementById('manager_table')){
      manager_table.classList.remove('col-md-10')
      manager_table.classList.add('col-md-12')
    }

    d_phone = null
    for (e in data) {
        document.getElementById('d_ref').value = data[e].dep_ref;
        if (location.pathname == '/meet') break

        d_phone = data[e].dep_phone;
        if (d_phone) {
            document.getElementById('d_phone').value = d_phone;
            document.getElementById('label_phone').innerHTML = d_phone;
            document.getElementById('call_dep_btn').style.display = '';
            manager_table.classList.remove('col-md-12')
            manager_table.classList.add('col-md-10')
        } else {
            break
        }
    }

    if (document.getElementById('managers').rows.length >1) {
      document.getElementById('m_tbody').style.display = 'block';

      if (location.pathname == '/meet') {
        m_tbody.style.maxHeight = ''
        m_tbody.style.display = ''
        managers.style.marginBottom = '0'
        document.getElementById('manager0').getElementsByTagName('td')[0].style.width = '100%'
      }

      if (location.pathname == '/call'){
        redioval = document.querySelectorAll('#targets .btn-success input[type=radio]')[0].value

        if(text_module_dep.style.display != 'none'){
          tmd = text_module_dep.offsetHeight
          m_tbody.style.maxHeight = 335 - tmd + 'px'
        }

        if(redioval == 3){
          m_tbody.style.maxHeight = '295px'
        }
      }

      resize_table();
    } else
      document.getElementById('m_tbody').style.display = '';

    all_cb = document.getElementsByClassName('call-button')
    if(all_cb.length > 0){
      for (i = 0; i < all_cb.length; i++){
        all_cb[i].onclick = function onclick(){onclick_cb(this)}
      }
    }
    call_dep_btn.onclick = function onclick(){onclick_call_group(this)}

    if(location.pathname == '/meet'){
      all_td = document.getElementById('managers').getElementsByTagName('td')
      if(all_td.length > 0){
        for (i = 0; i < all_td.length; i++){
          all_td[i].onclick = function onclick(){onclick_manager(this)}
        }
      }
    }
}

function fill_contacts(data) {
    if (document.getElementById('form')) {
        if (document.getElementById('form').style.display != 'none') {
            if (!document.getElementById('managers')) return
            fillTemplate('#managers', 'template_managers', data);

            if (document.getElementById('managers').rows.length > 1) {
                document.getElementById('m_tbody').style.display = 'block';

                redioval = document.querySelectorAll('#targets .btn-success input[type=radio]')[0].value
                if (redioval == 3) {
                    m_tbody.style.maxHeight = '295px'
                }

                resize_table();
            } else {
                document.getElementById('m_tbody').style.display = '';
            }
        } else {
            if (!document.getElementById('contacts')) return

            if (Object.keys(data).length) document.getElementById('contacts').style.border = ''
            else document.getElementById('contacts').style.border = 'none'
            fillTemplate('#contacts', 'template_contacts', data);
        }
    }
}

function fillTemplate(container, template, data) {
    container = $(container);
    if (container.length) {
        container.setTemplateElement(template, [], {filter_data: false});
        container.processTemplate(
            {
                count: Object.keys(data).length + 1,
                data: data,
            }
        );
    }
}

function fill_call_form(d) {
  console.log(d)
    if (d['c_name'] == d['caller_'])
        d['c_name'] = ''

    if (d['vd'])
        vd = d['vd']

    for (k in d) {
        elem = document.getElementById(k)
        if (elem)
            switch (k) {
            case 'c_memo':
                elem.innerHTML = d[k]
                //elem.innerHTML = '\n' + d[k]
                break
            case 'c_phones':
                continue
            case 'c_email':
                r = d[k].match(/(\S+)@(\S+)/)
                if (r && r.length == 3) {
                    document.getElementById('email_user').value = r[1]
                    document.getElementById('email_host').value = r[2]
                }
                break
            default:
                elem.value = d[k]
            }
    }

    //document.getElementById('test_vera').value = 'gggggggg'
	//test_vera.value = 'fdsa'
	//_dd = d
	//test_vera.value = d['events']
	
	ed = d['events']
	events = document.getElementById('events')
	for (edk in ed) {
		tr = document.createElement('tr')
		td = document.createElement('td')
		td.innerHTML = ed[edk].date
		tr.appendChild(td)
		
		td = document.createElement('td')
		td.innerHTML = ed[edk].type
		tr.appendChild(td)
		
	    td = document.createElement('td')
		td.innerHTML = ed[edk].manager
		tr.appendChild(td)
		
		events.appendChild(tr)
	}
	
    ps = document.getElementById('c_phones')

    for (k in d['c_phones']) {
      p = ps.firstElementChild

      if (k != 0) {
          c = p.cloneNode(true)
          ps.appendChild(c)
      } else {
          c = p
      }

      //c.value = d['c_phones'][k]
      z = c.lastElementChild
      console.log("OK", c, z)
      if (d['c_phones'][k] == 'Anonymous') {
        z.value = '0000000000'
      } else {
        z.value = d['c_phones'][k]
      }
    }
    ps = $('.c_phone');
    if (ps && ps.val().length >= 10) ps.mask("(999) 999-99-99", {placeholder:" "})

    get_targets(d['o_ref'])
}

function fill_meet_form(d) {
    os = document.getElementById('orgs')
    ws_send('ones_init', {
        'o_ref': os.value,
    })
    document.getElementById('author').value = localuser.ref
    change_view('form')
}

function missed_call(data){
  ws_send('get_missed', {'k': data})
}

function fill_missed_call(data) {
  if (typeof(_call) != "undefined") return

  d = data['data']
  //d['e_ref'] = data['e_ref']
  if (d['c_phones'].indexOf(d['caller']) < 0)
    d['c_phones'].push(d['caller'])

  g_number = d['caller']
  if (g_number.length == 10) g_number = '8' + g_number

  //sip_init_button.onclick = function(e) { call_to_client() }

  fill_call_form(d)
  change_view('form')
  //$('#missed').val(true)
  missed.value = true

  call_hold(true)
}
//=================================================================
//                         FILL FUNCTIONS
//=================================================================

window.onunload = function() {
  //ws_send("busy") // WTF?
  if (location.pathname.substr(0, 5) == '/call') {
    if (location.pathname == '/nps' && result_sent == false) {
      //send_results('no_answer')
      	dnd = true
        send_results('onunload')
    }
    //call_down(true)
  }

  if (goAway && localuser) logout()
}

function addmin() {
    waitings = document.getElementsByClassName('waiting')
    while (waitings.length > 0){
      v = waitings[0].innerText
      v = parseInt(v) + 1
      if (!isNaN(v)){
        waitings[0].innerText = v + ' мин'
      }
      w = waitings[0]
      w.setAttribute('class','text-center')
      if (v > 20){
        w.classList.add('danger')
      }else if (v > 10){
        w.classList.add('warning')
      }
    }
}

function add_phone() {
    /*
    ps = $("#c_phones");
    ok = true;
    ps.each(function(index){
      console.log("OK", $(this).last())
        if ($(this).last().val() == '') {
            $(this).last().focus();
            ok = false;
            return;
        }

    });
    if (!ok) return;
    */

    p = c_phones.firstElementChild
    c = p.cloneNode(true)
    cin = c.lastElementChild
    cin.id = 'phone_' + (c_phones.childElementCount)
    cin.onchange = function() { field_update(this.id, this.value) }
    cin.value = ''
    $(cin).mask("(999) 999-99-99", {placeholder:" "});
    c_phones.appendChild(c)
}

function authenticate() {
    test = false
    test_allowed = [
        'miklin',
        //'dasadchikova',
        //'yukarimova',
        'kdenisova',
        'eaksenova',
    ]
    if (test && test_allowed.indexOf(document.getElementById('username').value) < 0) {
        set_error('Режим тестирования. Будьте внимательны')
        return
    }
    change_view('wheel')
    ws_send('login', {
        'username': document.getElementById('username').value,
        'password': document.getElementById('password').value
    })
}

function change_view(active) {
    if (!active) active = 'wheel'

    p = document.getElementsByClassName('page_view')
    for (k in p) {
        if (p[k].id == active) {
            p[k].style.display = ''
            activeView = active
        } else {
            if (p[k].style) p[k].style.display = 'none'
        }
    }
}

function call_manager(man) {
  select_managers(man)
  check_call_form(man)
}

function check_call_form(man) {
    // перед проверкой/отправкой формы
    /*
    try {
      if (active_call != undefined) {
        // сначала соединяем!
        call_down()
        change_view('wheel')
        setTimeout(function() { check_call_form() }, 1000)
        return
      }
    } catch (e) {
      console.warn('submit_form', e)
    }
    */
    // активного звонка нет. 
    // или клиент соединен с менеджером
    // или соединения не было

    action = document.querySelectorAll('input[type=hidden][name=action]')[0]
    target = document.querySelectorAll('input[type=radio][name=target]:checked')[0]

    if (action)
      action = action.value

    if (target)
      target = target.value

    fields = [
        ['radio', 'targets' , 'target'      ],

        //['input', 'c_name'  , ''            ],
        ['input', 'c_phone' , ''            ],
        //['input', 'email'   , 'email_user'  ],
        //['input', 'email'   , 'email_host'  ],
    ]
    //if (man) fields.push(['radio', 'managers', 'manager']);
    /*
    if (action == 'send' && target != '3') {
      // 20170126
      // убрал проверку поля менеджер
      // на менеджера отправляем по кнопке
      //fields.push(['radio', 'managers', 'manager']);
    } else{
        //fields.push(['textarea', 'comment', '']);
    }
    */
    if (abuse == true) {
      fields = [
          ['textarea','abuse_text']
        ]
    }
    data = [
        '#e_ref',
        '#c_name',
        '#c_ref',
        '#o_ref',
        '#email_user',
        '#email_host',
        '#c_memo',
        '#action',
        '#author',
        '=target',
        '=manager',// только по кнопке
        '.c_phone',
        '#missed',

        '$abuse',
        '#abuse_text',
        '#v_mp',
        '#v_dep',
        '#v_time',
        '#v_parts',
        '#v_re',
        '#v_price',
        '#v_other',
				'&campaign'
    ]
    //if (man) data.push('=manager')

    if (!check_form(action, target, fields, data))
      action.value = 'send'
}

function check_form(action, target, fields, data) {
    required = document.getElementsByClassName('required')
    while (required.length > 0){
      required[0].classList.remove('required')
    }

    proceed = true;

    fields.forEach(function(entry) {
        type = entry[0];
        field = entry[1];
        item = entry[2];

        switch (type) {
            case 'radio':
                ok = (document.querySelectorAll('input[type=radio][name='+item+']:checked').length > 0)
                break;
            case 'input':
                if (item.length > 0)
                  el = document.querySelectorAll('input[name='+item+']')[0]
                  if (typeof el != 'undefined')
                    ok = (el.value.length > 0)
                else
                  el = document.querySelectorAll('input[name='+field+']')[0]
                  if (typeof el != 'undefined')
                    ok = (el.value.length > 0)
                break;
            case 'textarea':
                el = document.querySelectorAll('textarea#'+field)[0]
                if (typeof el != 'undefined')
                  ok = (el.value.length > 0)
                break;
            default:
                ok = true;
        }

        if (! ok) {
            //console.log(field)
            if( document.getElementById(field))
              document.getElementById(field).classList.add('required')
            proceed = false;
        }
    });

    console.log(action, target, proceed);
    if (proceed) {
        if (action.value == 'busy')
          if (!confirm('Все менеджеры заняты?'))
            return false

        submit_form(data);
    } else {
      return false
    }
}

function check_meet_form() {
    action = document.querySelectorAll('input[type=hidden][name=action]')[0].value
    target = document.querySelectorAll('input[type=radio][name=target]:checked')[0].value

    fields = [
        ['radio', 'targets' , 'target'      ],
        ['radio', 'sex'     , 'sex'         ],

        //['input', 'email'   , 'email_user'  ],
        //['input', 'email'   , 'email_host'  ],
    ]
    if (action == 'send' && target != '3') {
        fields.push(['radio', 'managers', 'manager']);
    } else {
        fields.push(['textarea', 'c_memo', '']);
    }
    data = [
        '#e_ref',
        '=sex',
        '#name',
        '#phone',
        '#email_user',
        '#email_host',
        '#c_memo',
        '#action',
        '#author',
        '#o_ref',
        '#dep_ref',
        '=target',
        '=manager',
    ]
    check_form(action, target, fields, data);
}

function create_buttons(owner, buttons) {
  owner.innerHTML = ''

  if (buttons.length > 9) cols = 4
  else if (buttons.length > 6) cols = 3
  else if (buttons.length > 3) cols = 2
  else cols = 1

  for (i = 0; i < buttons.length; i++) {
    btn = document.createElement('span')
    btn.className = "btn-"+buttons[i][1]+" btn btn-lg"
    btn.style.cssText = "width: 150px; margin-bottom: 8px"
    btn.innerHTML = buttons[i][0]
    btn.setAttribute('onclick', buttons[i][2])
    owner.appendChild(btn)

    if (i % cols == 0) {
      btn = document.createElement('br')
      owner.appendChild(btn)
    }
  }
}

function abuse_btn(){
  abuse_container.style.display = ''
  abuse_end.style.display = ''
  managers_list.style.display = 'none'
  buttons_form.style.display = 'none'
}

function navigate(name, id) {
  steps = document.getElementsByClassName(name)
  for (i = 0; i < steps.length; i++) {
    steps[i].style.display = 'none'
  }

  step = document.getElementById(name + id)
  if (step) step.style.display = ''
}

function phone_deformat(phone) {
    if (!phone) return phone;

    phone = phone.replace(/\(/g, '');
    phone = phone.replace(/\)/g, '');
    phone = phone.replace(/-/g, '');
    phone = phone.replace(/ /g, '');

    return phone;
}

function resize_table() {
  return
  if ( document.getElementById('m_thead') ) {
    th = m_thead.getElementsByTagName('th')
    manager0 = document.getElementById('manager0')

    if ( manager0 ){
      td = manager0.getElementsByTagName('td')

      for (i = 0; i < td.length; i++) {
        c = td[i].offsetWidth+'px';
        if (th[i])
          th[i].style.width = c;
      }
    }
  }
}

function select_org() {
    os = document.getElementById('orgs')
    if (os) {
        $.cookie('o_ref', os.value)
        change_view('main')
        get_targets(os.value)
    }
}

function select_managers(elem){
  // волшебный код
  radio = elem.querySelectorAll('input[type=radio][name=manager]')[0] || elem.parentElement.parentElement.querySelectorAll('input[type=radio][name=manager]')[0]
  radio.checked = true

  am = m_tbody.getElementsByClassName('active_manager')

  if(am.length){
    am[0].classList.remove('active_manager')
  }

  elem.classList.add('active_manager')

  act_man = document.getElementById('active_manager')
  if(act_man){
    act_man.value = radio.value
  }
  field_update('manager', radio.value)
}

function service_auto() {
  selector.style.display = 'none'
  cf_butt.style.display = 'none'
  service_container.style.display = ''
  serv_send.style.display = ''
  set_tm('')

  if(service_load == false){
    question1 = 'точните, пожалуйста, текущий пробег Вашего автомобиля.'
    question2 = 'ля прохождения ТО, могу предложить вам следующее время:'

    if(c_name.value.length){
      question1 = c_name.value+', у'+question1
      question2 = c_name.value+', д'+question2
    }else{
      question1 = 'У'+question1
      question2 = 'Д'+question2
    }

    tms = document.getElementsByClassName('tmodule')
    Array.prototype.forEach.call(tms, function (tm) {
      tm.innerText = tm.innerText.replace('{text_step2}', question1)
      tm.innerText = tm.innerText.replace('{text_step4}', question2)
    });

    loadJS('/static/js/service.js',function (){service_load = true})
    ws.onmessage = function(e) { service_onmessage(e) }
  }else{
    getModels()
    getPrice()
    getFreetime()
  }
}

function submit_form(fields) {
    data = {}
    for ( i = 0; i < fields.length; i++ ){
      entry = fields[i]
      if (entry.substr(0, 1) == '#') {
        try{
          v = document.getElementById(entry.substr(1)).value
        } catch (error) {
          v = ''
        }
        data[entry.substr(1)] = v

      } else if (entry.substr(0, 1) == '.') {
          ds = document.getElementsByClassName(entry.substr(1))
          arr = []
          for (j = 0; j < ds.length; j++) {
            v = ds[j].value
            arr.push(v)
          }
          data[entry.substr(1)] = arr
      } else if (entry.substr(0, 1) == '=') {
          try{
            v = document.querySelectorAll('[name='+ entry.substr(1) +']:checked')[0].value
          } catch (error) {
            v = ''
          }

          data[entry.substr(1)] = v
      } else if (entry.substr(0, 1) == '$') {
          k = entry.substr(1)
          data[k] = eval(k)
      } else if (entry.substr(0, 1) == '&'){
				try{
					v = document.getElementById(entry.substr(1)).checked
        } catch (error) {
					v = false
				}
				data[entry.substr(1)] = v
			}
    }

    /*
    if (typeof(recs) == "object") {
      data.recs = recs
    }
    data.user = {}
    data.user.ref = localuser.ref

    data.actions = actions
    */
    if (location.pathname == '/call') {
        //call_down(true)
        keys = Object.keys(calls)
        if (keys.length == 2) {
          ws_send('call_xfer', { aleg: keys[0], bleg: keys[1] })
          change_view('wheel')
          goAway = false
          setTimeout(function() {
            //ws_send('direct', data)
            location.reload()
          }, 1000)
          console.info('Клиент соединен')
          _debug = true
          return
        } else {
          console.log("SUBMIT", action, data)
          if (action == 'busy')
            ws_send('missed', data)
          else
            ws_send('direct', data)
          console.log(data)
          goAway = false
          location.reload()
          return
        }
/*
        if(data['e_ref'].indexOf(',')+1){
          array_ref = data['e_ref'].split(',')
          for(i = 0; i < array_ref.length; i++){
            data['e_ref'] = array_ref[i]
            //ws_send('ones_call', data)
            //ws_send('vox', data)
            console.log("SUBMIT", action, data)
            if (action == 'busy')
              ws_send('missed', data)
            else
              ws_send('direct', data)
          }
        } else {
          //ws_send('ones_call', data)
          //ws_send('vox', data)
          console.log("SUBMIT", action, data)
          if (action == 'busy')
            ws_send('missed', data)
          else
            ws_send('direct', data)
          console.log(data)
        }

        if (action.value == 'busy') {
          ws_sms(r_phone.value)
        }
        transferOK = false
        //r = call_down()
        goAway = false

        //if (r != 2) 
        location.reload()
    } else if (location.pathname == '/meet') {
        ws_send('ones_meet', data)
        goAway = false
        location.reload()
*/
    }
}

// logout on close
window.onbeforeunload = function() {
  /*
  if (goAway && localuser)
    return "Если вы продолжите, произойдет выход из системы.";
    */
}

